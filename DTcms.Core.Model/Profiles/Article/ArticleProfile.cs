﻿using AutoMapper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using DTcms.Core.Common.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DTcms.Core.Common.Extensions;

namespace DTcms.Core.Model.Profiles
{
    public class ArticleProfile : Profile
    {
        /// <summary>
        /// 内容实体映射
        /// </summary>
        public ArticleProfile()
        {

            #region 文章主表
            //源数据映射到DTO
            CreateMap<Articles, ArticlesDto>()
                .ForMember(
                    dest => dest.CategoryTitle,
                    opt =>
                    {
                        opt.MapFrom(src => string.Join(",", src.ArticleCategoryRelations.Select(x => x.ArticleCategory.Title).ToArray()));
                    }
                ).ForMember(
                    dest => dest.LabelTitle,
                    opt =>
                    {
                        opt.MapFrom(src => string.Join(",", src.ArticleLabelRelations.Select(x => x.ArticleLabel.Title).ToArray()));
                    }
                ).ForMember(
                    dest => dest.Fields,
                    opt =>
                    {
                        opt.MapFrom(src => GetFields(src.ArticleFields));
                    }
                ).ForMember(
                    dest => dest.IsComment,
                    opt =>
                    {
                        opt.MapFrom(src => (src.SiteChannel.IsComment == 1 ? src.IsComment : 0));
                    }
                );
            //源数据映射到DTO
            CreateMap<Articles, ArticlesClientDto>()
                .ForMember(
                    dest => dest.CategoryTitle,
                    opt =>
                    {
                        opt.MapFrom(src => string.Join(",", src.ArticleCategoryRelations.Select(x => x.ArticleCategory.Title).ToArray()));
                    }
                ).ForMember(
                    dest => dest.LabelTitle,
                    opt =>
                    {
                        opt.MapFrom(src => string.Join(",", src.ArticleLabelRelations.Select(x => x.ArticleLabel.Title).ToArray()));
                    }
                ).ForMember(
                    dest => dest.Fields,
                    opt =>
                    {
                        opt.MapFrom(src => GetFields(src.ArticleFields));
                    }
                ).ForMember(
                    dest => dest.IsComment,
                    opt =>
                    {
                        opt.MapFrom(src => (src.SiteChannel.IsComment == 1 ? src.IsComment : 0));
                    }
                );
            CreateMap<Articles, ArticlesEditDto>();
            //DTO映射到源数据
            CreateMap<ArticlesAddDto, Articles>();
            CreateMap<ArticlesEditDto, Articles>();
            //类别关系
            CreateMap<ArticleCategoryRelation, ArticleCategoryRelationDto>();
            CreateMap<ArticleCategoryRelationDto, ArticleCategoryRelation>();
            //标签关系
            CreateMap<ArticleLabelRelation, ArticleLabelRelationDto>();
            CreateMap<ArticleLabelRelationDto, ArticleLabelRelation>();
            #endregion


            #region 相册
            //源数据映射到DTO
            CreateMap<ArticleAlbum, ArticleAlbumDto>();
            //DTO映射到源数据
            CreateMap<ArticleAlbumDto, ArticleAlbum>();
            #endregion

            #region 附件
            //源数据映射到DTO
            CreateMap<ArticleAttach, ArticleAttachDto>().ForMember(
                    dest => dest.Size,
                    opt =>
                    {
                        opt.MapFrom(src => FileHelper.ByteConvertStorage(src.FileSize));
                    }
                );
            //源数据映射到DTO
            CreateMap<ArticleAttach, ArticleAttachClientDto>().ForMember(
                    dest => dest.Size,
                    opt =>
                    {
                        opt.MapFrom(src => FileHelper.ByteConvertStorage(src.FileSize));
                    }
                );
            //DTO映射到源数据
            CreateMap<ArticleAttachDto, ArticleAttach>();
            #endregion

            #region 文章分类
            //源数据映射到DTO
            CreateMap<ArticleCategory, ArticleCategoryDto>();
            //DTO映射到源数据
            CreateMap<ArticleCategoryAddDto, ArticleCategory>();
            CreateMap<ArticleCategoryEditDto, ArticleCategory>();
            #endregion

            #region 文章投稿
            //源数据映射到DTO
            CreateMap<ArticleContribute, ArticleContributeDto>();
            CreateMap<ArticleContribute, ArticleContributeViewDto>()
                .ForMember(dest => dest.Fields, opt => opt.MapFrom(src => JsonHelper.ToObject<List<ArticleContributeFieldValueDto>>(src.FieldMeta)))
                .ForMember(dest => dest.Albums, opt => opt.MapFrom(src => JsonHelper.ToObject<List<ArticleAlbumDto>>(src.AlbumMeta)))
                .ForMember(dest => dest.Attachs, opt => opt.MapFrom(src => JsonHelper.ToObject<List<ArticleAttachDto>>(src.AttachMeta)));
            //DTO映射到源数据
            CreateMap<ArticleContributeAddDto, ArticleContribute>()
                .ForMember(desc => desc.FieldMeta, opt => opt.MapFrom(src => src.Fields.ToJson()))
                .ForMember(desc => desc.AlbumMeta, opt => opt.MapFrom(src => src.Albums.ToJson()))
                .ForMember(desc => desc.AttachMeta, opt => opt.MapFrom(src => src.Attachs.ToJson()));
            CreateMap<ArticleContributeEditDto, ArticleContribute>()
                .ForMember(desc => desc.FieldMeta, opt => opt.MapFrom(src => src.Fields.ToJson()))
                .ForMember(desc => desc.AlbumMeta, opt => opt.MapFrom(src => src.Albums.ToJson()))
                .ForMember(desc => desc.AttachMeta, opt => opt.MapFrom(src => src.Attachs.ToJson()));
            #endregion

            #region 评论
            //源数据映射到DTO
            CreateMap<ArticleComment, ArticleCommentDto>()
                .ForMember(
                    dest => dest.UserAvatar,
                    opt =>
                    {
                        opt.MapFrom(src => src.User.Member.Avatar);
                    }
                )
                .ForMember(
                    dest => dest.CopyContent,
                    opt =>
                    {
                        opt.MapFrom(src => (src.IsDelete == 1 ? src.Content : ""));
                    }
                )
                .ForMember(
                    dest => dest.Content,
                    opt =>
                    {
                        opt.MapFrom(src => (src.IsDelete == 1 ? "原内容已删除" : src.Content));
                    }
                )
                .ForMember(
                    dest => dest.DateDescription,
                    opt =>
                    {
                        opt.MapFrom(src => src.AddTime.ToTimeDifferNow());
                    }
                );
            CreateMap<ArticleComment, ArticleCommentAddDto>();
            //DTO映射到源数据
            CreateMap<ArticleCommentDto, ArticleComment>();
            CreateMap<ArticleCommentAddDto, ArticleComment>();
            #endregion

            #region 扩展字段
            //源数据映射到DTO
            CreateMap<ArticleFieldValue, ArticleFieldValueDto>();
            CreateMap<ArticleFieldValue, ArticleFieldValueEditDto>();
            //DTO映射到源数据
            CreateMap<ArticleFieldValueAddDto, ArticleFieldValue>();
            CreateMap<ArticleFieldValueEditDto, ArticleFieldValue>();
            #endregion

            #region lable标签
            //源数据映射到DTO
            CreateMap<ArticleLabel, ArticleLabelDto>();
            //DTO映射到源数据
            CreateMap<ArticleLabelAddDto, ArticleLabel>();
            CreateMap<ArticleLabelEditDto, ArticleLabel>();
            #endregion

            #region 文章点赞
            //源数据映射到DTO
            CreateMap<ArticleLike, ArticleLikeDto>();
            //DTO映射到源数据
            CreateMap<ArticleLikeAddDto, ArticleLike>();
            CreateMap<ArticleLikeEditDto, ArticleLike>();
            #endregion



        }

        /// <summary>
        /// 获得扩展字段键值对
        /// </summary>
        /// <param name="articleFields">扩展字段列表</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetFields(ICollection<ArticleFieldValue> articleFields)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (var item in articleFields)
            {
                dic.Add(item.FieldName, item.FieldValue);
            }
            return dic;
        }

    }
}
