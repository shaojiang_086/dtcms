﻿using AutoMapper;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Model.Profiles
{
    public class SiteProfile : Profile
    {
        /// <summary>
        /// 站点实体映射
        /// </summary>
        public SiteProfile()
        {
            //站点信息,将源数据映射到DTO
            CreateMap<Sites, SitesDto>();
            CreateMap<Sites, SitesEditDto>();
            CreateMap<SiteDomain, SiteDomainDto>();
            //站点信息,将DTO映射到源数据
            CreateMap<SitesEditDto, Sites>();
            CreateMap<SiteDomainDto, SiteDomain>();

            //站点频道,将源数据映射到DTO
            CreateMap<SiteChannel, SiteChannelDto>();
            CreateMap<SiteChannel, SiteChannelEditDto>();
            CreateMap<SiteChannelField, SiteChannelFieldDto>()
                .ForMember(dest => dest.Options, opt => opt.MapFrom(src => UtilHelper.GetCheckboxOrRadioOptions(src.ControlType, src.ItemOption)))
            .ForMember(dest => dest.FieldValue, opt => opt.MapFrom(src => UtilHelper.GetCheckboxDefaultValue(src.ControlType, src.DefaultValue)));
            //站点频道,将DTO映射到源数据
            CreateMap<SiteChannelEditDto, SiteChannel>();
            CreateMap<SiteChannelFieldDto, SiteChannelField>();

            //设置配置DTO转换
            CreateMap<SysConfigDto, UploadConfigDto>();
        }

    }
}
