﻿using AutoMapper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DTcms.Core.Model.Profiles
{
    /// <summary>
    /// 管理员实体映射
    /// </summary>
    public class ManagerProfile : Profile
    {
        public ManagerProfile()
        {
            //管理员,将源数据映射到DTO
            CreateMap<Manager, ManagerDto>()
                .ForMember(
                    dest => dest.UserName,
                    opt =>
                    {
                        opt.MapFrom(src => src.ApplicatonUser.UserName);
                    }
                ).ForMember(
                    dest => dest.Email,
                    opt =>
                    {
                        opt.MapFrom(src => src.ApplicatonUser.Email);
                    }
                ).ForMember(
                    dest => dest.Phone,
                    opt =>
                    {
                        opt.MapFrom(src => src.ApplicatonUser.PhoneNumber);
                    }
                ).ForMember(
                    dest => dest.RoleId,
                    opt =>
                    {
                        opt.MapFrom(src => src.ApplicatonUser.UserRoles.FirstOrDefault().RoleId);
                    }
                ).ForMember(
                    dest => dest.Status,
                    opt =>
                    {
                        opt.MapFrom(src => src.ApplicatonUser.Status);
                    }
                );
            //管理员,将DTO映射到源数据
            CreateMap<ManagerDto, Manager>();
            CreateMap<ManagerEditDto, Manager>();

            //管理角色,将源数据映射到DTO
            CreateMap<ApplicationRole, ManagerRolesDto>();
            //管理角色,将DTO映射到源数据
            CreateMap<ManagerRolesDto, ApplicationRole>();
            CreateMap<ManagerRolesEditDto, ApplicationRole>();

            //管理日志,将源数据映射到DTO
            CreateMap<ManagerLog, ManagerLogDto>();
            //管理日志,将DTO映射到源数据
            CreateMap<ManagerLogDto, ManagerLog>();
        }
    }
}
