﻿using AutoMapper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Model.Profiles
{
    public class NavigationProfile : Profile
    {
        /// <summary>
        /// 菜单实体映射
        /// </summary>
        public NavigationProfile()
        {
            //将源数据映射到DTO
            CreateMap<Navigation, NavigationDto>();
            CreateMap<Navigation, NavigationEditDto>();
            //将DTO映射到源数据，修改或添加时忽略主健赋值
            CreateMap<NavigationEditDto, Navigation>();
        }

    }
}
