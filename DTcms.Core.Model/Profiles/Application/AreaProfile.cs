﻿using AutoMapper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Model.Profiles
{
    public class AreaProfile : Profile
    {
        /// <summary>
        /// 省市区实体映射
        /// </summary>
        public AreaProfile()
        {
            //将源数据映射到DTO
            CreateMap<Areas, AreasDto>();
            CreateMap<Areas, AreasEditDto>();
            //将DTO映射到源数据
            CreateMap<AreasEditDto, Areas>();
        }
    }
}
