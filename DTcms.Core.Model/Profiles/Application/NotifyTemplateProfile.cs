﻿using AutoMapper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Model.Profiles
{
    public class NotifyTemplateProfile : Profile
    {
        /// <summary>
        /// 通知模板实体映射
        /// </summary>
        public NotifyTemplateProfile()
        {
            //将源数据映射到DTO
            CreateMap<NotifyTemplate, NotifyTemplateDto>();
            CreateMap<NotifyTemplate, NotifyTemplateEditDto>();
            //将DTO映射到源数据
            CreateMap<NotifyTemplateEditDto, NotifyTemplate>();
        }
    }
}
