﻿using AutoMapper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Model.Profiles
{
    public class PaymentProfile : Profile
    {
        /// <summary>
        /// 支付方式实体映射
        /// </summary>
        public PaymentProfile()
        {
            //支付方式，将源数据映射到DTO
            CreateMap<Payment, PaymentDto>();
            CreateMap<Payment, PaymentEditDto>();
            //支付方式，将DTO映射到源数据
            CreateMap<PaymentEditDto, Payment>();

            //站点支付方式，将源数据映射到DTO
            CreateMap<SitePayment, SitePaymentDto>();
            CreateMap<SitePayment, SitePaymentEditDto>();
            //站点支付方式，将DTO映射到源数据
            CreateMap<SitePaymentEditDto, SitePayment>();
        }
    }
}
