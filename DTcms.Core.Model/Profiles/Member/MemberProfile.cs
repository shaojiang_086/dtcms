﻿using AutoMapper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Model.Profiles
{
    /// <summary>
    /// 会员实体映射
    /// </summary>
    public class MemberProfile : Profile
    {
        public MemberProfile()
        {
            //会员信息，将源数据映射到DTO
            CreateMap<Members, MembersDto>()
                .ForMember(
                    dest => dest.GroupTitle,
                    opt =>
                    {
                        opt.MapFrom(src => src.Group.Title);
                    }
                ).ForMember(
                    dest => dest.UserName,
                    opt =>
                    {
                        opt.MapFrom(src => src.User.UserName);
                    }
                ).ForMember(
                    dest => dest.Email,
                    opt =>
                    {
                        opt.MapFrom(src => src.User.Email);
                    }
                ).ForMember(
                    dest => dest.Phone,
                    opt =>
                    {
                        opt.MapFrom(src => src.User.PhoneNumber);
                    }
                ).ForMember(
                    dest => dest.Status,
                    opt =>
                    {
                        opt.MapFrom(src => src.User.Status);
                    }
                );
            CreateMap<Members, MembersEditDto>();
            //会员信息，将源数据映射到DTO
            CreateMap<MembersEditDto, Members>();
            CreateMap<MembersModifyDto, Members>();
            CreateMap<RegisterDto, MembersEditDto>();

            //会员组，将源数据映射到DTO
            CreateMap<MemberGroup, MemberGroupDto>();
            CreateMap<MemberGroup, MemberGroupEditDto>();
            //会员组，将DTO映射到源数据
            CreateMap<MemberGroupEditDto, MemberGroup>();

            //站内消息，将源数据映射到DTO
            CreateMap<MemberMessage, MemberMessageEditDto>();
            //站内消息，将DTO映射到源数据
            CreateMap<MemberMessageEditDto, MemberMessage>();

            //会员充值，将源数据映射到DTO
            CreateMap<MemberRecharge, MemberRechargeEditDto>();
            //会员充值，将DTO映射到源数据
            CreateMap<MemberRechargeEditDto, MemberRecharge>();

            //积分记录，将源数据映射到DTO
            CreateMap<MemberPointLog, MemberPointLogEditDto>();
            //积分记录，将DTO映射到源数据
            CreateMap<MemberPointLogEditDto, MemberPointLog>();

            //余额记录，将源数据映射到DTO
            CreateMap<MemberAmountLog, MemberAmountLogEditDto>();
            //余额记录，将DTO映射到源数据
            CreateMap<MemberAmountLogEditDto, MemberAmountLog>();

            //附件下载记录，将源数据映射到DTO
            CreateMap<MemberAttachLog, MemberAttachLogEditDto>();
            //附件下载记录，将DTO映射到源数据
            CreateMap<MemberAttachLogEditDto, MemberAttachLog>();
        }
    }
}
