﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 统计数量DTO
    /// </summary>
    public class ReportTotalDto
    {
        /// <summary>
        /// 显示标题
        /// </summary>
        [Display(Name = "显示标题")]
        public string Title { set; get; }

        /// <summary>
        /// 统计数量
        /// </summary>
        [Display(Name = "总数量")]
        public int Total { set; get; }

        /// <summary>
        /// 总金额
        /// </summary>
        public decimal Amount { set; get; }
    }
}
