﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 会员充值(显示)
    /// </summary>
    public class MemberRechargeDto : MemberRechargeEditDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "用户名")]
        [StringLength(128)]
        public string UserName { get; set; }

        [Display(Name = "支付方式名称")]
        [StringLength(128)]
        public string PaymentTitle { get; set; }

        [Display(Name = "充值单号")]
        [StringLength(128)]
        public string RechargeNo { get; set; }

        /// <summary>
        /// 状态0未完成1已完成
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;

        [Display(Name = "完成时间")]
        public DateTime? CompleteTime { get; set; }
    }

    /// <summary>
    /// 会员充值(增改)
    /// </summary>
    public class MemberRechargeEditDto
    {
        [Display(Name = "充值用户")]
        public int UserId { get; set; }

        [Display(Name = "支付方式")]
        [Required(ErrorMessage = "{0}不可为空")]
        public int PaymentId { get; set; }

        [Display(Name = "充值金额")]
        [Required(ErrorMessage = "{0}不可为空")]
        public decimal Amount { get; set; } = 0;
    }

    /// <summary>
    /// 修改支付方式
    /// </summary>
    public class MemberRechargePaymentDto
    {
        /// <summary>
        /// 自增ID
        /// </summary>
        [Display(Name = "充值ID")]
        [Required(ErrorMessage = "{0}不可为空")]
        public long Id { get; set; }

        [Display(Name = "支付方式")]
        [Required(ErrorMessage = "{0}不可为空")]
        public int PaymentId { get; set; }
    }
}
