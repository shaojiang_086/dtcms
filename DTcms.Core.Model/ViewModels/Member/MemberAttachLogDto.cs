﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 附件下载记录(显示)
    /// </summary>
    public class MemberAttachLogDto : MemberAttachLogEditDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "下载时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "用户名")]
        [StringLength(128)]
        public string UserName { get; set; }
    }

    /// <summary>
    /// 附件下载记录(增改)
    /// </summary>
    public class MemberAttachLogEditDto
    {
        [Display(Name = "所属用户")]
        [Required(ErrorMessage = "{0}不可为空")]
        public int UserId { get; set; }

        [Display(Name = "附件ID")]
        [Required(ErrorMessage = "{0}不可为空")]
        public long AttachId { get; set; }

        [Display(Name = "文件名称")]
        [Required(ErrorMessage = "{0}不可为空")]
        [StringLength(255)]
        public string FileName { get; set; }
    }
}
