﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 站内消息(显示)
    /// </summary>
    public class MemberMessageDto : MemberMessageEditDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "用户名")]
        [StringLength(128)]
        public string UserName { get; set; }
    }

    /// <summary>
    /// 站内消息(增改)
    /// </summary>
    public class MemberMessageEditDto
    {
        [Display(Name = "接收用户")]
        [Required(ErrorMessage = "{0}不可为空")]
        public int UserId { get; set; }

        [Display(Name = "标题")]
        [Required(ErrorMessage = "{0}不可为空")]
        [MinLength(3, ErrorMessage = "{0}不可少于{1}字符")]
        [MaxLength(128, ErrorMessage = "{0}不可多于{2}字符")]
        public string Title { get; set; }

        [Display(Name = "内容")]
        [Required(ErrorMessage = "{0}不可为空")]
        public string Content { get; set; }

        [Display(Name = "是否已读")]
        public byte IsRead { get; set; } = 0;

        [Display(Name = "读取时间")]
        public DateTime? ReadTime { get; set; }
    }
}
