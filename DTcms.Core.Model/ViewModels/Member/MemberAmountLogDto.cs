﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 余额记录(显示)
    /// </summary>
    public class MemberAmountLogDto : MemberAmountLogEditDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; }

        [Display(Name = "用户名")]
        [StringLength(128)]
        public string UserName { get; set; }
    }

    /// <summary>
    /// 余额记录(增改)
    /// </summary>
    public class MemberAmountLogEditDto
    {
        [Display(Name = "所属用户")]
        [Required(ErrorMessage = "{0}不可为空")]
        public int UserId { get; set; }

        [Display(Name = "金额")]
        [Required(ErrorMessage = "{0}不可为空")]
        public decimal Value { get; set; } = 0;

        [Display(Name = "备注说明")]
        [StringLength(512)]
        [Required(ErrorMessage = "{0}不可为空")]
        public string Remark { get; set; }
    }
}
