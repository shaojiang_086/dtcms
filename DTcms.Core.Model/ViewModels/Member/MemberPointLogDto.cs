﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 积分记录(显示)
    /// </summary>
    public class MemberPointLogDto : MemberPointLogEditDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "用户名")]
        [StringLength(128)]
        public string UserName { get; set; }
    }

    /// <summary>
    /// 积分记录(增改)
    /// </summary>
    public class MemberPointLogEditDto
    {
        [Display(Name = "所属用户")]
        [Required(ErrorMessage = "{0}不可为空")]
        public int UserId { get; set; }

        [Display(Name = "增减积分")]
        [Required(ErrorMessage = "{0}不可为空")]
        public int Value { get; set; } = 0;

        [Display(Name = "备注说明")]
        [StringLength(512)]
        public string Remark { get; set; }
    }
}
