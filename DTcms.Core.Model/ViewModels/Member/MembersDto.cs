﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 会员(显示)
    /// </summary>
    public class MembersDto : MembersEditDto
    {
        [Display(Name = "自增ID")]
        public int Id { get; set; }

        [Display(Name = "所属用户")]
        public int UserId { get; set; }

        [Display(Name = "会员组名")]
        public string GroupTitle { get; set; }

        [Display(Name = "余额")]
        public decimal Amount { get; set; }

        [Display(Name = "积分")]
        public int Point { get; set; }

        [Display(Name = "经验值")]
        public int Exp { get; set; }

        [Display(Name = "注册IP")]
        [MaxLength(128)]
        public string RegIp { get; set; }

        [Display(Name = "注册时间")]
        public DateTime RegTime { get; set; }

        [Display(Name = "最后登录IP")]
        [MaxLength(128)]
        public string LastIp { get; set; }

        [Display(Name = "最后登录时间")]
        public DateTime? LastTime { get; set; }
    }

    /// <summary>
    /// 会员(编辑)
    /// </summary>
    public class MembersEditDto
    {
        [Display(Name = "所属站点")]
        public int SiteId { get; set; }

        [Display(Name = "用户名")]
        [MinLength(3, ErrorMessage ="{0}至少{1}位字符")]
        [MaxLength(128, ErrorMessage ="{0}最多{2}位字符")]
        public string UserName { get; set; }

        [Display(Name = "邮箱地址")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "手机号码")]
        [RegularExpression(@"^(13|14|15|16|18|19|17)\d{9}$")]
        public string Phone { get; set; }

        [Display(Name = "登录密码")]
        [MinLength(6, ErrorMessage = "{0}至少{1}位字符")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// 状态(0正常1待验证2待审核3锁定)
        /// </summary>
        [Display(Name = "账户状态")]
        public byte Status { get; set; } = 0;


        [Display(Name = "会员组")]
        public int GroupId { get; set; }

        [Display(Name = "会员头像")]
        [MaxLength(512)]
        public string Avatar { get; set; }

        [Display(Name = "姓名")]
        [MaxLength(30)]
        public string RealName { get; set; }

        [Display(Name = "性别")]
        [MaxLength(30)]
        public string Sex { get; set; }

        [Display(Name = "生日")]
        public DateTime? Birthday { get; set; }
    }

    /// <summary>
    /// 会员(修改)
    /// </summary>
    public class MembersModifyDto
    {
        [Display(Name = "会员头像")]
        [MaxLength(512)]
        public string Avatar { get; set; }

        [Display(Name = "姓名")]
        [MaxLength(30)]
        public string RealName { get; set; }

        [Display(Name = "性别")]
        [MaxLength(30)]
        public string Sex { get; set; }

        [Display(Name = "生日")]
        public DateTime? Birthday { get; set; }
    }

    /// <summary>
    /// 会员统计DTO
    /// </summary>
    public class MembersReportDto
    {
        /// <summary>
        /// 显示标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 显示数量
        /// </summary>
        public int Count { get; set; }
    }
}
