﻿using System.ComponentModel.DataAnnotations;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 文章扩展字段DTO
    /// </summary>
    public class ArticleFieldValueBaseDto
    {
        
        [Display(Name = "所属文章")]
        public long ArticleId { get; set; }

        [Display(Name = "所属字段")]
        public long FieldId { get; set; }

        [Display(Name = "字段名")]
        [StringLength(128)]
        public string FieldName { get; set; }

        [Display(Name = "字段值")]
        public string FieldValue { get; set; }

    }

    /// <summary>
    /// 文章扩展字段(List)
    /// </summary>
    public class ArticleFieldValueDto : ArticleFieldValueBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 文章扩展字段(Add)
    /// </summary>
    public class ArticleFieldValueAddDto: ArticleFieldValueBaseDto
    {
        
    }

    /// <summary>
    /// 文章扩展字段(Edit)
    /// </summary>
    public class ArticleFieldValueEditDto : ArticleFieldValueBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

    }

    
}