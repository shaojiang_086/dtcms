﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 文章附件
    /// </summary>
    public class ArticleAttachDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "所属文章")]
        public long ArticleId { get; set; }

        [Display(Name = "文件名")]
        public string FileName { get; set; }

        [Display(Name = "文件路径")]
        public string FilePath { get; set; }

        [Display(Name = "文件大小")]
        public int FileSize { get; set; }

        [Display(Name = "扩展名")]
        public string FileExt { get; set; }

        [Display(Name = "下载所需积分")]
        public int Point { get; set; } = 0;

        [Display(Name = "下载次数")]
        public int DownCount { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "文件大小")]
        public string Size { get; set; }
    }

    /// <summary>
    /// 文章附件(前端)
    /// </summary>
    public class ArticleAttachClientDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "所属文章")]
        public long ArticleId { get; set; }

        [Display(Name = "文件名")]
        public string FileName { get; set; }

        [Display(Name = "文件大小")]
        public int FileSize { get; set; }

        [Display(Name = "扩展名")]
        public string FileExt { get; set; }

        [Display(Name = "下载所需积分")]
        public int Point { get; set; } = 0;

        [Display(Name = "下载次数")]
        public int DownCount { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "文件大小")]
        public string Size { get; set; }
    }
}
