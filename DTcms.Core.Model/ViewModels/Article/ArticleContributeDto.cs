﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{

    /// <summary>
    /// 文章投稿(Base)
    /// </summary>
    public class ArticleContributeBaseDto
    {
        [Display(Name = "所属站点")]
        public int SiteId { get; set; }

        [Display(Name = "所属频道")]
        public int ChannelId { get; set; }

        [Display(Name = "标题")]
        [Required(ErrorMessage = "{0}不可为空")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string Title { get; set; }

        [Display(Name = "来源")]
        [MaxLength(128, ErrorMessage = "{0}不可超出{1}字符")]
        public string Source { get; set; }

        [Display(Name = "作者")]
        [MaxLength(128, ErrorMessage = "{0}不可超出{1}字符")]
        public string Author { get; set; }

        [Display(Name = "图片地址")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string ImgUrl { get; set; }

        [Display(Name = "扩展字段集合")]
        public string FieldMeta { get; set; }

        [Display(Name = "相册扩展字段值")]
        public string AlbumMeta { get; set; }

        [Display(Name = "附件扩展字段值")]
        public string AttachMeta { get; set; }

        [Display(Name = "内容")]
        public string Content { get; set; }

        [Display(Name = "投稿用户")]
        public int UserId { get; set; } = 0;

        [Display(Name = "用户名")]
        public string UserName { get; set; }

        /// <summary>
        /// 状态0待审1通过2返回
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;

    }
    /// <summary>
    /// 文章投稿
    /// </summary>
    public class ArticleContributeDto: ArticleContributeBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "更新人")]
        public string UpdateBy { get; set; }

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }
    }



    /// <summary>
    /// 文章投稿(Add)
    /// </summary>
    public class ArticleContributeAddDto : ArticleContributeBaseDto
    {

        //[Display(Name = "创建时间")]
        //public DateTime AddTime { get; set; } = DateTime.Now;
        public List<ArticleContributeFieldValueAddDto> Fields { get; set; }
        /// <summary>
        /// 相册
        /// </summary>
        public List<ArticleAlbumDto> Albums { get; set; }
        /// <summary>
        /// 附件
        /// </summary>
        public List<ArticleAttachDto> Attachs { get; set; }
    }

    /// <summary>
    /// 文章投稿(Edit)
    /// </summary>
    public class ArticleContributeEditDto : ArticleContributeBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;
        [Display(Name = "更新人")]
        public string UpdateBy { get; set; }

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 扩展字段值
        /// </summary>
        public List<ArticleContributeFieldValueEditDto> Fields { get; set; }
        /// <summary>
        /// 相册
        /// </summary>
        public List<ArticleAlbumDto> Albums { get; set; }
        /// <summary>
        /// 附件
        /// </summary>
        public List<ArticleAttachDto> Attachs { get; set; }

        /// <summary>
        /// 所属分类
        /// </summary>
        public string[] Categorys { get; set; }
    }


    /// <summary>
    /// 文章投稿编辑展示(View)
    /// </summary>
    public class ArticleContributeViewDto : ArticleContributeBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "更新人")]
        public string UpdateBy { get; set; }

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 扩展字段
        /// </summary>
        public List<ArticleContributeFieldValueDto> Fields { get; set; }
        /// <summary>
        /// 相册
        /// </summary>
        public List<ArticleAlbumDto> Albums { get; set; }
        /// <summary>
        /// 附件
        /// </summary>
        public List<ArticleAttachDto> Attachs { get; set; }
    }

}
