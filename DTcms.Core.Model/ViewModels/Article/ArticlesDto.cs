﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 文章实体
    /// </summary>
    public class ArticlesBaseDto
    {
        [Display(Name = "所属站点")]
        public int SiteId { get; set; }

        [Display(Name = "所属频道")]
        public int ChannelId { get; set; }

        [Display(Name = "调用别名")]
        [StringLength(128)]
        public string CallIndex { get; set; } = string.Empty;

        [Display(Name = "标题")]
        [Required(ErrorMessage = "{0}不可为空")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string Title { get; set; } = string.Empty;

        [Display(Name = "来源")]
        [MaxLength(128, ErrorMessage = "{0}不可超出{1}字符")]
        public string Source { get; set; } = string.Empty;

        [Display(Name = "作者")]
        [MaxLength(128, ErrorMessage = "{0}不可超出{1}字符")]
        public string Author { get; set; } = string.Empty;

        [Display(Name = "外部链接")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string LinkUrl { get; set; } = string.Empty;

        [Display(Name = "图片地址")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string ImgUrl { get; set; } = string.Empty;

        [Display(Name = "SEO标题")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string SeoTitle { get; set; } = string.Empty;

        [Display(Name = "SEO关健字")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string SeoKeyword { get; set; } = string.Empty;

        [Display(Name = "SEO描述")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string SeoDescription { get; set; } = string.Empty;

        [Display(Name = "内容摘要")]
        [MaxLength(255, ErrorMessage = "{0}不可超出{1}字符")]
        public string Zhaiyao { get; set; } = string.Empty;

        [Display(Name = "内容")]
        public string Content { get; set; } = string.Empty;

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        [Display(Name = "浏览总数")]
        public int Click { get; set; } = 0;

        /// <summary>
        /// 状态0正常1待审2已删
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;

        /// <summary>
        /// 评论0禁止1允许
        /// </summary>
        [Display(Name = "是否评论")]
        public byte IsComment { get; set; } = 0;

        [Display(Name = "评论总数")]
        public int CommentCount { get; set; } = 0;

        [Display(Name = "点赞总数")]
        public int LikeCount { get; set; } = 0;

        [Display(Name = "扩展字段键值")]
        public Dictionary<string, string> Fields { get; set; }

    }

    /// <summary>
    /// 文章实体(List)
    /// </summary>
    public class ArticlesDto : ArticlesBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建人")]
        public string AddBy { get; set; } = string.Empty;

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "更新人")]
        public string UpdateBy { get; set; } = string.Empty;

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }

        [Display(Name = "选中状态")]
        public bool Checked { get; set; } = false;

        /// <summary>
        /// 类别标题，以逗号分隔
        /// </summary>
        public string CategoryTitle { get; set; }

        /// <summary>
        /// 标签标题，以逗号分隔
        /// </summary>
        public string LabelTitle { get; set; }

        /// <summary>
        /// 扩展字段列表
        /// </summary>
        public ICollection<ArticleFieldValueDto> ArticleFields { get; set; } = new List<ArticleFieldValueDto>();

        /// <summary>
        /// 类别关联列表
        /// </summary>
        public ICollection<ArticleCategoryRelationDto> ArticleCategoryRelations { get; set; } = new List<ArticleCategoryRelationDto>();

        /// <summary>
        /// 标签关联列表
        /// </summary>
        public ICollection<ArticleLabelRelationDto> ArticleLabelRelations { get; set; } = new List<ArticleLabelRelationDto>();

        /// <summary>
        /// 相册列表
        /// </summary>
        public ICollection<ArticleAlbumDto> ArticleAlbums { get; set; } = new List<ArticleAlbumDto>();

        /// <summary>
        /// 附件列表
        /// </summary>
        public ICollection<ArticleAttachDto> ArticleAttachs { get; set; } = new List<ArticleAttachDto>();
    }

    /// <summary>
    /// 文章实体(Add)
    /// </summary>
    public class ArticlesAddDto : ArticlesBaseDto
    {


        [Display(Name = "创建人")]
        public string AddBy { get; set; } = string.Empty;

        [Display(Name = "创建时间")]
        public DateTime? AddTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 扩展字段列表
        /// </summary>
        public ICollection<ArticleFieldValueAddDto> ArticleFields { get; set; } = new List<ArticleFieldValueAddDto>();

        /// <summary>
        /// 类别关联列表
        /// </summary>
        public ICollection<ArticleCategoryRelationDto> ArticleCategoryRelations { get; set; } = new List<ArticleCategoryRelationDto>();

        /// <summary>
        /// 标签关联列表
        /// </summary>
        public ICollection<ArticleLabelRelationDto> ArticleLabelRelations { get; set; } = new List<ArticleLabelRelationDto>();

        /// <summary>
        /// 相册列表
        /// </summary>
        public ICollection<ArticleAlbumDto> ArticleAlbums { get; set; } = new List<ArticleAlbumDto>();

        /// <summary>
        /// 附件列表
        /// </summary>
        public ICollection<ArticleAttachDto> ArticleAttachs { get; set; } = new List<ArticleAttachDto>();
    }

    /// <summary>
    /// 文章实体(Edit)
    /// </summary>
    public class ArticlesEditDto : ArticlesBaseDto
    {

        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "更新人")]
        public string UpdateBy { get; set; } = string.Empty;

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; }

        /// <summary>
        /// 扩展字段列表
        /// </summary>
        public ICollection<ArticleFieldValueEditDto> ArticleFields { get; set; } = new List<ArticleFieldValueEditDto>();

        /// <summary>
        /// 类别关联列表
        /// </summary>
        public ICollection<ArticleCategoryRelationDto> ArticleCategoryRelations { get; set; } = new List<ArticleCategoryRelationDto>();

        /// <summary>
        /// 标签关联列表
        /// </summary>
        public ICollection<ArticleLabelRelationDto> ArticleLabelRelations { get; set; } = new List<ArticleLabelRelationDto>();

        /// <summary>
        /// 相册列表
        /// </summary>
        public ICollection<ArticleAlbumDto> ArticleAlbums { get; set; } = new List<ArticleAlbumDto>();

        /// <summary>
        /// 附件列表
        /// </summary>
        public ICollection<ArticleAttachDto> ArticleAttachs { get; set; } = new List<ArticleAttachDto>();
    }

    /// <summary>
    /// 文章实体(前端)
    /// </summary>
    public class ArticlesClientDto : ArticlesBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建人")]
        public string AddBy { get; set; } = string.Empty;

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 类别标题，以逗号分隔
        /// </summary>
        public string CategoryTitle { get; set; }

        /// <summary>
        /// 标签标题，以逗号分隔
        /// </summary>
        public string LabelTitle { get; set; }

        /// <summary>
        /// 扩展字段列表
        /// </summary>
        public ICollection<ArticleFieldValueDto> ArticleFields { get; set; } = new List<ArticleFieldValueDto>();

        /// <summary>
        /// 类别关联列表
        /// </summary>
        public ICollection<ArticleCategoryRelationDto> ArticleCategoryRelations { get; set; } = new List<ArticleCategoryRelationDto>();

        /// <summary>
        /// 标签关联列表
        /// </summary>
        public ICollection<ArticleLabelRelationDto> ArticleLabelRelations { get; set; } = new List<ArticleLabelRelationDto>();

        /// <summary>
        /// 相册列表
        /// </summary>
        public ICollection<ArticleAlbumDto> ArticleAlbums { get; set; } = new List<ArticleAlbumDto>();

        /// <summary>
        /// 附件列表(前端)
        /// </summary>
        public ICollection<ArticleAttachClientDto> ArticleAttachs { get; set; } = new List<ArticleAttachClientDto>();
    }
}
