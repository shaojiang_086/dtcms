﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 文章投稿扩展字段DTO
    /// </summary>
    public class ArticleContributeFieldValueBaseDto
    {

        [Display(Name = "所属字段")]
        public long FieldId { get; set; }

        [Display(Name = "字段名")]
        [StringLength(128)]
        public string FieldName { get; set; }

        [Display(Name = "字段值")]
        public string FieldValue { get; set; }

    }

    /// <summary>
    /// 文章投稿扩展字段(List)
    /// </summary>
    [Serializable]
    public class ArticleContributeFieldValueDto : ArticleContributeFieldValueBaseDto
    {

    }

    /// <summary>
    /// 文章投稿扩展字段(Add)
    /// </summary>
    public class ArticleContributeFieldValueAddDto : ArticleContributeFieldValueBaseDto
    {
        
    }

    /// <summary>
    /// 文章投稿扩展字段(Edit)
    /// </summary>
    public class ArticleContributeFieldValueEditDto : ArticleContributeFieldValueBaseDto
    {

    }

    
}