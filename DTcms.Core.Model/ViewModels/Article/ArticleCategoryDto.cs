﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 文章分离(Base)
    /// </summary>
    public class ArticleCategoryBaseDto
    {
        [Display(Name = "所属站点")]
        public int SiteId { get; set; }

        [Display(Name = "所属频道")]
        public int ChannelId { get; set; }

        [Display(Name = "所属父类")]
        public long ParentId { get; set; } = 0;

        [Display(Name = "调用别名")]
        [MaxLength(128, ErrorMessage = "{0}不可超出{1}字符")]
        public string CallIndex { get; set; }

        [Display(Name = "类别深度")]
        public int ClassLayer { get; set; } = 1;

        [Display(Name = "标题")]
        [Required(ErrorMessage = "{0}不可为空")]
        [MaxLength(128, ErrorMessage = "{0}不可超出{1}字符")]
        [MinLength(1, ErrorMessage = "{0}不可小于{1}字符")]
        public string Title { get; set; }

        [Display(Name = "图片地址")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string ImgUrl { get; set; }

        [Display(Name = "外部链接")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string LinkUrl { get; set; }

        [Display(Name = "内容介绍")]
        public string Content { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        [Display(Name = "SEO标题")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string SeoTitle { get; set; }

        [Display(Name = "SEO关健字")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string SeoKeyword { get; set; }

        [Display(Name = "SEO描述")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string SeoDescription { get; set; }

        /// <summary>
        /// 状态0正常1禁用
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;
        
    }

    /// <summary>
    /// 文章类别(list)
    /// </summary>
    public class ArticleCategoryDto:ArticleCategoryBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建人")]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; }

        [Display(Name = "更新人")]
        public string UpdateBy { get; set; }

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 子类列表
        /// </summary>
        public List<ArticleCategoryDto> Children { get; set; } = new List<ArticleCategoryDto>();
    }

    /// <summary>
    /// 文章类别(Add)
    /// </summary>
    public class ArticleCategoryAddDto : ArticleCategoryBaseDto
    {
        [Display(Name = "创建人")]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

    }

    /// <summary>
    /// 文章类别(Edit)
    /// </summary>
    public class ArticleCategoryEditDto : ArticleCategoryBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "更新人")]
        public string UpdateBy { get; set; }

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }

    }
}
