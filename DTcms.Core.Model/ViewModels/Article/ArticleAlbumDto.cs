﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    public class ArticleAlbumDto
    {
        [Display(Name = "所属文章")]
        public long ArticleId { get; set; }

        [Display(Name = "缩略图")]
        public string ThumbPath { get; set; }

        [Display(Name = "原图")]
        public string OriginalPath { get; set; }

        [Display(Name = "图片描述")]
        public string Remark { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;
    }
}
