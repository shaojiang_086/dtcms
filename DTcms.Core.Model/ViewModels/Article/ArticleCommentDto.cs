﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 文章评论
    /// </summary>
    public class ArticleCommentDto
    {
        /// <summary>
        /// 自增ID
        /// </summary>
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        /// <summary>
        /// 所属站点
        /// </summary>
        [Display(Name = "所属站点")]
        public int SiteId { get; set; }

        /// <summary>
        /// 所属频道
        /// </summary>
        [Display(Name = "所属频道")]
        public int ChannelId { get; set; }

        /// <summary>
        /// 所属文章
        /// </summary>
        [Display(Name = "所属文章")]
        public long ArticleId { get; set; }

        /// <summary>
        /// 所属父类
        /// </summary>
        [Display(Name = "所属父类")]
        public long ParentId { get; set; }

        /// <summary>
        /// 首层主键
        /// </summary>
        [Display(Name = "首层主键")]
        public long RootId { get; set; }

        /// <summary>
        /// 评论用户
        /// </summary>
        [Display(Name = "评论用户")]
        [ForeignKey("User")]
        public int UserId { get; set; } = 0;

        /// <summary>
        /// 用户名
        /// </summary>
        [Display(Name = "用户名")]
        public string UserName { get; set; }

        [Display(Name = "用户头像")]
        public string UserAvatar { get; set; }

        /// <summary>
        /// 用户IP
        /// </summary>
        [Display(Name = "用户IP")]
        public string UserIp { get; set; }

        /// <summary>
        /// 被评论人ID
        /// </summary>
        [Display(Name = "被评论人")]
        public int AtUserId { get; set; } = 0;

        /// <summary>
        /// 被评论人用户名
        /// </summary>
        [Display(Name = "被评论人用户名")]
        public string AtUserName { get; set; }

        /// <summary>
        /// 评论内容
        /// </summary>
        [Display(Name = "评论内容")]
        public string Content { get; set; }

        /// <summary>
        /// 复制评论内容
        /// </summary>
        [Display(Name = "复制评论内容")]
        public string CopyContent { get; set; }

        /// <summary>
        /// 点赞数量
        /// </summary>
        [Display(Name = "点赞数量")]
        public int LikeCount { get; set; } = 0;

        /// <summary>
        /// 状态0正常1隐藏
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;

        /// <summary>
        /// 已删除0否1是
        /// </summary>
        [Display(Name = "已删除")]
        public byte IsDelete { get; set; } = 0;

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "类别深度")]
        public int ClassLayer { get; set; } = 1;

        /// <summary>
        /// 时间描述 多少天前
        /// </summary>
        [Display(Name = "时间描述")]
        public string DateDescription { get; set; }

        /// <summary>
        /// 文章信息
        /// </summary>
        public ArticlesDto Article { get; set; }

        /// <summary>
        /// 子类列表
        /// </summary>
        public List<ArticleCommentDto> Children { get; set; } = new List<ArticleCommentDto>();
    }


    /// <summary>
    /// 文章评论(编辑)
    /// </summary>
    public class ArticleCommentAddDto
    {
        /// <summary>
        /// 所属站点
        /// </summary>
        [Display(Name = "所属站点")]
        public int SiteId { get; set; }

        ///// <summary>
        ///// 所属频道
        ///// </summary>
        //[Display(Name = "所属频道")]
        //public int ChannelId { get; set; }

        /// <summary>
        /// 所属文章
        /// </summary>
        [Display(Name = "所属文章")]
        public long ArticleId { get; set; }

        /// <summary>
        /// 所属父类
        /// </summary>
        [Display(Name = "所属父类")]
        public long ParentId { get; set; }

        ///// <summary>
        ///// 评论用户
        ///// </summary>
        //[Display(Name = "评论用户")]
        //[ForeignKey("User")]
        //public int UserId { get; set; } = 0;

        ///// <summary>
        ///// 用户名
        ///// </summary>
        //[Display(Name = "用户名")]
        //[StringLength(128)]
        //public string UserName { get; set; }

        /// <summary>
        /// 用户IP
        /// </summary>
        [Display(Name = "用户IP")]
        [StringLength(128)]
        public string UserIp { get; set; }

        ///// <summary>
        ///// 被评论人ID
        ///// </summary>
        //[Display(Name = "被评论人")]
        //public int AtUserId { get; set; } = 0;

        ///// <summary>
        ///// 被评论人用户名
        ///// </summary>
        //[Display(Name = "被评论人用户名")]
        //[StringLength(128)]
        //public string AtUserName { get; set; }

        /// <summary>
        /// 评论内容
        /// </summary>
        [Display(Name = "评论内容")]
        [StringLength(512)]
        public string Content { get; set; }

    }
}
