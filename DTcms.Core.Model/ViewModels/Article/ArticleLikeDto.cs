﻿using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 文章点赞
    /// </summary>
    public class ArticleLikeBaseDto
    {

        [Display(Name = "所属文章")]
        public long ArticleId { get; set; }

    }
    /// <summary>
    /// 文章点赞(List)
    /// </summary>
    public class ArticleLikeDto: ArticleLikeBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "所属用户")]
        public int UserId { get; set; }

        [Display(Name = "点赞时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;
    }
    /// <summary>
    /// 文章点赞(add)
    /// </summary>
    public class ArticleLikeAddDto: ArticleLikeBaseDto
    {

        [Display(Name = "所属用户")]
        public int UserId { get; set; }

        [Display(Name = "点赞时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;
    }
    /// <summary>
    /// 文章点赞(Edit)
    /// </summary>
    public class ArticleLikeEditDto: ArticleLikeBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }
    }
}
