﻿using System.ComponentModel.DataAnnotations;

namespace DTcms.Core.Model.ViewModels
{
    public class ArticleLabelRelationDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "所属文章")]
        public long ArticleId { get; set; }

        [Display(Name = "所属标签")]
        public long LabelId { get; set; }

    }
}