﻿using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 文章标签
    /// </summary>
    public class ArticleLabelBaseDto
    {

        [Display(Name = "标题")]
        public string Title { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        /// <summary>
        /// 状态0正常1禁用
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;

    }

    /// <summary>
    /// 文章标签(List)
    /// </summary>
    public class ArticleLabelDto: ArticleLabelBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "创建人")]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "更新人")]
        public string UpdateBy { get; set; }

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }
    }

    /// <summary>
    /// 文章标签(Add)
    /// </summary>
    public class ArticleLabelAddDto : ArticleLabelBaseDto
    {
        [Display(Name = "创建人")]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;
    }


    /// <summary>
    /// 文章标签(Edit)
    /// </summary>
    public class ArticleLabelEditDto : ArticleLabelBaseDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "更新人")]
        public string UpdateBy { get; set; }

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }
    }
}
