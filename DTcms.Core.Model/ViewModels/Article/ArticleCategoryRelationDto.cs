﻿using System.ComponentModel.DataAnnotations;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 分类关系DTO
    /// </summary>
    public class ArticleCategoryRelationDto
    {
        [Display(Name = "自增ID")]
        public long Id { get; set; }

        [Display(Name = "所属文章")]
        public long ArticleId { get; set; }

        [Display(Name = "所属分类")]
        public long CategoryId { get; set; }

    }
}