﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 导航菜单(显示)
    /// </summary>
    public class NavigationDto : NavigationEditDto
    {
        [Display(Name = "自增ID")]
        public int Id { get; set; }

        [Display(Name = "导航深度")]
        public int ClassLayer { get; set; } = 1;

        [Display(Name = "创建人")]
        [MaxLength(128)]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; }

        public List<NavigationDto> Children { get; set; } = new List<NavigationDto>();
    }

    /// <summary>
    /// 导航菜单(编辑)
    /// </summary>
    public class NavigationEditDto
    {
        [Display(Name = "父导航")]
        public int? ParentId { get; set; } = 0;

        [Display(Name = "所属频道")]
        public int? ChannelId { get; set; } = 0;

        [Display(Name = "导航标识")]
        [Required(ErrorMessage = "{0}不可为空")]
        [MaxLength(128, ErrorMessage = "{0}不可超出{1}字符")]
        public string Name { get; set; }

        [Display(Name = "标题")]
        [Required(ErrorMessage = "{0}不可为空")]
        [MaxLength(128, ErrorMessage = "{0}不可超出{1}字符")]
        public string Title { get; set; }

        [Display(Name = "副标题")]
        [MaxLength(128, ErrorMessage = "{0}不可超出{1}字符")]
        public string SubTitle { get; set; }

        [Display(Name = "图标地址")]
        [StringLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string IconUrl { get; set; }

        [Display(Name = "链接地址")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string LinkUrl { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        /// <summary>
        /// 状态0显示1隐藏
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;

        /// <summary>
        /// 系统默认0否1是
        /// </summary>
        [Display(Name = "系统默认")]
        public byte IsSystem { get; set; } = 0;

        [Display(Name = "备注说明")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string Remark { get; set; }

        [Display(Name = "控制器名称")]
        [Required(ErrorMessage = "{0}不可为空")]
        [MaxLength(128, ErrorMessage = "{0}不可超出{1}字符")]
        public string Controller { get; set; }

        [Display(Name = "权限资源")]
        [Required(ErrorMessage = "{0}不可为空")]
        [MaxLength(512, ErrorMessage = "{0}不可超出{1}字符")]
        public string Resource { get; set; }
    }

    /// <summary>
    /// 导航菜单(前台)
    /// </summary>
    public class NavigationMenuDto
    {
        /// <summary>
        /// 自增ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 频道ID
        /// </summary>
        public int ChannelId { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 菜单标题
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 链接
        /// </summary>
        public string Href { get; set; }
        /// <summary>
        /// 是否展开
        /// </summary>
        public bool IsExpand { get; set; } = false;
        /// <summary>
        /// 是否选中
        /// </summary>
        public bool IsSelected { get; set; } = false;
        /// <summary>
        /// 子菜单列表
        /// </summary>
        public List<NavigationMenuDto> Children { get; set; } = new List<NavigationMenuDto>();
    }

    /// <summary>
    /// 导航菜单(角色)
    /// </summary>
    public class NavigationRoleDto
    {
        /// <summary>
        /// 自增ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 控制器名称
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// 权限列表
        /// </summary>
        public List<NavigationResourceDto> Resource { get; set; } = new List<NavigationResourceDto>();

        /// <summary>
        /// 子菜单列表
        /// </summary>
        public List<NavigationRoleDto> Children { get; set; } = new List<NavigationRoleDto>();
    }

    /// <summary>
    /// 菜单权限资源
    /// </summary>
    public class NavigationResourceDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 是否选中
        /// </summary>
        public bool IsSelected { get; set; } = false;
    }
}
