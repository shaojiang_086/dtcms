﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 省市区(显示)
    /// </summary>
    public class AreasDto : AreasEditDto
    {
        [Display(Name = "自增ID")]
        public int Id { get; set; }

        /// <summary>
        /// 子地区列表
        /// </summary>
        public List<AreasDto> Children { get; set; } = new List<AreasDto>();
    }

    /// <summary>
    /// 省市区(编辑)
    /// </summary>
    public class AreasEditDto
    {
        [Display(Name = "父级地区")]
        public int ParentId { get; set; } = 0;

        [Display(Name = "地区名称")]
        [Required(ErrorMessage = "{0}不可为空")]
        [StringLength(128)]
        public string Title { get; set; }

        [Display(Name = "排序数字")]
        [Required(ErrorMessage = "{0}不可为空")]
        public int SortId { get; set; } = 99;
    }
}
