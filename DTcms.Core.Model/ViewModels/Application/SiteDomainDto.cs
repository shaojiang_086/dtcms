﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    public class SiteDomainDto
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string Domain { get; set; }
        public string Remark { get; set; }
    }
}
