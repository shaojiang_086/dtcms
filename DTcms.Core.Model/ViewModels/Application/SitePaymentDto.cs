﻿using DTcms.Core.Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 站点支付方式(显示)
    /// </summary>
    public class SitePaymentDto: SitePaymentEditDto
    {
        [Display(Name = "自增ID")]
        public int Id { get; set; }

        [Display(Name = "创建人")]
        [StringLength(128)]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 站点信息
        /// </summary>
        public SitesDto Site { get; set; }

        /// <summary>
        /// 支付方式信息
        /// </summary>
        public PaymentDto Payment { get; set; }
    }

    /// <summary>
    /// 站点支付方式(编辑)
    /// </summary>
    public class SitePaymentEditDto
    {
        [Display(Name = "所属站点")]
        [Required(ErrorMessage = "{0}不可为空")]
        public int SiteId { get; set; }

        [Display(Name = "支付方式")]
        [Required(ErrorMessage = "{0}不可为空")]
        public int PaymentId { get; set; }

        /// <summary>
        /// 接口类型
        /// 线下付款：cash
        /// 余额支付：balance
        /// 支付宝：pc(电脑)
        /// 微信：native(扫码)
        /// </summary>
        [Display(Name = "接口类型")]
        [Required(ErrorMessage = "{0}不可为空")]
        [StringLength(128)]
        public string Type { get; set; }

        [Display(Name = "标题")]
        [Required(ErrorMessage = "{0}不可为空")]
        [StringLength(128)]
        public string Title { get; set; }

        [Display(Name = "通讯密钥1")]
        public string Key1 { get; set; }

        [Display(Name = "通讯密钥2")]
        public string Key2 { get; set; }

        [Display(Name = "通讯密钥3")]
        public string Key3 { get; set; }

        [Display(Name = "通讯密钥4")]
        public string Key4 { get; set; }

        [Display(Name = "通讯密钥5")]
        public string Key5 { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;
    }
}
