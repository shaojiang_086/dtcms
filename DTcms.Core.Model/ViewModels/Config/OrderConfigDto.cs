﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 订单设置
    /// </summary>
    public class OrderConfigDto
    {
        /// <summary>
        /// 税费类型1百分比2固定金额
        /// </summary>
        public int TaxType { get; set; }

        /// <summary>
        /// 百分比取值范围：0-100，固定金额单位为“元”
        /// </summary>
        public decimal TaxaMount { get; set; }

        /// <summary>
        /// 订单确认通知0关闭1短信2邮件
        /// </summary>
        public int ConfirmMsg { get; set; }

        /// <summary>
        /// 通知模板别名
        /// </summary>
        public string ConfirmCallindex { get; set; }

        /// <summary>
        /// 订单发货通知0关闭1短信2邮件
        /// </summary>
        public int ExpressMsg { get; set; }

        /// <summary>
        /// 通知模板别名
        /// </summary>
        public string ExpressCallindex { get; set; }

        /// <summary>
        /// 订单完成通知0关闭1短信2邮件
        /// </summary>
        public int CompleteMsg { get; set; }

        /// <summary>
        /// 通知模板别名
        /// </summary>
        public string CompleteCallindex { get; set; }

        /// <summary>
        /// 快递100API地址
        /// </summary>
        public string KuaidiApi { get; set; }

        /// <summary>
        /// 快递100Key
        /// </summary>
        public string KuaidiKey { get; set; }

        /// <summary>
        /// 快递100授权码
        /// </summary>
        public string KuaidiCust { get; set; }

        /// <summary>
        /// 物流跟踪返回0json字符串1xml对象2html表格3文本
        /// </summary>
        public int KuaidiShow { get; set; }

        /// <summary>
        /// 跟踪信息排序asc：按时间由旧到新,desc：按时间由新到旧
        /// </summary>
        public string KuaidiOrder { get; set; }
    }
}
