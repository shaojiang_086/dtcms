﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.ViewModels
{
    /// <summary>
    /// 手机验证码登录
    /// </summary>
    public class LoginPhoneDto : VerifyCode
    {
        [Display(Name = "手机号码")]
        [Required(ErrorMessage = "{0}不能为空")]
        [RegularExpression(@"^(13|14|15|16|18|19|17)\d{9}$")]
        public string Phone { get; set; }
    }
}
