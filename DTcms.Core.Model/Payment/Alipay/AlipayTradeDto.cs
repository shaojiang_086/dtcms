﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.Alipay
{
    /// <summary>
    /// 统一下单实体
    /// </summary>
    public class AlipayTradeDto
    {
        /// <summary>
        /// 商户订单号
        /// </summary>
        [Display(Name = "订单号")]
        [Required(ErrorMessage = "{0}不能为空")]
        public string OutTradeNo { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        [Display(Name = "商品描述")]
        [Required(ErrorMessage = "{0}不能为空")]
        public string Description { get; set; }

        /// <summary>
        /// 支付成功后跳转链接
        /// </summary>
        [Display(Name = "跳转链接")]
        [Required(ErrorMessage = "{0}不能为空")]
        public string ReturnUri { get; set; }

        /// <summary>
        /// 支付方式(待赋值)
        /// </summary>
        [Display(Name = "支付方式")]
        public int PaymentId { get; set; }

        /// <summary>
        /// 订单总金额(元)(待赋值)
        /// </summary>
        [Display(Name = "总金额(元)")]
        public decimal Total { get; set; } = 0M;
    }

    /// <summary>
    /// 支付宝账户实体
    /// </summary>
    public class AlipayAccountDto
    {
        /// <summary>
        /// 站点ID
        /// </summary>
        public int SiteId { get; set; }

        /// <summary>
        /// 应用Id
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 普通公钥：支付宝公钥 RSA公钥
        /// 为支付宝开放平台-支付宝公钥
        /// </summary>
        public string AlipayPublicKey { get; set; }

        /// <summary>
        /// 普通公钥：应用私钥 RSA私钥
        /// 为“支付宝开放平台开发助手”所生成的应用私钥
        /// </summary>
        public string AppPrivateKey { get; set; }

        /// <summary>
        /// 支付宝服务器主动通知商户服务器里指定的API接口
        /// </summary>
        public string NotifyUrl { get; set; }

        /// <summary>
        /// 支付方式接口类型
        /// </summary>
        public string NotifyType { get; set; }
    }

    #region 返回客户端的参数实体
    /// <summary>
    /// 电脑、手机支付返回实体
    /// </summary>
    public class AlipayPageParamDto
    {
        /// <summary>
        /// 跳转的URL
        /// </summary>
        public string Url { get; set; }
    }
    #endregion
}
