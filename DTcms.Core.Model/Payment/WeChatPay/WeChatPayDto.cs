﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTcms.Core.Model.WeChatPay
{
    #region 微信支付API返回实体
    /// <summary>
    /// 扫码下单返回结果
    /// </summary>
    public class WeChatPayNativeResultDto
    {
        /// <summary>
        /// 此URL用于生成支付二维码，然后提供给用户扫码支付
        /// </summary>
        [JsonProperty("code_url")]
        public string CodeUrl { get; set; }
    }
    #endregion

    #region 返回客户端的参数实体
    /// <summary>
    /// 扫码返回参数
    /// https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_4_1.shtml
    /// </summary>
    public class WeChatPayNativeParamDto
    {
        /// <summary>
        /// 二维码BASE64图片
        /// </summary>
        public string CodeData { get; set; }
    }
    #endregion

    /// <summary>
    /// 微信公共下单实体
    /// </summary>
    public class WeChatPayDto
    {
        /// <summary>
        /// 商户订单号
        /// </summary>
        [Display(Name = "订单号")]
        [Required(ErrorMessage = "{0}不能为空")]
        public string OutTradeNo { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        [Display(Name = "商品描述")]
        [Required(ErrorMessage = "{0}不能为空")]
        public string Description { get; set; }

        /// <summary>
        /// 支付方式(待赋值)
        /// </summary>
        [Display(Name = "支付方式")]
        public int PaymentId { get; set; }

        /// <summary>
        /// 订单总金额(元)
        /// </summary>
        [Display(Name = "总金额(元)")]
        public decimal Total { get; set; } = 0M;

        /// <summary>
        /// 支付成功后跳转链接
        /// </summary>
        [Display(Name = "跳转链接")]
        public string ReturnUri { get; set; }

        /// <summary>
        /// 授权CODE
        /// </summary>
        [Display(Name = "授权Code")]
        public string Code { get; set; }
    }

    /// <summary>
    /// 微信账户实体
    /// </summary>
    public class WeChatPayAccountDto
    {
        /// <summary>
        /// 站点ID
        /// </summary>
        public int SiteId { get; set; }

        /// <summary>
        /// 微信AppId
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 微信AppSecret
        /// </summary>
        public string AppSecret { get; set; }

        /// <summary>
        /// 商户号
        /// </summary>
        public string MchId { get; set; }

        /// <summary>
        /// CA证书相对路径
        /// </summary>
        public string CertPath { get; set; }

        /// <summary>
        /// APIV3密钥
        /// </summary>
        public string Apiv3Key { get; set; }

        /// <summary>
        /// 通知相对地址
        /// </summary>
        public string NotifyUrl { get; set; }
    }

}
