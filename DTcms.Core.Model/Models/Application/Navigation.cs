﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 导航菜单
    /// </summary>
    public class Navigation
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "父导航")]
        public int ParentId { get; set; } = 0;

        [Display(Name = "所属频道")]
        public int ChannelId { get; set; } = 0;

        [Display(Name = "导航标识")]
        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Display(Name = "标题")]
        [StringLength(128)]
        public string Title { get; set; }

        [Display(Name = "副标题")]
        [StringLength(128)]
        public string SubTitle { get; set; }

        [Display(Name = "图标地址")]
        [StringLength(512)]
        public string IconUrl { get; set; }

        [Display(Name = "链接地址")]
        [StringLength(512)]
        public string LinkUrl { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        /// <summary>
        /// 状态0显示1隐藏
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;

        /// <summary>
        /// 系统默认0否1是
        /// </summary>
        [Display(Name = "系统默认")]
        public byte IsSystem { get; set; } = 0;

        [Display(Name = "备注说明")]
        [StringLength(512)]
        public string Remark { get; set; }

        [Display(Name = "控制器名称")]
        [StringLength(128)]
        public string Controller { get; set; }

        [Display(Name = "权限资源")]
        [StringLength(512)]
        public string Resource { get; set; }

        [Display(Name = "创建人")]
        [StringLength(128)]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;
    }
}
