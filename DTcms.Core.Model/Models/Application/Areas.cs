﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 全国省市区
    /// </summary>
    public class Areas
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "父级ID")]
        public int ParentId { get; set; } = 0;

        [Display(Name = "标题")]
        [Required]
        [StringLength(128)]
        public string Title { get; set; } = string.Empty;

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;
    }
}
