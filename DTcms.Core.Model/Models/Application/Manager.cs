﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 管理员信息
    /// </summary>
    public class Manager
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "所属用户")]
        [ForeignKey("ApplicatonUser")]
        public int UserId { get; set; }

        [Display(Name = "会员头像")]
        [StringLength(512)]
        public string Avatar { get; set; }

        [Display(Name = "姓名")]
        [StringLength(30)]
        public string RealName { get; set; }

        [Display(Name = "启用发布审核")]
        public byte IsAudit { get; set; } = 0;

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "最后登录IP")]
        [StringLength(128)]
        public string LastIp { get; set; }

        [Display(Name = "最后登录时间")]
        public DateTime? LastTime { get; set; }

        /// <summary>
        /// 用户信息
        /// </summary>
        public ApplicationUser ApplicatonUser { get; set; }
    }
}
