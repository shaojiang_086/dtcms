﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 系统配置
    /// </summary>
    public class SysConfig
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "配置类型")]
        [Required]
        [StringLength(128)]
        public string Type { get; set; }

        [Display(Name = "Json数据")]
        public string JsonData { get; set; }
    }
}
