﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 站点频道
    /// </summary>
    public class SiteChannel
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "所属站点")]
        [ForeignKey("Site")]
        public int SiteId { get; set; }

        [Display(Name = "频道名称")]
        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Display(Name = "频道标题")]
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Display(Name = "是否开启评论")]
        [Range(0, 9)]
        public byte IsComment { get; set; } = 0;

        [Display(Name = "是否开启相册")]
        [Range(0, 9)]
        public byte IsAlbum { get; set; } = 0;

        [Display(Name = "是否开启附件")]
        [Range(0, 9)]
        public byte IsAttach { get; set; } = 0;

        [Display(Name = "是否允许投稿")]
        [Range(0, 9)]
        public byte IsContribute { get; set; } = 0;

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        /// <summary>
        /// 状态0正常1禁用
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;

        [Display(Name = "创建人")]
        [StringLength(128)]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 字段列表
        /// </summary>
        public ICollection<SiteChannelField> Fields { get; set; } = new List<SiteChannelField>();

        /// <summary>
        /// 站点信息
        /// </summary>
        public Sites Site { get; set; }
    }
}
