﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 导航菜单生成模型
    /// </summary>
    public class NavigationModel
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "父节点")]
        public int ParentId { get; set; } = 0;

        [Display(Name = "导航类型")]
        [StringLength(128)]
        public string NavType { get; set; }

        [Display(Name = "导航标识")]
        [StringLength(128)]
        public string Name { get; set; }

        [Display(Name = "标题")]
        [StringLength(128)]
        public string Title { get; set; }

        [Display(Name = "副标题")]
        [StringLength(128)]
        public string SubTitle { get; set; }

        [Display(Name = "图标地址")]
        [StringLength(512)]
        public string IconUrl { get; set; }

        [Display(Name = "链接地址")]
        [StringLength(512)]
        public string LinkUrl { get; set; }

        [Display(Name = "控制器名称")]
        [StringLength(128)]
        public string Controller { get; set; }

        [Display(Name = "资源权限")]
        [StringLength(512)]
        public string Resource { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;
    }
}
