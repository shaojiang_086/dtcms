﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 扩展字段
    /// </summary>
    public class SiteChannelField
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "所属频道")]
        [ForeignKey("SiteChannel")]
        public int ChannelId { get; set; }

        [Display(Name = "字段名")]
        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Display(Name = "标题")]
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Display(Name = "控件类型")]
        [Required]
        [StringLength(128)]
        public string ControlType { get; set; }

        [Display(Name = "选项列表")]
        public string ItemOption { get; set; }

        [Display(Name = "默认值")]
        [StringLength(512)]
        public string DefaultValue { get; set; }

        [Display(Name = "是否密码框")]
        [Range(0, 9)]
        public byte IsPassword { get; set; } = 0;

        /// <summary>
        /// 是否必填0非必填1必填
        /// </summary>
        [Display(Name = "是否必填")]
        [Range(0, 9)]
        public byte IsRequired { get; set; } = 0;

        /// <summary>
        /// 编辑器0标准型1简洁型
        /// </summary>
        [Display(Name = "编辑器")]
        [Range(0, 9)]
        public byte EditorType { get; set; } = 0;

        [Display(Name = "验证提示信息")]
        [StringLength(255)]
        public string ValidTipMsg { get; set; }

        [Display(Name = "验证失败提示信息")]
        [StringLength(255)]
        public string ValidErrorMsg { get; set; }

        [Display(Name = "验证正则表达式")]
        [StringLength(255)]
        public string ValidPattern { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        /// <summary>
        /// 频道信息
        /// </summary>
        public SiteChannel SiteChannel { get; set; }
    }
}
