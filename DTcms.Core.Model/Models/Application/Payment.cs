﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 支付方式
    /// </summary>
    public class Payment
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "支付名称")]
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Display(Name = "显示图片")]
        [StringLength(512)]
        public string ImgUrl { get; set; }

        [Display(Name = "备注说明")]
        [StringLength(512)]
        public string Remark { get; set; }

        /// <summary>
        /// 收款类型0线下1余额2微信3支付宝
        /// </summary>
        [Display(Name = "收款类型")]
        [Range(0, 9)]
        public byte Type { get; set; } = 0;

        /// <summary>
        /// 手续费类型1百分比2固定金额
        /// </summary>
        [Display(Name = "手续费类型")]
        [Range(0, 9)]
        public byte PoundageType { get; set; } = 0;

        [Display(Name = "手续费金额")]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal PoundageAmount { get; set; } = 0;

        [Display(Name = "支付接口页面")]
        [StringLength(512)]
        public string PayUrl { get; set; }

        [Display(Name = "支付通知页面")]
        [StringLength(512)]
        public string NotifyUrl { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        /// <summary>
        /// 状态0启用1关闭
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;
    }
}
