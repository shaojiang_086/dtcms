﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 站点列表
    /// </summary>
    public class Sites
    {
        [Display(Name ="自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "父站点ID")]
        public int ParentId { get; set; } = 0;

        [Display(Name = "站点名称")]
        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Display(Name = "标题")]
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Display(Name = "模板目录名")]
        [Required]
        [StringLength(128)]
        public string DirPath { get; set; }

        [Display(Name = "是否默认站")]
        public byte IsDefault { get; set; } = 0;

        [Display(Name = "网站LOGO")]
        [StringLength(512)]
        public string Logo { get; set; }

        [Display(Name = "单位名称")]
        [StringLength(128)]
        public string Company { get; set; }
        
        [Display(Name = "单位地址")]
        [StringLength(512)]
        public string Address { get; set; }

        [Display(Name = "联系电话")]
        [StringLength(128)]
        public string Tel { get; set; }

        [Display(Name = "传真号码")]
        [StringLength(128)]
        public string Fax { get; set; }

        [Display(Name = "电子邮箱")]
        [StringLength(128)]
        public string Email { get; set; }

        [Display(Name = "备案号")]
        [StringLength(128)]
        public string Crod { get; set; }

        [Display(Name = "版权信息")]
        [StringLength(1024)]
        public string Copyright { get; set; }

        [Display(Name = "SEO标题")]
        [StringLength(512)]
        public string SeoTitle { get; set; }

        [Display(Name = "SEO关健字")]
        [StringLength(512)]
        public string SeoKeyword { get; set; }

        [Display(Name = "SEO描述")]
        [StringLength(512)]
        public string SeoDescription { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        /// <summary>
        /// 状态0正常1禁用
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;

        [Display(Name = "创建人")]
        [StringLength(128)]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 域名列表
        /// </summary>
        public ICollection<SiteDomain> SiteDomains { get; set; } = new List<SiteDomain>();
    }
}
