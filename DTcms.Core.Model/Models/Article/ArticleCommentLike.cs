﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 评论点赞
    /// </summary>
    public class ArticleCommentLike
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属评论")]
        [ForeignKey("Comment")]
        public long CommentId { get; set; }

        [Display(Name = "所属用户")]
        public int UserId { get; set; }

        [Display(Name = "点赞时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 评论信息
        /// </summary>
        public ArticleComment Comment { get; set; }
    }
}
