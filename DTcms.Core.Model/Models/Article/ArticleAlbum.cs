﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 文章相册
    /// </summary>
    public class ArticleAlbum
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属文章")]
        [ForeignKey("Article")]
        public long ArticleId { get; set; }

        [Display(Name = "缩略图")]
        [StringLength(512)]
        public string ThumbPath { get; set; }

        [Display(Name = "原图")]
        [StringLength(512)]
        public string OriginalPath { get; set; }

        [Display(Name = "图片描述")]
        [StringLength(512)]
        public string Remark { get; set; }

        /// <summary>
        /// 排序数字
        /// </summary>
        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 文章信息
        /// </summary>
        public Articles Article { get; set; }

    }
}
