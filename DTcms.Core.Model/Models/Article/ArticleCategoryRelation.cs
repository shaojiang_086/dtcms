﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 文章类别关联
    /// </summary>
    public class ArticleCategoryRelation
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属文章")]
        [ForeignKey("Article")]
        public long ArticleId { get; set; }

        [Display(Name = "所属分类")]
        [ForeignKey("ArticleCategory")]
        public long CategoryId { get; set; }

        /// <summary>
        /// 文章信息
        /// </summary>
        public Articles Article { get; set; }
        
        /// <summary>
        /// 类别信息
        /// </summary>
        public ArticleCategory ArticleCategory { get; set; }
    }
}
