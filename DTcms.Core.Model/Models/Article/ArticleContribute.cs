﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 文章投稿
    /// </summary>
    public class ArticleContribute
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属站点")]
        public int SiteId { get; set; }

        [Display(Name = "所属频道")]
        [ForeignKey("SiteChannel")]
        public int ChannelId { get; set; }

        [Display(Name = "标题")]
        [Required]
        [StringLength(512)]
        public string Title { get; set; }

        [Display(Name = "来源")]
        [StringLength(128)]
        public string Source { get; set; }

        [Display(Name = "作者")]
        [StringLength(128)]
        public string Author { get; set; }

        [Display(Name = "图片地址")]
        [StringLength(512)]
        public string ImgUrl { get; set; }

        [Display(Name = "扩展字段集合")]
        public string FieldMeta { get; set; }

        [Display(Name = "相册扩展字段值")]
        public string AlbumMeta { get; set; }

        [Display(Name = "附件扩展字段值")]
        public string AttachMeta { get; set; }

        [Display(Name = "内容")]
        [Column(TypeName = "text")]
        public string Content { get; set; }

        [Display(Name = "投稿用户")]
        public int UserId { get; set; } = 0;

        [Display(Name = "用户名")]
        [StringLength(128)]
        public string UserName { get; set; }
        /// <summary>
        /// 状态0待审1通过2返回
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "更新人")]
        [StringLength(128)]
        public string UpdateBy { get; set; }

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 频道信息
        /// </summary>
        public SiteChannel SiteChannel { get; set; }
    }
}
