﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 文章主表
    /// </summary>
    public class Articles
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属站点")]
        public int SiteId { get; set; }

        [Display(Name = "所属频道")]
        [ForeignKey("SiteChannel")]
        public int ChannelId { get; set; }

        [Display(Name = "调用别名")]
        [StringLength(128)]
        public string CallIndex { get; set; }

        [Display(Name = "标题")]
        [Required]
        [StringLength(512)]
        public string Title { get; set; }

        [Display(Name = "来源")]
        [StringLength(128)]
        public string Source { get; set; }

        [Display(Name = "作者")]
        [StringLength(128)]
        public string Author { get; set; }

        [Display(Name = "外部链接")]
        [StringLength(512)]
        public string LinkUrl { get; set; }

        [Display(Name = "图片地址")]
        [StringLength(512)]
        public string ImgUrl { get; set; }

        [Display(Name = "SEO标题")]
        [StringLength(255)]
        public string SeoTitle { get; set; }

        [Display(Name = "SEO关健字")]
        [StringLength(512)]
        public string SeoKeyword { get; set; }

        [Display(Name = "SEO描述")]
        [StringLength(512)]
        public string SeoDescription { get; set; }

        [Display(Name = "内容摘要")]
        [StringLength(255)]
        public string Zhaiyao { get; set; }

        [Display(Name = "内容")]
        [Column(TypeName = "text")]
        public string Content { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        [Display(Name = "浏览总数")]
        public int Click { get; set; } = 0;

        /// <summary>
        /// 状态0正常1待审2已删
        /// </summary>
        [Display(Name = "状态")]
        [Range(0, 9)]
        public byte Status { get; set; } = 0;

        /// <summary>
        /// 评论0禁止1允许
        /// </summary>
        [Display(Name = "是否评论")]
        [Range(0, 9)]
        public byte IsComment { get; set; } = 0;

        [Display(Name = "评论总数")]
        public int CommentCount { get; set; } = 0;

        [Display(Name = "点赞总数")]
        public int LikeCount { get; set; } = 0;

        [Display(Name = "创建人")]
        [StringLength(128)]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "更新人")]
        [StringLength(128)]
        public string UpdateBy { get; set; }

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 扩展字段列表
        /// </summary>
        public ICollection<ArticleFieldValue> ArticleFields { get; set; } = new List<ArticleFieldValue>();

        /// <summary>
        /// 类别关联列表
        /// </summary>
        public ICollection<ArticleCategoryRelation> ArticleCategoryRelations { get; set; } = new List<ArticleCategoryRelation>();

        /// <summary>
        /// 标签关联列表
        /// </summary>
        public ICollection<ArticleLabelRelation> ArticleLabelRelations { get; set; } = new List<ArticleLabelRelation>();

        /// <summary>
        /// 相册列表
        /// </summary>
        public ICollection<ArticleAlbum> ArticleAlbums { get; set; } = new List<ArticleAlbum>();

        /// <summary>
        /// 附件列表
        /// </summary>
        public ICollection<ArticleAttach> ArticleAttachs { get; set; } = new List<ArticleAttach>();

        /// <summary>
        /// 频道信息
        /// </summary>
        public SiteChannel SiteChannel { get; set; }
    }
}
