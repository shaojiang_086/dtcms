﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 文章标签关联
    /// </summary>
    public class ArticleLabelRelation
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属文章")]
        [ForeignKey("Article")]
        public long ArticleId { get; set; }

        [Display(Name = "所属标签")]
        [ForeignKey("ArticleLabel")]
        public long LabelId { get; set; }

        /// <summary>
        /// 文章信息
        /// </summary>
        public Articles Article { get; set; }

        /// <summary>
        /// 标签信息
        /// </summary>
        public ArticleLabel ArticleLabel { get; set; }
    }
}
