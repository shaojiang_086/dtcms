﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 文章类别
    /// </summary>
    public class ArticleCategory
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属站点")]
        public int SiteId { get; set; }

        [Display(Name = "所属频道")]
        public int ChannelId { get; set; }

        [Display(Name = "所属父类")]
        public long ParentId { get; set; } = 0;

        [Display(Name = "调用别名")]
        [StringLength(128)]
        public string CallIndex { get; set; }

        [Display(Name = "类别深度")]
        public int ClassLayer { get; set; } = 1;

        [Display(Name = "标题")]
        [StringLength(128)]
        public string Title { get; set; }

        [Display(Name = "图片地址")]
        [StringLength(512)]
        public string ImgUrl { get; set; }

        [Display(Name = "外部链接")]
        [StringLength(512)]
        public string LinkUrl { get; set; }

        [Display(Name = "内容介绍")]
        [Column(TypeName = "text")]
        public string Content { get; set; }

        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        [Display(Name = "SEO标题")]
        [StringLength(512)]
        public string SeoTitle { get; set; }

        [Display(Name = "SEO关健字")]
        [StringLength(512)]
        public string SeoKeyword { get; set; }

        [Display(Name = "SEO描述")]
        [StringLength(512)]
        public string SeoDescription { get; set; }

        /// <summary>
        /// 状态0正常1禁用
        /// </summary>
        [Display(Name = "状态")]
        [Range(0, 9)]
        public byte Status { get; set; } = 0;

        [Display(Name = "创建人")]
        [StringLength(128)]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "更新人")]
        [StringLength(128)]
        public string UpdateBy { get; set; }

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 类别关联列表
        /// </summary>
        public ICollection<ArticleCategoryRelation> ArticleCategoryRelations { get; set; } = new List<ArticleCategoryRelation>();
    }
}
