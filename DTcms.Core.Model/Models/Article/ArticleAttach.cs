﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 文章附件
    /// </summary>
    public class ArticleAttach
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属文章")]
        [ForeignKey("Article")]
        public long ArticleId { get; set; }

        [Display(Name = "文件名")]
        [StringLength(255)]
        public string FileName { get; set; }

        [Display(Name = "文件路径")]
        [StringLength(512)]
        public string FilePath { get; set; }

        [Display(Name = "文件大小")]
        public int FileSize { get; set; }

        [Display(Name = "扩展名")]
        [StringLength(128)]
        public string FileExt { get; set; }

        [Display(Name = "下载所需积分")]
        public int Point { get; set; } = 0;

        [Display(Name = "下载次数")]
        public int DownCount { get; set; }

        /// <summary>
        /// 排序数字
        /// </summary>
        [Display(Name = "排序数字")]
        public int SortId { get; set; } = 99;

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 文章信息
        /// </summary>
        public Articles Article { get; set; }

    }
}
