﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 文章扩展字段值
    /// </summary>
    public class ArticleFieldValue
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属文章")]
        [ForeignKey("Article")]
        public long ArticleId { get; set; }

        [Display(Name = "所属字段")]
        //[ForeignKey("SiteChannelFieldId")]
        public long FieldId { get; set; }

        [Display(Name = "字段名")]
        [StringLength(128)]
        public string FieldName { get; set; }

        [Display(Name = "字段值")]
        public string FieldValue { get; set; }

        /// <summary>
        /// 文章信息
        /// </summary>
        public Articles Article { get; set; }
    }
}
