﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 文章点赞
    /// </summary>
    public class ArticleLike
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属文章")]
        [ForeignKey("Article")]
        public long ArticleId { get; set; }

        [Display(Name = "所属用户")]
        public int UserId { get; set; }

        [Display(Name = "点赞时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 文章信息
        /// </summary>
        public Articles Article { get; set; }
    }
}
