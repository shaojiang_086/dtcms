﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 会员积分记录
    /// </summary>
    public class MemberPointLog
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属用户")]
        public int UserId { get; set; }

        [Display(Name = "增减积分")]
        public int Value { get; set; } = 0;

        [Display(Name = "备注说明")]
        [StringLength(512)]
        public string Remark { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;
    }
}
