﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    public class MemberAttachLog
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "所属用户")]
        public int UserId { get; set; }

        [Display(Name = "附件ID")]
        [ForeignKey("ArticleAttach")]
        public long AttachId { get; set; }

        [Display(Name = "文件名称")]
        [StringLength(255)]
        public string FileName { get; set; }

        [Display(Name = "下载时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 附件信息
        /// </summary>
        public ArticleAttach ArticleAttach { get; set; }
    }
}
