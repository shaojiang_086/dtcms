﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 会员组
    /// </summary>
    public class MemberGroup
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "组名称")]
        [StringLength(128)]
        public string Title { get; set; }

        [Display(Name = "最小经验值")]
        public int MinExp { get; set; } = 0;

        [Display(Name = "最大经验值")]
        public int MaxExp { get; set; } = 0;

        [Display(Name = "预存款")]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; } = 0;

        [Display(Name = "购物折扣")]
        [Range(1, 100)]
        public int Discount { get; set; } = 100;

        [Display(Name = "是否参与升级")]
        public byte IsUpgrade { get; set; } = 1;

        [Display(Name = "是否默认")]
        public byte IsDefault { get; set; } = 0;

        [Display(Name = "创建人")]
        [StringLength(128)]
        public string AddBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "更新人")]
        [StringLength(128)]
        public string UpdateBy { get; set; }

        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }
    }
}
