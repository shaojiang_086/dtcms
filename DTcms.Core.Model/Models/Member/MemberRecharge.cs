﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 会员充值
    /// </summary>
    public class MemberRecharge
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "充值用户")]
        public int UserId { get; set; }

        [Display(Name = "充值单号")]
        [StringLength(128)]
        public string RechargeNo { get; set; }

        [Display(Name = "支付方式")]
        public int PaymentId { get; set; }

        [Display(Name = "充值金额")]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; } = 0;

        /// <summary>
        /// 状态0未完成1已完成
        /// </summary>
        [Display(Name = "状态")]
        public byte Status { get; set; } = 0;

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "完成时间")]
        public DateTime? CompleteTime { get; set; }
    }
}
