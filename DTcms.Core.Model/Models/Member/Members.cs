﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 会员信息
    /// </summary>
    public class Members
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "所属站点")]
        public int SiteId { get; set; }

        [Display(Name = "所属用户")]
        [ForeignKey("ApplicatonUser")]
        public int UserId { get; set; }

        [Display(Name = "会员组")]
        [ForeignKey("MemberGroup")]
        public int GroupId { get; set; }

        [Display(Name = "会员头像")]
        [StringLength(512)]
        public string Avatar { get; set; }

        [Display(Name = "姓名")]
        [StringLength(30)]
        public string RealName { get; set; }

        [Display(Name = "性别")]
        [StringLength(30)]
        public string Sex { get; set; }

        [Display(Name = "生日")]
        public DateTime? Birthday { get; set; }

        [Display(Name = "余额")]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; } = 0;

        [Display(Name = "积分")]
        public int Point { get; set; } = 0;

        [Display(Name = "经验值")]
        public int Exp { get; set; } = 0;

        [Display(Name = "注册IP")]
        [StringLength(128)]
        public string RegIp { get; set; }

        [Display(Name = "注册时间")]
        public DateTime RegTime { get; set; } = DateTime.Now;

        [Display(Name = "最后登录IP")]
        [StringLength(128)]
        public string LastIp { get; set; }

        [Display(Name = "最后登录时间")]
        public DateTime? LastTime { get; set; }

        /// <summary>
        /// 用户信息
        /// </summary>
        public ApplicationUser User { get; set; }

        /// <summary>
        /// 会员组
        /// </summary>
        public MemberGroup Group { get; set; }
    }
}
