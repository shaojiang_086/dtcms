﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DTcms.Core.Model.Models
{
    /// <summary>
    /// 站内消息
    /// </summary>
    public class MemberMessage
    {
        [Display(Name = "自增ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "接收用户")]
        public int UserId { get; set; }

        [Display(Name = "标题")]
        [StringLength(128)]
        public string Title { get; set; }

        [Display(Name = "内容")]
        [Column(TypeName = "text")]
        public string Content { get; set; }

        [Display(Name = "创建时间")]
        public DateTime AddTime { get; set; } = DateTime.Now;

        [Display(Name = "是否已读")]
        public byte IsRead { get; set; } = 0;

        [Display(Name = "读取时间")]
        public DateTime? ReadTime { get; set; }
    }
}
