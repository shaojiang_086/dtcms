﻿using DTcms.Core.Common.Emum;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Common.Helper
{
    [Serializable]
    public class ResponseException : ApplicationException
    {
        /// <summary>
        /// 状态码
        /// </summary>
        private readonly int _code;

        /// <summary>
        /// 错误码，当为0时，代表正常
        /// </summary>
        private readonly ErrorCode _errorCode;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ResponseException() : base("服务器繁忙，请稍后再试!")
        {
            _errorCode = ErrorCode.Fail;
            _code = 400;
        }

        public ResponseException(string message = "服务器繁忙，请稍后再试!", ErrorCode errorCode = ErrorCode.Fail, int code = 400) : base(message)
        {
            this._errorCode = errorCode;
            _code = code;

        }

        /// <summary>
        /// 
        /// </summary>
        public int GetCode()
        {
            return _code;
        }

        public ErrorCode GetErrorCode()
        {
            return _errorCode;
        }
    }
}
