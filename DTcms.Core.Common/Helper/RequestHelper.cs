﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Http;
using RestSharp;

namespace DTcms.Core.Common.Helper
{
    /// <summary>
    /// 基于RestSharp帮助类
    /// </summary>
    public class RequestHelper
    {
        /// <summary>
        /// Get请求
        /// </summary>
        /// <param name="url">请求地址</param>
        public static string Get(string url)
        {
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                return response.Content;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        /// <summary>
        /// Get请求
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="headers">头部键值对</param>
        public static (int statusCode, string reHeaders, string reBody) Get(string url, ICollection<KeyValuePair<string, string>> headers)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeaders(headers);
            IRestResponse response = client.Execute(request);
            var statusCode = (int)response.StatusCode;
            var reHeaders = response.Headers.ToJson();
            var reBody = response.Content;
            return (statusCode, reHeaders, reBody);
        }

        /// <summary>
        /// Post请求
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="content">POST内容</param>
        /// <param name="contentType">application/x-www-form-urlencoded</param>
        public static string Post(string url, string content, string contentType= "application/json")
        {
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddParameter(contentType, content, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                return response.Content;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        /// <summary>
        /// Post请求
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="headers">头部键值对</param>
        /// <param name="content">POST内容</param>
        /// <param name="contentType">默认JSON</param>
        public static (int statusCode, string reHeaders, string reBody) Post(string url,
            ICollection<KeyValuePair<string, string>> headers, string content, string contentType = "application/json")
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeaders(headers);
            request.AddParameter(contentType, content, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var statusCode = (int)response.StatusCode;
            var reHeaders = response.Headers.ToJson();
            var reBody = response.Content;
            return (statusCode, reHeaders, reBody);
        }

    }
}
