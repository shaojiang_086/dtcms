﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Common.Helper
{
    public class DTKeys
    {
        //系统版本===================================================
        /// <summary>
        /// 版本号全称
        /// </summary>
        public const string ASSEMBLY_VERSION = "1.0.0";
        /// <summary>
        /// 版本年号
        /// </summary>
        public const string ASSEMBLY_YEAR = "2021";

        //Directory==================================================
        /// <summary>
        /// 静态文件目录名
        /// </summary>
        public const string DIRECTORY_WEB_PATH = "wwwroot";
    }
}
