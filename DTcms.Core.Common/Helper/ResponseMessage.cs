﻿using DTcms.Core.Common.Emum;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Common.Helper
{
    /// <summary>
    /// 请求响应统一消息
    /// </summary>
    public class ResponseMessage
    {
        /// <summary>
        ///错误码
        /// </summary>
        public ErrorCode Code { get; set; }

        /// <summary>
        ///错误信息
        /// </summary>
        public object Message { get; set; }

        /// <summary>
        ///请求地址
        /// </summary>
        public string Request { get; set; }

        public ResponseMessage() { }

        public ResponseMessage(ErrorCode errorCode)
        {
            Code = errorCode;
        }
        public ResponseMessage(ErrorCode errorCode, object message)
        {
            Code = errorCode;
            Message = message ?? throw new ArgumentNullException(nameof(message));
        }
        public ResponseMessage(ErrorCode errorCode, object message, HttpContext httpContext)
        {
            Code = errorCode;
            Message = message;
            Request = httpContext.Request.Method + " " + httpContext.Request.Path;
        }

        public static ResponseMessage Success(string message = "操作成功")
        {
            return new ResponseMessage(ErrorCode.Success, message);
        }

        public static ResponseMessage Error(string message = "操作失败")
        {
            return new ResponseMessage(ErrorCode.Fail, message);
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings()
            {
                ContractResolver = new DefaultContractResolver()
                {
                    NamingStrategy = new SnakeCaseNamingStrategy()
                }
            });
        }
    }
}
