﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Common.Emum
{
    /// <summary>
    /// 系统配置类型枚举
    /// </summary>
    public enum ConfigType
    {
        /// <summary>
        /// 系统设置
        /// </summary>
        SysConfig,
        /// <summary>
        /// 订单设置
        /// </summary>
        OrderConfig,
        /// <summary>
        /// 会员设置
        /// </summary>
        MemberConfig,
        /// <summary>
        /// 上传配置
        /// </summary>
        UploadConfig,
    }
}
