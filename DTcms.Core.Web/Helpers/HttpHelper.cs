﻿using DTcms.Core.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTcms.Core.Web.Helpers
{
    public class HttpHelper
    {
        /// <summary>
        /// 发送GET请求
        /// </summary>
        /// <typeparam name="T">转换类型</typeparam>
        /// <param name="url">接口地址</param>
        /// <returns>T类型</returns>
        public static T Get<T>(string url, bool isCache = true) where T : class
        {
            //读取缓存
            if (isCache)
            {
                var model = MemoryCacheHelper.Get<T>(url);
                if (model != null)
                {
                    return model;
                }
            }
            var baseApi = Appsettings.GetValue(new string[] { "ApiConfig", "BaseUrl" });
            var expired = int.Parse(Appsettings.GetValue(new string[] { "ApiConfig", "Expired" }));
            var jsonData = RequestHelper.Get($"{baseApi}{url}");
            var result = JsonHelper.ToJson<T>(jsonData);
            //存入缓存
            if (isCache && result != null)
            {
                MemoryCacheHelper.Set(url, result, new TimeSpan(0, 0, expired));
            }
            return result;
        }

    }
}
