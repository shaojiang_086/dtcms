/*
 * VUE页面底部导航组件
 * channel 频道名
 * category 分类数量
 * article 文章数量
 */
Vue.component('page-footer-nav', {
	template: `<div class="link-box">
                    <div v-for="(item,index) in listData" :key="index" class="list-box">
						<div class="title">{{item.title}}</div>
						<div class="list">
							<a v-for="(aitem,aindex) in item.data" :key="aindex" :href="'/'+channel+'/'+(aitem.callIndex?aitem.callIndex:aitem.id)">{{aitem.title}}</a>
						</div>
					</div>
					<slot></slot>
                </div>`,
	props: {
		channel: {
			type: String,
			default: '',
		},
		category: {
			type: Number,
			default: 1,
		},
		article: {
			type: Number,
			default: 1,
		},
	},
	data: function () {
		return {
			listData: [],
		}
	},
	methods: {
		//初始化数据
		initData() {
			let _this = this;
			AxiosAjax({
				url: `/client/article/view/${_this.channel}/${_this.category}/${_this.article}?siteId=${$site}`,
				success: function (res) {
					_this.listData = res.data;
				}
			});
		},
	},
	created() {
		this.initData();
	}
});