﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTcms.Core.Web.Controllers
{
    [Route("[controller]")]
    public class AccountController : Controller
    {
        /// <summary>
        /// 会员中心
        /// </summary>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        [Route("Login")]
        public IActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// 用户注册
        /// </summary>
        [Route("Register")]
        public IActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// 找回密码
        /// </summary>
        [Route("Reset/Password")]
        public IActionResult ResetPassword()
        {
            return View();
        }

        /// <summary>
        /// 投稿列表
        /// </summary>
        [Route("Contribute/List")]
        public IActionResult ContributeList()
        {
            return View();
        }

        /// <summary>
        /// 投稿编辑
        /// </summary>
        [Route("Contribute/Edit")]
        public IActionResult ContributeEdit()
        {
            return View(0);
        }

        /// <summary>
        /// 投稿编辑
        /// </summary>
        [Route("Contribute/Edit/{id}")]
        public IActionResult ContributeEdit([FromRoute] int id)
        {
            return View(id);
        }

        /// <summary>
        /// 余额列表
        /// </summary>
        [Route("Balance/List")]
        public IActionResult BalanceList()
        {
            return View();
        }

        /// <summary>
        /// 充值列表
        /// </summary>
        [Route("Recharge/List")]
        public IActionResult RechargeList()
        {
            return View();
        }

        /// <summary>
        /// 在线充值
        /// </summary>
        [Route("Recharge/Add")]
        public IActionResult RechargeAdd()
        {
            return View();
        }

        /// <summary>
        /// 个人资料
        /// </summary>
        [Route("Setting/Info")]
        public IActionResult SettingInfo()
        {
            return View();
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        [Route("Setting/Password")]
        public IActionResult SettingPassword()
        {
            return View();
        }
    }
}
