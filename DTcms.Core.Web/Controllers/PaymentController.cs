﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DTcms.Core.Web.Controllers
{
    public class PaymentController : Controller
    {
        /// <summary>
        /// 确认支付
        /// </summary>
        public IActionResult Confirm()
        {
            return View();
        }

        /// <summary>
        /// 支付结果
        /// </summary>
        public IActionResult Result()
        {
            return View();
        }
    }
}
