﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DTcms.Core.Web.Controllers
{
    public class NewsController : Controller
    {
        /// <summary>
        /// 新闻首页
        /// </summary>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 新闻列表页
        /// </summary>
        public IActionResult List([FromRoute] int id)
        {
            return View(id);
        }

        /// <summary>
        /// 新闻详情页
        /// </summary>
        public IActionResult Show([FromRoute] int id)
        {
            return View(id);
        }
    }
}
