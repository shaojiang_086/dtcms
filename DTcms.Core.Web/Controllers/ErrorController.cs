﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTcms.Core.Web.Controllers
{
    public class ErrorController : Controller
    {
        /// <summary>
        /// 全局异常
        /// </summary>
        /// <returns></returns>
        [Route("Error")]
        public IActionResult Error()
        {
            var result = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            ViewBag.ErrorCode = "500";
            ViewBag.ErrorTitle = "程序过程中发生错误";
            ViewBag.ErrorPath = result.Path;
            ViewBag.ErrorMessage = result.Error.Message;
            return View("Error");
        }

        /// <summary>
        /// 页面异常
        /// </summary>
        [Route("Error/{statusCode}")]
        public IActionResult Error(int statusCode)
        {
            var result = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorCode = "404";
                    ViewBag.ErrorTitle = "页面不存在";
                    ViewBag.ErrorPath = result.OriginalPath;
                    ViewBag.ErrorMessage = "抱歉，您访问的页面不存在";
                    break;
            }
            return View("Error");
        }
    }
}
