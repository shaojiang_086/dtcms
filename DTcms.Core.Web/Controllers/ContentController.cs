﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTcms.Core.Web.Controllers
{
    [Route("[controller]")]
    public class ContentController : Controller
    {
        /// <summary>
        /// 详情页
        /// </summary>
        [Route("{callIndex}")]
        public IActionResult Index([FromRoute] string callIndex)
        {
            return View("Index", callIndex);
        }
    }
}
