﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTcms.Core.Web.Controllers
{
    public class PhotoController : Controller
    {
        /// <summary>
        /// 频道首页
        /// </summary>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 列表页
        /// </summary>
        public IActionResult List([FromRoute] int id)
        {
            return View(id);
        }

        /// <summary>
        /// 详情页
        /// </summary>
        public IActionResult Show([FromRoute] int id)
        {
            return View(id);
        }
    }
}
