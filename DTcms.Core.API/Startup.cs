using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DTcms.Core.API.Filters;
using DTcms.Core.API.Handler;
using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.IServices.Alipay;
using DTcms.Core.IServices.WeChatPay;
using DTcms.Core.Model.Models;
using DTcms.Core.Services;
using DTcms.Core.Services.Alipay;
using DTcms.Core.Services.WeChatPay;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;

namespace DTcms.Core.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Env { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(new Appsettings(Env.ContentRootPath));//配置文件读写帮助类注入
            services.AddSingleton(new FileHelper(Env.ContentRootPath));//文件帮助类注入
            services.AddTransient<IDbContextFactory, DbContextFactory>();//注册数据库连接服务
            //IdentityUser配置
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequiredLength = 6;//密码最小长度
                options.Password.RequireNonAlphanumeric = false;//密码必须包含字母关闭
                options.Password.RequireUppercase = false;//密码必须包含大写关闭
            });
            services.AddDbContext<AppDbContext>(); //IdentityUser数据上下文
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders(); //使用IdentityUser模型

            //设置接收文件长度的最大值。
            services.Configure<FormOptions>(opt =>
            {
                opt.ValueLengthLimit = int.MaxValue;
                opt.MultipartBodyLengthLimit = int.MaxValue;
                opt.MultipartHeadersLengthLimit = int.MaxValue;
            });
            services.Configure<IISServerOptions>(opt =>
            {
                opt.MaxRequestBodySize = int.MaxValue;
            });

            #region 实体映射服务注入==========================
            //Application
            services.AddTransient<ISysConfigService, SysConfigService>();
            services.AddTransient<ISiteService, SiteService>();
            services.AddTransient<ISiteChannelService, SiteChannelService>();
            services.AddTransient<IAreaService, AreaService>();
            services.AddTransient<IManagerService, ManagerService>();
            services.AddTransient<IManagerLogService, ManagerLogService>();
            services.AddTransient<IManagerRoleService, ManagerRoleService>();
            services.AddTransient<INavigationService, NavigationService>();
            services.AddTransient<INotifyTemplateService, NotifyTemplateService>();
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<ISitePaymentService, SitePaymentService>();
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ITokenService, JwtTokenService>();
            services.AddTransient<ISmsService, SmsService>();
            //Article
            services.AddTransient<IArticleCategoryService, ArticleCategoryService>();
            services.AddTransient<IArticleCommentService, ArticleCommentService>();
            services.AddTransient<IArticleCommentLikeService, ArticleCommentLikeService>();
            services.AddTransient<IArticleContributeService, ArticleContributeService>();
            services.AddTransient<IArticleContributeService, ArticleContributeService>();
            services.AddTransient<IArticleLabelService, ArticleLabelService>();
            services.AddTransient<IArticleLikeService, ArticleLikeService>();
            services.AddTransient<IArticleService, ArticleService>();
            services.AddTransient<IArticleAttachService, ArticleAttachService>();
            //Member
            services.AddTransient<IMemberAmountLogService, MemberAmountLogService>();
            services.AddTransient<IMemberAttachLogService, MemberAttachLogService>();
            services.AddTransient<IMemberGroupService, MemberGroupService>();
            services.AddTransient<IMemberMessageService, MemberMessageService>();
            services.AddTransient<IMemberPointLogService, MemberPointLogService>();
            services.AddTransient<IMemberRechargeService, MemberRechargeService>();
            services.AddTransient<IMemberService, MemberService>();
            //Payment
            services.AddTransient<IWeChatPayNotify, WeChatPayNotify>();
            services.AddTransient<IWeChatPayExecute, WeChatPayExecute>();
            services.AddTransient<IAlipayExecute, AlipayExecute>();
            services.AddTransient<IAlipayNotify, AlipayNotify>();
            #endregion

            //注入资源授权处理Handler
            services.AddTransient<IAuthorizationHandler, PermissionAuthorizationHandler>();

            services.AddControllers(setupAction =>
            {
                //默认响应JSON格式
                setupAction.ReturnHttpNotAcceptable = true;
            })
            .AddNewtonsoftJson(opt =>
            {
                opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                opt.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
            })
            .AddXmlDataContractSerializerFormatters()//开启XML输入输出支持
            .ConfigureApiBehaviorOptions(setupAction =>
            {
                //数据验证失败响应
                setupAction.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetails = new ValidationProblemDetails(context.ModelState)
                    {
                        Status = StatusCodes.Status422UnprocessableEntity,
                    };
                    //取第一条错误信息返回
                    var resultMessage = new ResponseMessage(ErrorCode.ParameterError,
                        problemDetails.Errors.Values, context.HttpContext);
                    //返回422状态码
                    return new UnprocessableEntityObjectResult(resultMessage)
                    {
                        ContentTypes = { "application/json" }
                    };

                };
            });

            //JWT认证服务配置
            services.AddAuthentication(config =>
             {
                 config.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                 config.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                 config.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
             })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateLifetime = true,//是否验证失效时间
                    ClockSkew = TimeSpan.FromSeconds(30),

                    ValidateAudience = true,//是否验证Audience
                    //动态验证的方式，刷新token，旧token强制失效
                    AudienceValidator = (m, n, z) =>
                    {
                        return m != null && m.FirstOrDefault().Equals(Configuration["Authentication:Audience"]);
                    },
                    ValidateIssuer = true,//是否验证Issuer
                    ValidIssuer = Configuration["Authentication:Issuer"],

                    ValidateIssuerSigningKey = true,//是否验证SecurityKey
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Authentication:SecretKey"]))
                };
                options.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        //Token expired
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("token-expired", "true");
                        }
                        return Task.CompletedTask;
                    }
                };
            });

            //禁止跳转到登录页，直接返回401
            services.ConfigureApplicationCookie(options =>
            {
                options.Events.OnRedirectToLogin = context =>
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    return Task.CompletedTask;
                };
            });

            //扫描Profile文件
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddMvc(setupAction => {
                //全局异常过滤
                setupAction.Filters.Add(typeof(GlobalExceptionFilter));
                //全局请求过滤
                setupAction.Filters.Add(typeof(GlobalRequestFilter));
            });

            //配置跨域处理，允许配置文件AllowedHosts节点的值
            services.AddCors(options =>
            {
                options.AddPolicy("cors", builder =>
                    builder.WithOrigins(Configuration["AllowedHosts"])
                    .AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin()
                    .WithExposedHeaders("content-disposition", "token-expired", "x-pagination"));
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //使用静态文件
            app.UseStaticFiles();
            //使用路由
            app.UseRouting();
            //使用跨域
            app.UseCors("cors");
            //启用认证
            app.UseAuthentication();
            //授权服务
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
