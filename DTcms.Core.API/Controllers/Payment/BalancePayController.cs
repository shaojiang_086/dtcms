﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.IServices;
using DTcms.Core.Model.BalancePay;
using DTcms.Core.Model.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTcms.Core.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BalancePayController : ControllerBase
    {
        private readonly ISitePaymentService _sitePaymentService;
        public BalancePayController(ISitePaymentService sitePaymentService)
        {
            _sitePaymentService = sitePaymentService;
        }

        /// <summary>
        /// 余额支付统一下单
        /// 示例：/balancepay
        /// </summary>
        [HttpPost]
        public Task<IActionResult> Pay(BalancePayDto modelDto)
        {
            throw new ResponseException("余额支付不允许充值");
        }

    }
}
