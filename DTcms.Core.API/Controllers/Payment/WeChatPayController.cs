﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.IServices;
using DTcms.Core.IServices.WeChatPay;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.WeChatPay;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTcms.Core.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class WeChatPayController : ControllerBase
    {
        private readonly IWeChatPayExecute _weChatPayExecute;
        private readonly ISitePaymentService _sitePaymentService;
        private readonly IMemberRechargeService _memberRechargeService;
        public WeChatPayController(IWeChatPayExecute weChatPayExecute, ISitePaymentService sitePaymentService, 
            IMemberRechargeService memberRechargeService)
        {
            _weChatPayExecute = weChatPayExecute;
            _sitePaymentService = sitePaymentService;
            _memberRechargeService = memberRechargeService;
        }

        /// <summary>
        /// 微信统一下单
        /// 示例：/wechatpay
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Pay(WeChatPayDto modelDto)
        {
            //判断充值订单
            if (modelDto.OutTradeNo.StartsWith("RN"))
            {
                //充值订单状态
                var model = await _memberRechargeService.QueryAsync<MemberRecharge>(x => x.RechargeNo == modelDto.OutTradeNo, WriteRoRead.Write);
                if (model == null)
                {
                    throw new ResponseException("充值订单号有误，请重试");
                }
                if (model.Status == 1)
                {
                    throw new ResponseException("订单已支付，请勿重复付款");
                }
                if (model.Amount <= 0)
                {
                    throw new ResponseException("支付金额必须大于零");
                }
                //赋值支付方式及总金额
                modelDto.PaymentId = model.PaymentId;
                modelDto.Total = model.Amount;
            }
            //判断支付接口类型
            var pay = await _sitePaymentService.QueryAsync<SitePayment>(x => x.Id == modelDto.PaymentId);
            if (pay == null)
            {
                throw new ResponseException("支付方式有误，请重试");
            }if (pay.Type == "native")
            {
                return Ok(await _weChatPayExecute.NativePayAsync(modelDto));
            }
            throw new ResponseException("支付方式有误，请重试");
        }
    }
}
