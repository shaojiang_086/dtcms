﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DTcms.Core.API.Filters;
using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace DTcms.Core.API.Controllers
{
    /// <summary>
    /// 商品标签
    /// </summary>
    [Route("article/label")]
    [ApiController]
    public class ArticleLabelController : ControllerBase
    {
        private readonly IArticleLabelService _articleLabelService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public ArticleLabelController(IArticleLabelService articleLabelService, IUserService userService, IMapper mapper)
        {
            _articleLabelService = articleLabelService;
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// 根据ID获取数据
        /// 示例：/article/label/1/1
        /// </summary>
        [HttpGet("{channelId}/{id}")]
        [Authorize]
        [AuthorizeFilter("ArticleLabel", ActionType.View, "channelId")]
        public async Task<IActionResult> GetById([FromRoute] long id, [FromQuery] BaseParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<ArticleLabelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //查询数据库获取实体
            var model = await _articleLabelService.QueryAsync<ArticleLabel>(x => x.Id == id);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或已删除"));
            }
            //使用AutoMapper转换成ViewModel，根据字段进行塑形
            var result = _mapper.Map<ArticleLabelDto>(model).ShapeData(param.Fields);
            return Ok(result);
        }

        /// <summary>
        /// 获取指定数量列表
        /// 示例：/article/label/view/1/10
        /// </summary>
        [HttpGet("view/{channelId}/{top}")]
        [Authorize]
        [AuthorizeFilter("ArticleLabel", ActionType.View, "channelId")]
        public async Task<IActionResult> GetList([FromRoute] int top, [FromQuery] BaseParameter searchParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.Replace("-", "").IsPropertyExists<ArticleLabelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<ArticleLabelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据库列表
            var resultFrom = await _articleLabelService.QueryListAsync<ArticleLabel>(top,
                x => x.Status == searchParam.Status && (searchParam.Keyword.IsNotNullOrEmpty() ? x.Title.Contains(searchParam.Keyword) : true),
                searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "SortId,-Id");

            //映射成DTO，根据字段进行塑形
            var resultDto = _mapper.Map<IEnumerable<ArticleLabelDto>>(resultFrom).ShapeData(searchParam.Fields);
            //返回成功200
            return Ok(resultDto);
        }

        /// <summary>
        /// 获取分页列表
        /// 示例：/article/label/1?pageSize=10&pageIndex=1
        /// </summary>
        [HttpGet("{channelId}")]
        [Authorize]
        [AuthorizeFilter("ArticleLabel", ActionType.View, "channelId")]
        public async Task<IActionResult> GetList([FromQuery] BaseParameter searchParam, [FromQuery] PageParamater pageParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.Replace("-", "").IsPropertyExists<ArticleLabelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<ArticleLabelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据列表，如果ID大于0则查询该用户下所有的列表
            var list = await _articleLabelService.QueryPageAsync<ArticleLabel>(
                pageParam.PageSize,
                pageParam.PageIndex,
                x => (searchParam.Keyword.IsNotNullOrEmpty() ? x.Title.Contains(searchParam.Keyword) : true),
                searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "SortId,-Id");
            if (list == null || list.Count() <= 0)
            {
                return NotFound(ResponseMessage.Error("暂无查询到记录"));
            }

            //x-pagination
            var paginationMetadata = new
            {
                totalCount = list.TotalCount,
                pageSize = list.PageSize,
                pageIndex = list.PageIndex,
                totalPages = list.TotalPages
            };
            Response.Headers.Add("x-pagination", SerializeHelper.SerializeObject(paginationMetadata));

            //映射成DTO，根据字段进行塑形
            var resultDto = _mapper.Map<IEnumerable<ArticleLabelDto>>(list).ShapeData(searchParam.Fields);
            return Ok(resultDto);
        }

        /// <summary>
        /// 添加一条记录
        /// 示例：/article/label/1
        /// </summary>
        [HttpPost("{channelId}")]
        [Authorize]
        [AuthorizeFilter("ArticleLabel", ActionType.Add, "channelId")]
        public async Task<IActionResult> Add([FromBody] ArticleLabelEditDto modelDto)
        {
            //映射成实体
            var model = _mapper.Map<ArticleLabel>(modelDto);
            //获取当前用户名
            model.AddBy = await _userService.GetUserNameAsync();
            model.AddTime = DateTime.Now;
            //写入数据库
            var mapModel = await _articleLabelService.AddAsync(model);
            //映射成DTO再返回，否则出错
            var result = _mapper.Map<ArticleLabelDto>(mapModel);
            return Ok(result);
        }

        /// <summary>
        /// 修改一条记录
        /// 示例：/article/label/1/1
        /// </summary>
        [HttpPut("{channelId}/{id}")]
        [Authorize]
        [AuthorizeFilter("ArticleLabel", ActionType.Edit, "channelId")]
        public async Task<IActionResult> Update([FromRoute] long id, [FromBody] ArticleLabelEditDto modelDto)
        {
            //查找记录
            var model = await _articleLabelService.QueryAsync<ArticleLabel>(x => x.Id == id, WriteRoRead.Write);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或已删除"));
            }
            //获取当前用户名
            model.UpdateBy = await _userService.GetUserNameAsync();
            model.UpdateTime = DateTime.Now;

            //更新操作AutoMapper替我们完成，只需要调用保存即可
            _mapper.Map(modelDto, model);
            var result = await _articleLabelService.SaveAsync();
            return NoContent();
        }

        /// <summary>
        /// 局部更新一条记录
        /// 示例：/article/label/1/1
        /// Body：[{"op":"replace","path":"/title","value":"new title"}]
        /// </summary>
        [HttpPatch("{channelId}/{id}")]
        [Authorize]
        [AuthorizeFilter("ArticleLabel", ActionType.Edit, "channelId")]
        public async Task<IActionResult> Update([FromRoute] long id, [FromBody] JsonPatchDocument<ArticleLabelEditDto> patchDocument)
        {
            var model = await _articleLabelService.QueryAsync<ArticleLabel>(x => x.Id == id, WriteRoRead.Write);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或已删除"));
            }

            var modelToPatch = _mapper.Map<ArticleLabelEditDto>(model);
            patchDocument.ApplyTo(modelToPatch, ModelState);
            //验证数据是否合法
            if (!TryValidateModel(modelToPatch))
            {
                return ValidationProblem(ModelState);
            }
            //更新操作AutoMapper替我们完成，只需要调用保存即可
            _mapper.Map(modelToPatch, model);
            await _articleLabelService.SaveAsync();

            return NoContent();
        }

        /// <summary>
        /// 删除一条记录
        /// 示例：/article/label/1/1
        /// </summary>
        [HttpDelete("{channelId}/{id}")]
        [Authorize]
        [AuthorizeFilter("ArticleLabel", ActionType.Delete, "channelId")]
        public async Task<IActionResult> Delete([FromRoute] long id)
        {
            if (!await _articleLabelService.ExistsAsync<ArticleLabel>(x => x.Id == id))
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或已删除"));
            }
            var result = await _articleLabelService.DeleteAsync<ArticleLabel>(x => x.Id == id);

            return NoContent();
        }

        /// <summary>
        /// 批量删除记录(级联数据)
        /// 示例：/article/label/1?ids=1,2,3
        /// </summary>
        [HttpDelete("{channelId}")]
        [Authorize]
        [AuthorizeFilter("ArticleLabel", ActionType.Delete, "channelId")]
        public async Task<IActionResult> DeleteByIds([FromQuery] string Ids)
        {
            if (Ids == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不可为空"));
            }
            //将ID列表转换成IEnumerable
            var arrIds = Ids.ToIEnumerable<long>();
            if (arrIds == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不符合规范"));
            }
            //执行批量删除操作
            await _articleLabelService.DeleteAsync<ArticleLabel>(x => arrIds.Contains(x.Id));

            return NoContent();
        }


        #region 前台调用接口============================
        /// <summary>
        /// 获取指定数量列表
        /// 示例：/client/article/label/view/10
        /// </summary>
        [HttpGet("/client/article/label/view/{top}")]
        public async Task<IActionResult> ClientGetList([FromRoute] int top, [FromQuery] BaseParameter searchParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.Replace("-", "").IsPropertyExists<ArticleLabelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<ArticleLabelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据库列表
            var resultFrom = await _articleLabelService.QueryListAsync<ArticleLabel>(top,
                x => searchParam.Keyword.IsNotNullOrEmpty() ? x.Title.Contains(searchParam.Keyword) : true,
                searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "SortId,-Id");

            //映射成DTO，根据字段进行塑形
            var resultDto = _mapper.Map<IEnumerable<ArticleLabelDto>>(resultFrom).ShapeData(searchParam.Fields);
            //返回成功200
            return Ok(resultDto);
        }
        #endregion

    }
}
