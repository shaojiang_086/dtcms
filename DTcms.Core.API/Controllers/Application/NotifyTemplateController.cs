﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DTcms.Core.API.Filters;
using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace DTcms.Core.API.Controllers
{
    [Route("notify/template")]
    [ApiController]
    public class NotifyTemplateController : ControllerBase
    {
        private readonly INotifyTemplateService _notifyTemplateService;
        private readonly IMapper _mapper;
        public NotifyTemplateController(INotifyTemplateService notifyTemplateService, IMapper mapper)
        {
            _notifyTemplateService = notifyTemplateService;
            _mapper = mapper;
        }

        /// <summary>
        /// 获取分页列表
        /// 示例：/notify/template?pageSize=10&pageIndex=1
        /// </summary>
        [HttpGet]
        [Authorize]
        [AuthorizeFilter("NotifyTemplate", ActionType.View)]
        public async Task<IActionResult> GetList([FromQuery] BaseParameter searchParam, [FromQuery] PageParamater pageParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.TrimStart('-').IsPropertyExists<NotifyTemplateDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<NotifyTemplateDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据列表
            var list = await _notifyTemplateService.QueryPageAsync<NotifyTemplate>(
                pageParam.PageSize,
                pageParam.PageIndex,
                x => searchParam.Keyword.IsNotNullOrEmpty() ? x.Title.Contains(searchParam.Keyword) : true,
                searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "-Id"
                );
            if (list == null || list.Count() <= 0)
            {
                return NotFound(ResponseMessage.Error("暂无查询到记录"));
            }

            //x-pagination
            var paginationMetadata = new
            {
                totalCount = list.TotalCount,
                pageSize = list.PageSize,
                pageIndex = list.PageIndex,
                totalPages = list.TotalPages
            };
            Response.Headers.Add("x-pagination", SerializeHelper.SerializeObject(paginationMetadata));

            //映射成DTO
            var resultDto = _mapper.Map<IEnumerable<NotifyTemplateDto>>(list).ShapeData(searchParam.Fields);
            return Ok(resultDto);
        }

        /// <summary>
        /// 根据ID获取数据
        /// 示例：/notify/template/1
        /// </summary>
        [HttpGet("{id}")]
        [Authorize]
        [AuthorizeFilter("NotifyTemplate", ActionType.View)]
        public async Task<IActionResult> GetById([FromRoute] int id, [FromQuery] BaseParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<NotifyTemplateDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //查询数据库获取实体
            var model = await _notifyTemplateService.QueryAsync<NotifyTemplate>(x => x.Id == id);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或已删除"));
            }
            //使用AutoMapper转换成ViewModel
            //根据字段进行塑形
            var result = _mapper.Map<NotifyTemplateDto>(model).ShapeData(param.Fields);
            return Ok(result);
        }

        /// <summary>
        /// 根据别名获取数据
        /// </summary>
        [HttpGet("{type}/{callIndex}")]
        [Authorize]
        [AuthorizeFilter("NotifyTemplate", ActionType.View)]
        public async Task<IActionResult> GetCallIndex([FromRoute] int type, [FromRoute] string callIndex, [FromQuery] BaseParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<NotifyTemplateDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //查询数据库获取实体
            var model = await _notifyTemplateService.QueryAsync<NotifyTemplate>(
                x => x.Type == type && x.CallIndex.ToLower() == callIndex.ToLower());
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{callIndex}不存在或已删除"));
            }
            //根据字段进行塑形
            var result = _mapper.Map<NotifyTemplateDto>(model).ShapeData(param.Fields);
            return Ok(result);
        }

        /// <summary>
        /// 添加一条记录
        /// 示例：/notify/template
        /// </summary>
        [HttpPost]
        [Authorize]
        [AuthorizeFilter("NotifyTemplate", ActionType.Add)]
        public async Task<IActionResult> Add([FromBody] NotifyTemplateEditDto modelDto)
        {
            //映射成实体
            var model = _mapper.Map<NotifyTemplate>(modelDto);
            //写入数据库
            await _notifyTemplateService.AddAsync(model);
            //映射成DTO再返回，否则出错
            var result = _mapper.Map<NotifyTemplateDto>(model);
            return Ok(result);
        }

        /// <summary>
        /// 修改一条记录
        /// 示例：/notify/template/1
        /// </summary>
        [HttpPut("{id}")]
        [Authorize]
        [AuthorizeFilter("NotifyTemplate", ActionType.Edit)]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] NotifyTemplateEditDto modelDto)
        {
            //查找记录
            var model = await _notifyTemplateService.QueryAsync<NotifyTemplate>(x => x.Id == id);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或已删除"));
            }

            model.UpdateTime = DateTime.Now; //更新时间
            //更新操作AutoMapper替我们完成，只需要调用保存即可
            _mapper.Map(modelDto, model);
            var result = await _notifyTemplateService.SaveAsync();
            return NoContent();
        }

        /// <summary>
        /// 局部更新一条记录
        /// 示例：/notify/template/1
        /// Body：[{"op":"replace","path":"/title","value":"new title"}]
        /// </summary>
        [HttpPatch("{id}")]
        [Authorize]
        [AuthorizeFilter("NotifyTemplate", ActionType.Edit)]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] JsonPatchDocument<NotifyTemplateEditDto> patchDocument)
        {
            var model = await _notifyTemplateService.QueryAsync<NotifyTemplate>(x => x.Id == id);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或已删除"));
            }

            var modelToPatch = _mapper.Map<NotifyTemplateEditDto>(model);
            patchDocument.ApplyTo(modelToPatch, ModelState);
            //验证数据是否合法
            if (!TryValidateModel(modelToPatch))
            {
                return ValidationProblem(ModelState);
            }
            //更新操作AutoMapper替我们完成，只需要调用保存即可
            _mapper.Map(modelToPatch, model);
            await _notifyTemplateService.SaveAsync();
            return NoContent();
        }

        /// <summary>
        /// 删除一条记录
        /// 示例：/notify/template/1
        /// </summary>
        [HttpDelete("{id}")]
        [Authorize]
        [AuthorizeFilter("NotifyTemplate", ActionType.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            //系统默认不允许删除
            if (!await _notifyTemplateService.ExistsAsync<NotifyTemplate>(x => x.Id == id && x.IsSystem == 0))
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或无权删除"));
            }
            var result = await _notifyTemplateService.DeleteAsync<NotifyTemplate>(x => x.Id == id);

            return NoContent();
        }

        /// <summary>
        /// 批量删除记录(级联数据)
        /// 示例：/notify/template?ids=1,2,3
        /// </summary>
        [HttpDelete]
        [Authorize]
        [AuthorizeFilter("NotifyTemplate", ActionType.Delete)]
        public async Task<IActionResult> DeleteByIds([FromQuery] string Ids)
        {
            if (Ids == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不可为空"));
            }
            //将ID列表转换成IEnumerable
            var listIds = Ids.ToIEnumerable<int>();
            if (listIds == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不符合规范"));
            }
            //执行批量删除操作
            await _notifyTemplateService.DeleteAsync<NotifyTemplate>(x => listIds.Contains(x.Id) && x.IsSystem == 0);

            return NoContent();
        }
    }
}
