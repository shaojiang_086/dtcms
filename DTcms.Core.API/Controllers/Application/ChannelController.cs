﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AutoMapper;
using DTcms.Core.API.Filters;
using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace DTcms.Core.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ChannelController : ControllerBase
    {
        private readonly ISiteChannelService _siteChannelService;
        private readonly ISiteService _siteService;
        private readonly IMapper _mapper;
        public ChannelController(ISiteChannelService siteChannelService,
            ISiteService siteService, IMapper mapper)
        {
            _siteChannelService = siteChannelService;
            _siteService = siteService;
            _mapper = mapper;
        }

        /// <summary>
        /// 根据ID获取频道
        /// 示例：/channel/1
        /// </summary>
        [HttpGet("{channelId}")]
        [Authorize]
        [AuthorizeFilter("Channel", ActionType.View)]
        public async Task<IActionResult> GetById([FromRoute] int channelId, [FromQuery] BaseParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<SiteChannelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //查询数据库获取实体
            var model = await _siteChannelService.QueryAsync(x => x.Id == channelId);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"频道{channelId}不存在或已删除"));
            }
            //使用AutoMapper转换成ViewModel
            //根据字段进行塑形
            var result = _mapper.Map<SiteChannelDto>(model).ShapeData(param.Fields);
            return Ok(result);
        }

        /// <summary>
        /// 获取指定数量列表
        /// 示例：/channel/view/0
        /// </summary>
        [HttpGet("view/{top}")]
        [Authorize]
        [AuthorizeFilter("Channel", ActionType.View)]
        public async Task<IActionResult> GetList([FromRoute] int top, [FromQuery] BaseParameter searchParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.Replace("-", "").IsPropertyExists<SiteChannelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<SiteChannelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据库列表
            var resultFrom = await _siteChannelService.QueryListAsync<SiteChannel>(top,
                x => (searchParam.SiteId > 0 ? x.SiteId == searchParam.SiteId : true)
                && (searchParam.Keyword.IsNotNullOrEmpty() ? x.Title.Contains(searchParam.Keyword) : true),
                searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "SortId,AddTime");

            //映射成DTO，根据字段进行塑形
            var resultDto = _mapper.Map<IEnumerable<SiteChannelDto>>(resultFrom).ShapeData(searchParam.Fields);
            //返回成功200
            return Ok(resultDto);
        }

        /// <summary>
        /// 获取频道分页列表
        /// 示例：/channel?pageSize=10&pageIndex=1
        /// </summary>
        [HttpGet]
        [Authorize]
        [AuthorizeFilter("Channel", ActionType.View)]
        public async Task<IActionResult> GetList([FromQuery] BaseParameter searchParam, [FromQuery] PageParamater pageParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.TrimStart('-').IsPropertyExists<SiteChannelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<SiteChannelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据列表
            var list = await _siteChannelService.QueryPageAsync(
                pageParam.PageSize,
                pageParam.PageIndex,
                x => (searchParam.SiteId > 0 ? x.SiteId == searchParam.SiteId : true)
                && (searchParam.Keyword.IsNotNullOrEmpty() ? x.Title.Contains(searchParam.Keyword) : true),
                searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "SortId,AddTime");
            if (list == null || list.Count() <= 0)
            {
                return NotFound(ResponseMessage.Error("暂无查询到记录"));
            }

            //x-pagination
            var paginationMetadata = new
            {
                totalCount = list.TotalCount,
                pageSize = list.PageSize,
                pageIndex = list.PageIndex,
                totalPages = list.TotalPages
            };
            Response.Headers.Add("x-pagination", SerializeHelper.SerializeObject(paginationMetadata));

            //映射成DTO
            var resultDto = _mapper.Map<IEnumerable<SiteChannelDto>>(list).ShapeData(searchParam.Fields);
            return Ok(resultDto);
        }

        /// <summary>
        /// 添加一条记录
        /// 示例：/channel/
        /// </summary>
        [HttpPost]
        [Authorize]
        [AuthorizeFilter("Channel", ActionType.Add)]
        public async Task<IActionResult> Add([FromBody] SiteChannelDto modelDto)
        {
            var result = await _siteChannelService.AddAsync(modelDto);
            return Ok(result);
        }

        /// <summary>
        /// 修改一条记录
        /// 示例：/channel/1
        /// </summary>
        [HttpPut("{id}")]
        [Authorize]
        [AuthorizeFilter("Channel", ActionType.Edit)]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] SiteChannelEditDto modelDto)
        {
            var result = await _siteChannelService.UpdateAsync(id, modelDto);
            return NoContent();
        }

        /// <summary>
        /// 局部更新一条记录
        /// 示例：/channel/1
        /// Body：[{"op":"replace","path":"/title","value":"new title"}]
        /// </summary>
        [HttpPatch("{id}")]
        [Authorize]
        [AuthorizeFilter("Channel", ActionType.Edit)]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] JsonPatchDocument<SiteChannelEditDto> patchDocument)
        {
            //注意：要使用写的数据库进行查询，才能正确写入数据主库
            var model = await _siteChannelService.QueryAsync(x => x.Id == id, WriteRoRead.Write);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"频道{id}不存在或已删除"));
            }

            var modelToPatch = _mapper.Map<SiteChannelEditDto>(model);
            patchDocument.ApplyTo(modelToPatch, ModelState);
            //验证数据是否合法
            if (!TryValidateModel(modelToPatch))
            {
                return ValidationProblem(ModelState);
            }
            //更新操作AutoMapper替我们完成，只需要调用保存即可
            _mapper.Map(modelToPatch, model);
            await _siteChannelService.SaveAsync();
            return NoContent();
        }

        /// <summary>
        /// 删除一条记录(级联数据)
        /// 示例：/channel/1
        /// </summary>
        [HttpDelete("{id}")]
        [Authorize]
        [AuthorizeFilter("Channel", ActionType.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            //取得站点实体信息(带域名)
            var model = await _siteChannelService.QueryAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"频道{id}不存在或已删除"));
            }
            //应还要检查频道下是否有文章，有则删除失败
            var result = await _siteChannelService.DeleteAsync(model);
            return NoContent();
        }

        /// <summary>
        /// 批量删除记录(级联数据)
        /// 示例：/channel?ids=1,2,3
        /// </summary>
        [HttpDelete]
        [Authorize]
        [AuthorizeFilter("Channel", ActionType.Delete)]
        public async Task<IActionResult> DeleteByIds([FromQuery] string Ids)
        {
            if (Ids == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不可为空"));
            }
            //将ID列表转换成IEnumerable
            var arrIds = Ids.ToIEnumerable<int>();
            if (arrIds == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不符合规范"));
            }
            //执行删除操作
            var result = await _siteChannelService.DeleteAsync(x => arrIds.Contains(x.Id));
            return NoContent();
        }

        /// <summary>
        /// 获取频道扩展字段
        /// 示例：/channel/1/field
        /// </summary>
        [HttpGet("{id}/field")]
        [Authorize]
        [AuthorizeFilter("Channel", ActionType.View)]
        public async Task<IActionResult> GetFieldList([FromRoute] int id, [FromQuery] BaseParameter searchParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.Replace("-", "").IsPropertyExists<SiteChannelFieldDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<SiteChannelFieldDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据库列表
            var resultFrom = await _siteChannelService.QueryListAsync<SiteChannelField>(0,
                x => x.ChannelId == id, searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "SortId,Id");

            //映射成DTO，根据字段进行塑形
            var resultDto = _mapper.Map<IEnumerable<SiteChannelFieldDto>>(resultFrom).ShapeData(searchParam.Fields);
            //返回成功200
            return Ok(resultDto);
        }

        #region 前台调用接口
        /// <summary>
        /// 根据ID获取频道
        /// 示例：/client/channel/1
        /// </summary>
        [HttpGet("/client/channel/{channelId}")]
        public async Task<IActionResult> ClientGetById([FromRoute] int channelId, [FromQuery] BaseParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<SiteChannelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //查询数据库获取实体
            var model = await _siteChannelService.QueryAsync(x => x.Id == channelId);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"频道{channelId}不存在或已删除"));
            }
            //使用AutoMapper转换成ViewModel
            //根据字段进行塑形
            var result = _mapper.Map<SiteChannelDto>(model).ShapeData(param.Fields);
            return Ok(result);
        }

        /// <summary>
        /// 获取指定数量列表
        /// 示例：/client/channel/view/0
        /// </summary>
        [HttpGet("/client/channel/view/{top}")]
        public async Task<IActionResult> ClientGetList([FromRoute] int top, [FromQuery] BaseParameter searchParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.Replace("-", "").IsPropertyExists<SiteChannelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<SiteChannelDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据库列表
            var resultFrom = await _siteChannelService.QueryListAsync<SiteChannel>(top,
                x => (searchParam.SiteId > 0 ? x.SiteId == searchParam.SiteId : true)
                && (searchParam.Keyword.IsNotNullOrEmpty() ? x.Title.Contains(searchParam.Keyword) : true),
                searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "SortId,AddTime");

            //映射成DTO，根据字段进行塑形
            var resultDto = _mapper.Map<IEnumerable<SiteChannelDto>>(resultFrom).ShapeData(searchParam.Fields);
            //返回成功200
            return Ok(resultDto);
        }

        /// <summary>
        /// 获取频道扩展字段
        /// 示例：/client/channel/1/field
        /// </summary>
        [HttpGet("/client/channel/{id}/field")]
        public async Task<IActionResult> ClientGetFieldList([FromRoute] int id, [FromQuery] BaseParameter searchParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.Replace("-", "").IsPropertyExists<SiteChannelFieldDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<SiteChannelFieldDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据库列表
            var resultFrom = await _siteChannelService.QueryListAsync<SiteChannelField>(0,
                x => x.ChannelId == id, searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "SortId,Id");

            //映射成DTO，根据字段进行塑形
            var resultDto = _mapper.Map<IEnumerable<SiteChannelFieldDto>>(resultFrom).ShapeData(searchParam.Fields);
            //返回成功200
            return Ok(resultDto);
        }
        #endregion
    }
}
