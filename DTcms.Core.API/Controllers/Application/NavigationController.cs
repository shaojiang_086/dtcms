﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DTcms.Core.API.Filters;
using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace DTcms.Core.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NavigationController : ControllerBase
    {
        private readonly INavigationService _navigationService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public NavigationController(INavigationService navigationService, IUserService userService, IMapper mapper)
        {
            _navigationService = navigationService;
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// 获取导航树目录列表
        /// 示例：/navigation/
        /// </summary>
        [HttpGet]
        [Authorize]
        [AuthorizeFilter("Navigation", ActionType.View)]
        public async Task<IActionResult> GetList([FromQuery] BaseParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<NavigationDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //如果有查询关健字
            var parentId = 0; //父节点ID
            if (param.Keyword.IsNotNullOrWhiteSpace())
            {
                var model = await _navigationService.QueryAsync<Navigation>(x => x.Title.Contains(param.Keyword));
                if (model == null)
                {
                    return NotFound(ResponseMessage.Error("暂无查询记录"));
                }
                parentId = model.Id;
            }
            //获取数据库列表
            var resultFrom = await _navigationService.QueryListAsync(parentId);
            //使用AutoMapper转换成ViewModel
            //根据字段进行塑形
            var resultDto = resultFrom.ShapeData(param.Fields);
            //返回成功200
            return Ok(resultDto);
        }

        /// <summary>
        /// 根据ID获取导航
        /// 示例：/navigation/1
        /// </summary>
        [HttpGet("{navId}")]
        [Authorize]
        [AuthorizeFilter("Navigation", ActionType.View)]
        public async Task<IActionResult> GetById([FromRoute] int navId, [FromQuery] BaseParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<NavigationDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //查询数据库获取实体
            var model = await _navigationService.QueryAsync<Navigation>(x => x.Id == navId);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{navId}不存在或已删除"));
            }
            //使用AutoMapper转换成ViewModel
            //根据字段进行塑形
            var result = _mapper.Map<NavigationDto>(model).ShapeData(param.Fields);
            return Ok(result);
        }

        /// <summary>
        /// 添加一条记录
        /// 示例：/navigation/
        /// </summary>
        [HttpPost]
        [Authorize]
        [AuthorizeFilter("Navigation", ActionType.Add)]
        public async Task<IActionResult> Add([FromBody] NavigationEditDto navigationEditDto)
        {
            //检查站点名称是否重复
            if (await _navigationService.ExistsAsync<Navigation>(x => x.Name.ToLower() == navigationEditDto.Name.ToLower()))
            {
                return BadRequest(new ResponseMessage(ErrorCode.RepeatField, $"菜单{navigationEditDto.Name}已存在"));
            }
            //映射成实体
            var model = _mapper.Map<Navigation>(navigationEditDto);
            //获取当前用户名
            model.AddBy = await _userService.GetUserNameAsync();
            model.AddTime = DateTime.Now;
            //写入数据库
            var sourceModel = await _navigationService.AddAsync(model);
            //映射成DTO再返回，否则出错
            var result = _mapper.Map<NavigationDto>(sourceModel);
            return Ok(result);
        }

        /// <summary>
        /// 修改一条记录
        /// 示例：/navigation/1
        /// </summary>
        [HttpPut("{navId}")]
        [Authorize]
        [AuthorizeFilter("Navigation", ActionType.Edit)]
        public async Task<IActionResult> Update([FromRoute] int navId, [FromBody] NavigationEditDto navigationEditDto)
        {
            //查找站点信息
            var model = await _navigationService.QueryAsync<Navigation>(x => x.Id == navId);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{navId}不存在或已删除"));
            }
            //检查站点名称是否重复
            if (model.Name.ToLower() != navigationEditDto.Name.ToLower()
                && await _navigationService.ExistsAsync<Sites>(x => x.Name.ToLower() == navigationEditDto.Name.ToLower()))
            {
                return BadRequest(new ResponseMessage(ErrorCode.RepeatField, $"菜单{navigationEditDto.Name}已存在"));
            }
            //更新操作AutoMapper替我们完成，只需要调用保存即可
            _mapper.Map(navigationEditDto, model);
            var result = await _navigationService.SaveAsync();
            return NoContent();
        }

        /// <summary>
        /// 局部更新一条记录
        /// 示例：/navigation/1
        /// Body：[{"op":"replace","path":"/title","value":"new title"}]
        /// </summary>
        [HttpPatch("{navId}")]
        [Authorize]
        [AuthorizeFilter("Navigation", ActionType.Edit)]
        public async Task<IActionResult> Update([FromRoute] int navId, [FromBody] JsonPatchDocument<NavigationEditDto> patchDocument)
        {
            var model = await _navigationService.QueryAsync<Navigation>(x => x.Id == navId);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{navId}不存在或已删除"));
            }

            var modelToPatch = _mapper.Map<NavigationEditDto>(model);
            patchDocument.ApplyTo(modelToPatch, ModelState);
            //验证数据是否合法
            if (!TryValidateModel(modelToPatch))
            {
                return ValidationProblem(ModelState);
            }
            //更新操作AutoMapper替我们完成，只需要调用保存即可
            _mapper.Map(modelToPatch, model);
            await _navigationService.SaveAsync();

            return NoContent();
        }

        /// <summary>
        /// 删除一条记录
        /// 示例：/navigation/1
        /// </summary>
        [HttpDelete("{navId}")]
        [Authorize]
        [AuthorizeFilter("Navigation", ActionType.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int navId)
        {
            if (!await _navigationService.ExistsAsync<Navigation>(x => x.Id == navId))
            {
                return NotFound(ResponseMessage.Error($"数据{navId}不存在或已删除"));
            }
            var result = await _navigationService.DeleteAsync(x => x.Id == navId);

            return NoContent();
        }

        /// <summary>
        /// 批量删除记录(级联数据)
        /// 示例：/navigation?ids=1,2,3
        /// </summary>
        [HttpDelete]
        [Authorize]
        [AuthorizeFilter("Navigation", ActionType.Delete)]
        public async Task<IActionResult> DeleteByIds([FromQuery] string Ids)
        {
            if (Ids == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不可为空"));
            }
            //将ID列表转换成IEnumerable
            var areaIds = Ids.ToIEnumerable<int>();
            if (areaIds == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不符合规范"));
            }
            //执行批量删除操作
            await _navigationService.DeleteAsync(x => areaIds.Contains(x.Id));

            return NoContent();
        }

        #region 管理员API接口
        /// <summary>
        /// 获取管理员导航树目录列表
        /// 示例：/admin/navigation/
        /// </summary>
        [HttpGet("/admin/navigation")]
        [Authorize]
        public async Task<IActionResult> GetNavList([FromQuery] BaseParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<NavigationDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //获取数据库列表
            var resultFrom = await _navigationService.QueryListAsync();
            //使用AutoMapper转换成ViewModel
            //根据字段进行塑形
            var resultDto = resultFrom.ShapeData(param.Fields);
            //返回成功200
            return Ok(resultDto);
        }
        #endregion

    }
}
