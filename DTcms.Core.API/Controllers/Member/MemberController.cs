﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DTcms.Core.API.Filters;
using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace DTcms.Core.API.Controllers
{
    /// <summary>
    /// 会员信息
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class MemberController : ControllerBase
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMemberService _memberService;
        private readonly IUserService _userService;
        private readonly ISysConfigService _sysConfigService;
        private readonly ISiteService _siteService;
        private readonly IMapper _mapper;
        public MemberController(IMemberService memberService, IUserService userService, 
            ISysConfigService sysConfigService, ISiteService siteService,
            IHttpContextAccessor httpContextAccessor, IMapper mapper)
        {
            _memberService = memberService;
            _userService = userService;
            _sysConfigService = sysConfigService;
            _siteService = siteService;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
        }

        /// <summary>
        /// 根据ID获取一条记录
        /// 示例：/member/1
        /// </summary>
        [HttpGet("{userId}")]
        [Authorize]
        [AuthorizeFilter("Member", ActionType.View)]
        public async Task<IActionResult> GetById([FromRoute] int userId, [FromQuery] MemberParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<MembersDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //查询数据库获取实体
            var model = await _memberService.QueryAsync(x => x.UserId == userId);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"会员{userId}不存在或已删除"));
            }
            //根据字段进行塑形
            var result = _mapper.Map<MembersDto>(model).ShapeData(param.Fields);
            return Ok(result);
        }

        /// <summary>
        /// 获取总记录数量
        /// 示例：/member/view/count
        /// </summary>
        [HttpGet("view/count")]
        [Authorize]
        [AuthorizeFilter("Member", ActionType.View)]
        public async Task<IActionResult> GetCount([FromQuery] MemberParameter searchParam)
        {
            var result = await _memberService.QueryCountAsync(x => searchParam.Status < 0 || x.User.Status == searchParam.Status);
            //返回成功200
            return Ok(result);
        }

        /// <summary>
        /// 获取会员注册统计
        /// 示例：/member/view/report
        /// </summary>
        [HttpGet("view/report")]
        [Authorize]
        [AuthorizeFilter("Member", ActionType.View)]
        public async Task<IActionResult> GetMemberRegister([FromQuery] MemberParameter searchParam)
        {
            if (searchParam.StartTime == null)
            {
                searchParam.StartTime = DateTime.Now.AddDays(-DateTime.Now.Day + 1);
            }
            if (searchParam.EndTime == null)
            {
                searchParam.EndTime = DateTime.Now;
            }
            var result = await _memberService.QueryCountListAsync(0,
                x => (DateTime.Compare(x.RegTime, searchParam.StartTime.GetValueOrDefault()) >= 0)
                && (DateTime.Compare(x.RegTime, searchParam.EndTime.GetValueOrDefault()) <= 0)
            );
            return Ok(result);
        }

        // <summary>
        /// 获取分页列表
        /// 示例：/member?pageSize=10&pageIndex=1
        /// </summary>
        [HttpGet]
        [Authorize]
        [AuthorizeFilter("Member", ActionType.View)]
        public async Task<IActionResult> GetList([FromQuery] MemberParameter searchParam, [FromQuery] PageParamater pageParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.Replace("-", "").IsPropertyExists<MembersDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<MembersDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据列表
            var list = await _memberService.QueryPageAsync(
                pageParam.PageSize,
                pageParam.PageIndex,
                x => (searchParam.SiteId <= 0 || x.SiteId == searchParam.SiteId)
                && (searchParam.Status < 0 || x.User.Status == searchParam.Status)
                /*&& DateTime.Compare(x.RegTime, DateTime.Now) <= 0
                && DateTime.Compare(x.RegTime, DateTime.Now) >= 0*/
                && (!searchParam.Keyword.IsNotNullOrEmpty() || x.RealName.Contains(searchParam.Keyword) || x.User.UserName.Contains(searchParam.Keyword)),
                searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "-Id,-RegTime");
            if (list == null || list.Count() <= 0)
            {
                return NotFound(ResponseMessage.Error("暂无查询到记录"));
            }

            //x-pagination
            var paginationMetadata = new
            {
                totalCount = list.TotalCount,
                pageSize = list.PageSize,
                pageIndex = list.PageIndex,
                totalPages = list.TotalPages
            };
            Response.Headers.Add("x-pagination", SerializeHelper.SerializeObject(paginationMetadata));

            //映射成DTO
            var resultDto = _mapper.Map<IEnumerable<MembersDto>>(list).ShapeData(searchParam.Fields);
            return Ok(resultDto);
        }

        /// <summary>
        /// 添加一条记录
        /// 示例：/member
        /// </summary>
        [HttpPost]
        [Authorize]
        [AuthorizeFilter("Member", ActionType.Add)]
        public async Task<IActionResult> Add([FromBody] MembersEditDto modelDto)
        {
            var result = await _memberService.AddAsync(modelDto);
            return Ok(result);
        }

        /// <summary>
        /// 修改一条记录
        /// 示例：/member/1
        /// </summary>
        [HttpPut("{userId}")]
        [Authorize]
        [AuthorizeFilter("Member", ActionType.Edit)]
        public async Task<IActionResult> Update([FromRoute] int userId, [FromBody] MembersEditDto modelDto)
        {
            var result = await _memberService.UpdateAsync(userId, modelDto);
            return NoContent();
        }

        /// <summary>
        /// 局部更新一条记录
        /// 示例：/member/1
        /// Body：[{"op":"replace","path":"/title","value":"new title"}]
        /// </summary>
        [HttpPatch("{userId}")]
        [Authorize]
        [AuthorizeFilter("Member", ActionType.Edit)]
        public async Task<IActionResult> Update([FromRoute] int userId, [FromBody] JsonPatchDocument<MembersEditDto> patchDocument)
        {
            var model = await _memberService.QueryAsync<Members>(x => x.UserId == userId, WriteRoRead.Write);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{userId}不存在或已删除"));
            }

            var modelToPatch = _mapper.Map<MembersEditDto>(model);
            patchDocument.ApplyTo(modelToPatch, ModelState);
            //验证数据是否合法
            if (!TryValidateModel(modelToPatch))
            {
                return ValidationProblem(ModelState);
            }
            //更新操作AutoMapper替我们完成，只需要调用保存即可
            _mapper.Map(modelToPatch, model);
            await _memberService.SaveAsync();
            return NoContent();
        }

        /// <summary>
        /// 删除一条记录
        /// 示例：/member/1
        /// </summary>
        [HttpDelete("{userId}")]
        [Authorize]
        [AuthorizeFilter("Member", ActionType.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int userId)
        {
            //查找记录是否存在
            if (!await _memberService.ExistsAsync<Members>(x => x.UserId == userId))
            {
                return NotFound(ResponseMessage.Error($"数据{userId}不存在或已删除"));
            }
            var result = await _memberService.DeleteAsync(userId);

            return NoContent();
        }

        /// <summary>
        /// 批量删除记录(级联数据)
        /// 示例：/member?ids=1,2,3
        /// </summary>
        [HttpDelete]
        [Authorize]
        [AuthorizeFilter("Member", ActionType.Delete)]
        public async Task<IActionResult> DeleteByIds([FromQuery] string Ids)
        {
            if (Ids == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不可为空"));
            }
            //将ID列表转换成IEnumerable
            var listIds = Ids.ToIEnumerable<int>();
            if (listIds == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不符合规范"));
            }
            //将符合条件的会员ID一次查询出来
            var list = await _memberService.QueryListAsync<Members>(0, x => listIds.Contains(x.UserId));
            //执行批量删除操作
            foreach (var modelt in list)
            {
                await _memberService.DeleteAsync(modelt.UserId);
            }
            return NoContent();
        }

        #region 管理员API接口===========================
        /// <summary>
        /// 审核一条记录
        /// 示例：/admin/member/audit
        /// </summary>
        [HttpPut("/admin/member/audit")]
        [Authorize]
        [AuthorizeFilter("Member", ActionType.Audit)]
        public async Task<IActionResult> Audit([FromQuery] string Ids)
        {
            if (Ids == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不可为空"));
            }
            //将ID列表转换成IEnumerable
            var listIds = Ids.ToIEnumerable<int>();
            if (listIds == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不符合规范"));
            }
            //将符合条件的会员ID一次查询出来
            var list = await _memberService.QueryListAsync(0, x => x.User.Status == 2 && listIds.Contains(x.UserId), "-RegTime,-Id", WriteRoRead.Write);
            //执行批量删除操作
            foreach (var modelt in list)
            {
                modelt.User.Status = 0;
            }
            var result = await _memberService.SaveAsync();
            return NoContent();
        }
        #endregion

        #region 前台调用接口============================
        /// <summary>
        /// 会员注册
        /// 示例：/account/member/register
        /// </summary>
        [HttpPost("/account/member/register")]
        public async Task<IActionResult> AccountAdd([FromBody] RegisterDto registerDto)
        {
            //检查验证码是否正确
            var cacheObj = MemoryCacheHelper.Get(registerDto.CodeKey);
            if (cacheObj == null)
            {
                throw new ResponseException("验证码已过期，请重新获取");
            }
            var cacheValue = cacheObj.ToString();
            var codeSecret = string.Empty;
            if (registerDto.Method == 1)
            {
                if (!registerDto.Phone.IsNotNullOrEmpty())
                {
                    throw new ResponseException("请填写手机号码");
                }
                codeSecret = MD5Helper.MD5Encrypt32(registerDto.Phone + registerDto.CodeValue);
            }
            else if (registerDto.Method == 2)
            {
                if (!registerDto.Email.IsNotNullOrEmpty())
                {
                    throw new ResponseException("请填写邮箱地址");
                }
                codeSecret = MD5Helper.MD5Encrypt32(registerDto.Email + registerDto.CodeValue);
            }
            else
            {
                if (!registerDto.UserName.IsNotNullOrEmpty())
                {
                    throw new ResponseException("请填写用户名");
                }
                codeSecret = registerDto.CodeValue;
            }
            if (cacheValue.ToLower() != codeSecret.ToLower())
            {
                throw new ResponseException("验证码有误，请重新获取");
            }
            //检查站点是否正确
            if (!await _siteService.ExistsAsync<Sites>(x => x.Id == registerDto.SiteId))
            {
                throw new ResponseException("所属站点不存在或已删除");
            }
            //取得会员参数设置
            var jsonData = await _sysConfigService.QueryByTypeAsync(ConfigType.MemberConfig);
            var memberConfig = JsonHelper.ToJson<MemberConfigDto>(jsonData.JsonData);
            var modelDto = _mapper.Map<MembersEditDto>(registerDto); //将注册DTO映射成修改DTO
            //检查系统是否开放注册
            if (memberConfig.RegStatus == 1)
            {
                throw new ResponseException("系统暂停开放注册，请稍候再试");
            }
            //检查保留用户名关健字
            if (modelDto.UserName.IsNotNullOrEmpty())
            {
                var keywords = memberConfig.RegKeywords.Split(',');
                if (keywords.Any(x => x.ToLower().Equals(modelDto.UserName.ToLower()))){
                    throw new ResponseException("用户名被系统保留，请更换");
                }
            }
            //检查同一IP注册的时间间隔
            if (memberConfig.RegCtrl > 0)
            {
                //获取客户IP地址
                var userIp = _httpContextAccessor.HttpContext?.Connection.RemoteIpAddress.ToString();
                //查找同一IP下最后的注册用户
                if (await _memberService.QueryAsync<Members>(x => x.RegIp.Equals(userIp)
                    && DateTime.Compare(x.RegTime.AddHours(memberConfig.RegCtrl), DateTime.Now) > 0) != null)
                {
                    throw new ResponseException($"同IP注册过于频繁，请稍候再试");
                }
            }
            //新用户是否开启人工审核
            if (memberConfig.RegVerify == 1)
            {
                modelDto.Status = 2;
            }
            
            var result = await _memberService.AddAsync(modelDto);
            MemoryCacheHelper.Remove(registerDto.CodeKey); //删除验证码缓存

            return Ok(result);
        }

        /// <summary>
        /// 根据当前会员信息
        /// 示例：/account/member
        /// </summary>
        [HttpGet("/account/member")]
        [Authorize]
        public async Task<IActionResult> AccountGetById([FromQuery] MemberParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<MembersDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //获取登录用户ID
            int userId = await _userService.GetUserIdAsync();
            if (userId == 0)
            {
                return NotFound(ResponseMessage.Error("用户尚未登录"));
            }
            //查询数据库获取实体
            var model = await _memberService.QueryAsync(x => x.UserId == userId);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"会员{userId}不存在或已删除"));
            }
            //根据字段进行塑形
            var result = _mapper.Map<MembersDto>(model).ShapeData(param.Fields);
            return Ok(result);
        }

        /// <summary>
        /// 修改一条记录
        /// 示例：/account/member/info
        /// </summary>
        [HttpPut("/account/member/info")]
        [Authorize]
        public async Task<IActionResult> Update([FromBody] MembersModifyDto modelDto)
        {
            //获取登录用户ID
            int userId = await _userService.GetUserIdAsync();
            if (userId == 0)
            {
                return NotFound(ResponseMessage.Error("用户尚未登录"));
            }
            var model = await _memberService.QueryAsync<Members>(x => x.UserId == userId, WriteRoRead.Write);
            _mapper.Map(modelDto, model);
            await _memberService.SaveAsync();
            return NoContent();
        }

        /// <summary>
        /// 修改当前会员密码
        /// 示例：/account/member/password
        /// </summary>
        [HttpPut("/account/member/password")]
        [Authorize]
        public async Task<IActionResult> AccountPassword([FromBody] PasswordDto modelDto)
        {
            await _userService.UpdatePasswordAsync(modelDto);
            return NoContent();
        }

        /// <summary>
        /// 根据当前会员信息(公共)
        /// 示例：/client/member
        /// </summary>
        [HttpGet("/client/member")]
        public async Task<IActionResult> ClientGetById([FromQuery] MemberParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<MembersDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //获取登录用户ID
            int userId = await _userService.GetUserIdAsync();
            if (userId == 0)
            {
                return NotFound(ResponseMessage.Error("用户尚未登录"));
            }
            //查询数据库获取实体
            var model = await _memberService.QueryAsync(x => x.UserId == userId);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"会员{userId}不存在或已删除"));
            }
            //根据字段进行塑形
            var result = _mapper.Map<MembersDto>(model).ShapeData(param.Fields);
            return Ok(result);
        }
        #endregion
    }
}
