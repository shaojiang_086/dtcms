﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DTcms.Core.API.Filters;
using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace DTcms.Core.API.Controllers
{
    /// <summary>
    /// 会员站内消息
    /// </summary>
    [Route("member/message")]
    [ApiController]
    public class MemberMessageController : ControllerBase
    {
        private readonly IMemberMessageService _memberMessageService;
        private readonly IMapper _mapper;

        public MemberMessageController(IMemberMessageService memberMessageService, IMapper mapper)
        {
            _memberMessageService = memberMessageService;
            _mapper = mapper;
        }

        /// <summary>
        /// 根据ID获取数据
        /// 示例：/member/message/1
        /// </summary>
        [HttpGet("{id}")]
        [Authorize]
        [AuthorizeFilter("MemberMessage", ActionType.View)]
        public async Task<IActionResult> GetById([FromRoute] long id, [FromQuery] BaseParameter param)
        {
            //检测参数是否合法
            if (!param.Fields.IsPropertyExists<MemberMessageDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }
            //查询数据库获取实体
            var resultFrom = await _memberMessageService.QueryAsync(x => x.Id == id);
            if (resultFrom == null)
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或已删除"));
            }
            //根据字段进行塑形
            var result = resultFrom.ShapeData(param.Fields);
            return Ok(result);
        }

        /// <summary>
        /// 获取指定数量列表
        /// 示例：/member/message/view/0
        /// </summary>
        [HttpGet("view/{top}")]
        [Authorize]
        public async Task<IActionResult> GetList([FromRoute] int top, [FromQuery] BaseParameter searchParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.Replace("-", "").IsPropertyExists<SitePaymentDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<MemberMessageDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据库列表
            var resultFrom = await _memberMessageService.QueryListAsync(top,
                x => (!searchParam.Keyword.IsNotNullOrEmpty() || x.UserName.Contains(searchParam.Keyword)),
                searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "-AddTime,-Id");
            //根据字段进行塑形
            var result = resultFrom.ShapeData(searchParam.Fields);
            //返回成功200
            return Ok(result);
        }

        /// <summary>
        /// 获取分页列表
        /// 示例：/member/message?pageSize=10&pageIndex=1
        /// </summary>
        [HttpGet]
        [Authorize]
        [AuthorizeFilter("MemberMessage", ActionType.View)]
        public async Task<IActionResult> GetList([FromQuery] BaseParameter searchParam, [FromQuery] PageParamater pageParam)
        {
            //检测参数是否合法
            if (searchParam.OrderBy.IsNotNullOrEmpty()
                && !searchParam.OrderBy.Replace("-", "").IsPropertyExists<MemberMessageDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的排序参数"));
            }
            if (!searchParam.Fields.IsPropertyExists<MemberMessageDto>())
            {
                return BadRequest(ResponseMessage.Error("请输入正确的属性参数"));
            }

            //获取数据列表，如果ID大于0则查询该用户下所有的列表
            var list = await _memberMessageService.QueryPageAsync(
                pageParam.PageSize,
                pageParam.PageIndex,
                x => (searchParam.Keyword.IsNotNullOrEmpty() ? x.UserName.Contains(searchParam.Keyword) : true),
                searchParam.OrderBy.IsNotNullOrWhiteSpace() ? searchParam.OrderBy : "-AddTime,-Id"
                );
            if (list == null || list.Count() <= 0)
            {
                return NotFound(ResponseMessage.Error("暂无查询到记录"));
            }

            //x-pagination
            var paginationMetadata = new
            {
                totalCount = list.TotalCount,
                pageSize = list.PageSize,
                pageIndex = list.PageIndex,
                totalPages = list.TotalPages
            };
            Response.Headers.Add("x-pagination", SerializeHelper.SerializeObject(paginationMetadata));

            //根据字段进行塑形，注意因为没有使用AotoMapper，所以要转换成Enumerable
            var resultDto = list.AsEnumerable<MemberMessageDto>().ShapeData(searchParam.Fields);
            return Ok(resultDto);
        }


        /// <summary>
        /// 添加一条记录
        /// 示例：/member/message
        /// </summary>
        [HttpPost]
        [Authorize]
        [AuthorizeFilter("MemberMessage", ActionType.Add)]
        public async Task<IActionResult> Add([FromBody] MemberMessageEditDto modelDto)
        {
            //检查会员是否存在
            if (!await _memberMessageService.ExistsAsync<Members>(x => x.UserId == modelDto.UserId))
            {
                return NotFound(ResponseMessage.Error($"会员ID{modelDto.UserId}不存在"));
            }
            //映射成实体
            var model = _mapper.Map<MemberMessage>(modelDto);
            //写入数据库
            await _memberMessageService.AddAsync(model);
            //查询刚添加的记录
            var result = await _memberMessageService.QueryAsync(x => x.Id == model.Id);
            return Ok(result);
        }

        /// <summary>
        /// 修改一条记录
        /// 示例：/member/message/1
        /// </summary>
        [HttpPut("{id}")]
        [Authorize]
        [AuthorizeFilter("MemberMessage", ActionType.Edit)]
        public async Task<IActionResult> Update([FromRoute] long id, [FromBody] MemberMessageEditDto modelDto)
        {
            //查找记录
            var model = await _memberMessageService.QueryAsync<MemberMessage>(x => x.Id == id);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或已删除"));
            }

            //因为多次进行查询，所以手动调用Update方法保存
            _mapper.Map(modelDto, model);
            var result = await _memberMessageService.UpdateAsync(model);
            return NoContent();
        }

        /// <summary>
        /// 局部更新一条记录
        /// 示例：/member/message/1
        /// Body：[{"op":"replace","path":"/title","value":"new title"}]
        /// </summary>
        [HttpPatch("{id}")]
        [Authorize]
        [AuthorizeFilter("MemberMessage", ActionType.Edit)]
        public async Task<IActionResult> Update([FromRoute] long id, [FromBody] JsonPatchDocument<MemberMessageEditDto> patchDocument)
        {
            //检查记录是否存在
            var model = await _memberMessageService.QueryAsync<MemberMessage>(x => x.Id == id, WriteRoRead.Write);
            if (model == null)
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或已删除"));
            }

            var modelToPatch = _mapper.Map<MemberMessageEditDto>(model);
            patchDocument.ApplyTo(modelToPatch, ModelState);
            //验证数据是否合法
            if (!TryValidateModel(modelToPatch))
            {
                return ValidationProblem(ModelState);
            }
            //更新操作AutoMapper替我们完成，只需要调用保存即可
            _mapper.Map(modelToPatch, model);
            await _memberMessageService.SaveAsync();
            return NoContent();
        }

        /// <summary>
        /// 删除一条记录
        /// 示例：/member/message/1
        /// </summary>
        [HttpDelete("{id}")]
        [Authorize]
        [AuthorizeFilter("MemberMessage", ActionType.Delete)]
        public async Task<IActionResult> Delete([FromRoute] long id)
        {
            //检查参数是否正确
            if (!await _memberMessageService.ExistsAsync<MemberMessage>(x => x.Id == id))
            {
                return NotFound(ResponseMessage.Error($"数据{id}不存在或已删除"));
            }
            var result = await _memberMessageService.DeleteAsync<MemberMessage>(x => x.Id == id);
            return NoContent();
        }

        /// <summary>
        /// 批量删除记录
        /// 示例：/member/message?ids=1,2,3
        /// </summary>
        [HttpDelete]
        [Authorize]
        [AuthorizeFilter("MemberMessage", ActionType.Delete)]
        public async Task<IActionResult> DeleteByIds([FromQuery] string Ids)
        {
            //检查参数是否为空
            if (Ids == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不可为空"));
            }
            //将ID列表转换成IEnumerable
            var listIds = Ids.ToIEnumerable<long>();
            if (listIds == null)
            {
                return BadRequest(ResponseMessage.Error("传输参数不符合规范"));
            }
            //执行批量删除操作
            await _memberMessageService.DeleteAsync<MemberMessage>(x => listIds.Contains(x.Id));
            return NoContent();
        }
    }
}
