﻿using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DTcms.Core.API.Filters
{
    /// <summary>
    /// 管理日志过滤器
    /// </summary>
    public class GlobalRequestFilter : IAsyncResourceFilter
    {
        private readonly IManagerLogService _managerLogService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public GlobalRequestFilter(IManagerLogService managerLogService,IHttpContextAccessor httpContextAccessor)
        {
            _managerLogService = managerLogService;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
        {
            await next();
            var method = context.HttpContext.Request.Method;
            var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Name)?.Value;
            //记录日志
            if (userName != null
                && (method.ToLower().Equals("post")
                || method.ToLower().Equals("put")
                || method.ToLower().Equals("patch")
                || method.ToLower().Equals("delete")))
            {
                ManagerLog model = new ManagerLog
                {
                    UserName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Name)?.Value,
                    Method = method,
                    Path = context.HttpContext.Request.Path,
                    Query = context.HttpContext.Request.QueryString.ToString(),
                    StatusCode = context.HttpContext.Response.StatusCode.ToString(),
                    AddTime = DateTime.Now
                };
                await _managerLogService.AddAsync<ManagerLog>(model);
            }
        }
    }
}
