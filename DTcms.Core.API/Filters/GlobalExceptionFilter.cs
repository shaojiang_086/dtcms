﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTcms.Core.API.Filters
{
    /// <summary>
    /// 全局异常过滤器
    /// </summary>
    public class GlobalExceptionFilter : IExceptionFilter
    {
        private readonly ILogger<GlobalExceptionFilter> _logger;
        private readonly IWebHostEnvironment _environment;

        public GlobalExceptionFilter(ILogger<GlobalExceptionFilter> logger, IWebHostEnvironment environment)
        {
            _logger = logger;
            _environment = environment;
        }

        public void OnException(ExceptionContext context)
        {
            if (context.Exception is ResponseException cmsException)
            {
                ResponseMessage warnResponse = new ResponseMessage(cmsException.GetErrorCode(), cmsException.Message, context.HttpContext);
                _logger.LogWarning(SerializeHelper.SerializeObject(warnResponse));
                HandlerException(context, warnResponse, cmsException.GetCode());
                return;
            }
            string error = "异常信息：";
            void ReadException(Exception ex)
            {
                error += $"{ex.Message} | {ex.StackTrace} | {ex.InnerException}";
                if (ex.InnerException != null)
                {
                    ReadException(ex.InnerException);
                }
            }
            ReadException(context.Exception);

            _logger.LogError(error);//记录错误日志

            ResponseMessage apiResponse = new ResponseMessage(ErrorCode.UnknownError, _environment.IsDevelopment() ? error : "服务器正忙，请稍后再试.", context.HttpContext);
            HandlerException(context, apiResponse, StatusCodes.Status500InternalServerError);
        }

        private void HandlerException(ExceptionContext context, ResponseMessage apiResponse, int statusCode)
        {
            context.Result = new JsonResult(apiResponse)
            {
                StatusCode = statusCode,
                ContentType = "application/json",
            };
            context.ExceptionHandled = true;
        }
    }
}
