# DTcms

#### Description
启航内容管理系统(DTcms)是国内ASP.NET开源界少见的优秀开源网站管理系统，基于 ASP.NETCORE的技术开发，开放源代码。轻量级架构，后台使用原始的开发方式，无任何技术门槛，使得开发人员更容易上手。注重后台管理界面，采用vue界面设计，兼容主流浏览器响应式后台管理界面，支持电脑、移动设备，小程序等终端使用。目前是深圳市动力启航软件有限公司旗下一个开源软件产品，最早创建于2009年10月，其宗旨是让更多的编程爱好者分享交流互联网开发技术。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
