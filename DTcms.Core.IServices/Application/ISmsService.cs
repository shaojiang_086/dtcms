﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 手机短信接口
    /// </summary>
    public interface ISmsService : IBaseService
    {
        /// <summary>
        /// 发送短信
        /// </summary>
        Task<SmsResultDto> Send(SmsMessageDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询账户剩余短信数量
        /// </summary>
        Task<SmsResultDto> GetAccountQuantity(WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询已发送数量
        /// </summary>
        Task<SmsResultDto> GetSendQuantity(WriteRoRead writeAndRead = WriteRoRead.Read);
    }
}
