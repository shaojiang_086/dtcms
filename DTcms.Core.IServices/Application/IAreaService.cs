﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 全国省市区接口
    /// </summary>
    public interface IAreaService : IBaseService
    {
        /// <summary>
        /// 返回所有地区目录树
        /// </summary>
        Task<IEnumerable<AreasDto>> QueryListAsync(int parentId, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件删除数据(迭代删除)
        /// </summary>
        Task<bool> DeleteAsync(Expression<Func<Areas, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Write);
    }
}
