﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 站点频道
    /// </summary>
    public interface ISiteChannelService : IBaseService
    {
        /// <summary>
        /// 获取频道信息(含扩展字段)
        /// </summary>
        Task<SiteChannel> QueryAsync(Expression<Func<SiteChannel, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询分页列表
        /// </summary>
        Task<PaginationList<SiteChannel>> QueryPageAsync(int pageSize, int pageIndex, Expression<Func<SiteChannel, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 添加频道(含菜单导航)
        /// </summary>
        Task<SiteChannelDto> AddAsync(SiteChannelEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 修改一条记录
        /// </summary>
        Task<bool> UpdateAsync(int id, SiteChannelEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 删除频道(含扩展字段及菜单)
        /// </summary>
        Task<bool> DeleteAsync(SiteChannel model, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 删除频道(含扩展字段及菜单)
        /// </summary>
        Task<bool> DeleteAsync(Expression<Func<SiteChannel, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Write);
    }
}
