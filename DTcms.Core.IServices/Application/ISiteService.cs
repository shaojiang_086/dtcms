﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 站点
    /// 接口继承父接口，拥父接口的方法
    /// </summary>
    public interface ISiteService : IBaseService
    {
        /// <summary>
        /// 查询默认站点信息
        /// </summary>
        Task<Sites> QueryDefaultAsync(WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据域名查询站点信息
        /// </summary>
        Task<Sites> QueryByDomainAsync(string domain, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 获取站点信息(带域名)
        /// </summary>
        Task<Sites> QueryAsync(Expression<Func<Sites, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 获取列表(带域名)
        /// </summary>
        Task<IEnumerable<Sites>> QueryListAsync(int top, Expression<Func<Sites, bool>> funcWhere, string orderBy,
            WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 获取分页列表(带域名)
        /// </summary>
        Task<PaginationList<Sites>> QueryPageAsync(int pageSize, int pageIndex, Expression<Func<Sites, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 获取站点总数量
        /// </summary>
        Task<int> QueryCountAsync(Expression<Func<Sites, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据IDS获取列表(带域名)
        /// </summary>
        Task<IEnumerable<Sites>> QueryListAsync(IEnumerable<int> ids, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 添加站点(含菜单创建)
        /// </summary>
        Task<SitesDto> AddAsync(SitesEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 修改站点(含菜单修改)
        /// </summary>
        Task<bool> UpdateAsync(int id, SitesEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 删除站点(含菜单删除)
        /// </summary>
        Task<bool> DeleteAsync(Expression<Func<Sites, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Write);
    }
}
