﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 管理员信息接口
    /// </summary>
    public interface IManagerService : IBaseService
    {
        /// <summary>
        /// 获取管理员信息
        /// </summary>
        Task<Manager> QueryAsync(Expression<Func<Manager, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 获取管理员分页列表
        /// </summary>
        Task<PaginationList<Manager>> QueryPageAsync(int pageSize, int pageIndex, Expression<Func<Manager, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 添加管理员
        /// </summary>
        Task<ManagerDto> AddAsync(ManagerEditDto modelDto);

        /// <summary>
        /// 修改管理员
        /// </summary>
        Task<bool> UpdateAsync(int userId, ManagerEditDto modelDto);

        /// <summary>
        /// 删除管理员
        /// </summary>
        Task<bool> DeleteAsync(int userId);
    }
}
