﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 管理员角色接口
    /// </summary>
    public interface IManagerRoleService : IBaseService
    {
        /// <summary>
        /// 查询一条记录
        /// </summary>
        Task<ManagerRolesDto> QueryAsync(Expression<Func<ApplicationRole, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询指定数量列表
        /// </summary>
        Task<IEnumerable<ApplicationRole>> QueryListAsync(int top, Expression<Func<ApplicationRole, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询分页列表
        /// </summary>
        Task<PaginationList<ApplicationRole>> QueryPageAsync(int pageSize, int pageIndex, Expression<Func<ApplicationRole, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 添加一条记录
        /// </summary>
        Task<int> AddAsync(ManagerRolesEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 修改一条记录
        /// </summary>
        Task<bool> UpdateAsync(int id, ManagerRolesEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 根据条件删除记录
        /// </summary>
        Task<bool> DeleteAsync(Expression<Func<ApplicationRole, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Write);
    }
}
