﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 站点支付接口
    /// </summary>
    public interface ISitePaymentService : IBaseService
    {
        /// <summary>
        /// 查询一条记录
        /// </summary>
        Task<SitePayment> QueryAsync(Expression<Func<SitePayment, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询指定数量列表
        /// </summary>
        Task<IEnumerable<SitePayment>> QueryListAsync(int top, Expression<Func<SitePayment, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询分页列表
        /// </summary>
        Task<PaginationList<SitePayment>> QueryPageAsync(int pageSize, int pageIndex, Expression<Func<SitePayment, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);
    }
}
