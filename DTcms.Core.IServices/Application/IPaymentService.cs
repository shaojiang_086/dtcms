﻿using DTcms.Core.Common.Emum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 支付方式接口
    /// </summary>
    public interface IPaymentService : IBaseService
    {
        /// <summary>
        /// 删除支付方式(含站点支付方式删除)
        /// </summary>
        Task<bool> DeleteAsync(int id, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 批量删除(含站点支付方式删除)
        /// </summary>
        Task<bool> DeleteAsync(IEnumerable<int> listIds, WriteRoRead writeAndRead = WriteRoRead.Write);
    }
}
