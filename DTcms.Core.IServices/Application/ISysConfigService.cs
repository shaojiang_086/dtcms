﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 系统配置接口
    /// </summary>
    public interface ISysConfigService : IBaseService
    {
        /// <summary>
        /// 根据配置类型返回相应数据
        /// </summary>
        Task<SysConfig> QueryByTypeAsync(ConfigType configType, WriteRoRead writeAndRead = WriteRoRead.Read);
    }
}
