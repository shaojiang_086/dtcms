﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 文章附件
    /// </summary>
    public interface IArticleAttachService : IBaseService
    {
        /// <summary>
        /// 更新下载数量
        /// </summary>
        /// <returns></returns>
        Task<bool> UpdateDownCount(long id, ApplicationUser appUser, WriteRoRead writeAndRead = WriteRoRead.Write);
    }
}
