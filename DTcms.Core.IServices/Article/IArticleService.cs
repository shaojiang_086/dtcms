﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 文章接口
    /// </summary>
    public interface IArticleService : IBaseService
    {
        /// <summary>
        /// 查询一条记录
        /// </summary>
        Task<Articles> QueryAsync(Expression<Func<Articles, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 修改一条记录
        /// </summary>
        Task<bool> UpdateAsync(long id, ArticlesEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);
        /// <summary>
        /// 更新数据(局部更新)
        /// </summary>
        Task<bool> UpdateAsync(Articles model, Expression<Func<Articles, int>> funcProperty, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 更新浏览量(局部更新)
        /// </summary>
        Task<int> UpdateClickAsync(long articleId, int clickCount, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 查询文章分页数据
        /// </summary>
        /// <param name="pageSize">页大小</param>
        /// <param name="pageIndex">当前页</param>
        /// <param name="funcWhere">查询条件</param>
        /// <param name="orderBy">排序</param>
        /// <param name="writeAndRead"></param>
        /// <returns></returns>
        Task<PaginationList<Articles>> QueryPageAsync(int pageSize, int pageIndex,
            Expression<Func<Articles, bool>> funcWhere,
            string orderBy,
            WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 获取记录总数量
        /// </summary>
        Task<int> QueryCountAsync(Expression<Func<Articles, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询文章前几条数据
        /// </summary>
        /// <param name="top">前几条</param>
        /// <param name="funcWhere">查询条件</param>
        /// <param name="orderBy">排序</param>
        /// <param name="writeAndRead"></param>
        /// <returns></returns>
        Task<IEnumerable<Articles>> QueryListAsync(int top,
            Expression<Func<Articles, bool>> funcWhere,
            string orderBy,
            WriteRoRead writeAndRead = WriteRoRead.Read);
    }
}
