﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 文章评论接口
    /// </summary>
    public interface IArticleCommentService : IBaseService
    {
        /// <summary>
        /// 查询一条记录
        /// </summary>
        Task<ArticleComment> QueryAsync(Expression<Func<ArticleComment, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询分页列表(树形)
        /// </summary>
        Task<PaginationList<ArticleCommentDto>> QueryPageTreeAsync(int pageSize, int pageIndex, Expression<Func<ArticleComment, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询分页列表
        /// </summary>
        Task<PaginationList<ArticleComment>> QueryPageAsync(int pageSize, int pageIndex, Expression<Func<ArticleComment, bool>> funcWhere, string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询分页列表
        /// </summary>
        Task<PaginationList<ArticleCommentDto>> QueryPageFromDtoAsync(int pageSize, int pageIndex, Expression<Func<ArticleComment, bool>> funcWhere, string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件删除一条记录
        /// </summary>
        Task<bool> DeleteAsync(Expression<Func<ArticleComment, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 软删除一条数据
        /// </summary>
        Task<bool> MarkDeleteAsync(Expression<Func<ArticleComment, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 获取记录总数量
        /// </summary>
        Task<int> QueryCountAsync(Expression<Func<ArticleComment, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询前几条数据
        /// </summary>
        Task<IEnumerable<ArticleCommentDto>> QueryListAsync(int top, Expression<Func<ArticleComment, bool>> funcWhere, string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 查询前几条数据(返回树)
        /// </summary>
        Task<IEnumerable<ArticleCommentDto>> QueryListTreeAsync(int top, Expression<Func<ArticleComment, bool>> funcWhere, string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 添加一条数据
        /// </summary>
        Task<ArticleComment> AddAsync(ArticleCommentAddDto modelDto);
    }
}
