﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 文章投稿接口
    /// </summary>
    public interface IArticleContributeService : IBaseService
    {
        /// <summary>
        /// 修改一条记录
        /// </summary>
        Task<bool> UpdateAsync(long id, ArticleContributeEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 修改一条记录
        /// </summary>
        Task<bool> UserUpdateAsync(long id, ArticleContributeEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 获取稿件总数量
        /// </summary>
        Task<int> QueryCountAsync(Expression<Func<ArticleContribute, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);
    }
}
