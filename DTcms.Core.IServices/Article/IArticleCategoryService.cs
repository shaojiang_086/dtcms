﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 文章类别接口
    /// </summary>
    public interface IArticleCategoryService : IBaseService
    {
        /// <summary>
        /// 根据父ID返回一级列表
        /// </summary>
        Task<IEnumerable<ArticleCategory>> QueryListAsync(int channelId, int top, long parentId, WriteRoRead writeAndRead = WriteRoRead.Read);
        /// <summary>
        /// 根据父ID返回目录树
        /// </summary>
        Task<IEnumerable<ArticleCategoryDto>> QueryListAsync(int channelId, long parentId, WriteRoRead writeAndRead = WriteRoRead.Read);
        /// <summary>
        /// 根据条件删除数据(迭代删除)
        /// </summary>
        Task<bool> DeleteAsync(Expression<Func<ArticleCategory, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Write);
        /// <summary>
        /// 更新一条数据(重组父子关系)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="modelDto"></param>
        /// <param name="writeAndRead"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(long id, ArticleCategoryEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);
    }
}
