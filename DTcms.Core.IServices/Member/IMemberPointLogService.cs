﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 会员积分记录接口
    /// </summary>
    public interface IMemberPointLogService : IBaseService
    {
        /// <summary>
        /// 根据条件获取一条记录
        /// </summary>
        Task<MemberPointLogDto> QueryAsync(Expression<Func<MemberPointLogDto, bool>> funcWhere,
            WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件获取列表
        /// </summary>
        Task<IEnumerable<MemberPointLogDto>> QueryListAsync(int top, Expression<Func<MemberPointLogDto, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件获取分页列表
        /// </summary>
        Task<PaginationList<MemberPointLogDto>> QueryPageAsync(int pageSize, int pageIndex,
            Expression<Func<MemberPointLogDto, bool>> funcWhere, string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 新增一条数据，须检查是否正数，正数须同时增加会员积分及经验值，检查升级
        /// </summary>
        Task<MemberPointLog> AddAsync(MemberPointLog model, WriteRoRead writeAndRead = WriteRoRead.Write);
    }
}
