﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 会员组接口
    /// </summary>
    public interface IMemberGroupService : IBaseService
    {
        /// <summary>
        /// 根据条件获取一条记录
        /// </summary>
        Task<MemberGroup> QueryAsync(Expression<Func<MemberGroup, bool>> funcWhere,
            WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件获取一条记录
        /// </summary>
        Task<MemberGroup> QueryAsync(Expression<Func<MemberGroup, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);
    }
}
