﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 站内消息接口
    /// </summary>
    public interface IMemberMessageService : IBaseService
    {
        /// <summary>
        /// 根据条件获取一条记录
        /// </summary>
        Task<MemberMessageDto> QueryAsync(Expression<Func<MemberMessageDto, bool>> funcWhere,
            WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件获取列表
        /// </summary>
        Task<IEnumerable<MemberMessageDto>> QueryListAsync(int top, Expression<Func<MemberMessageDto, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件获取分页列表
        /// </summary>
        Task<PaginationList<MemberMessageDto>> QueryPageAsync(int pageSize, int pageIndex,
            Expression<Func<MemberMessageDto, bool>> funcWhere, string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);
    }
}
