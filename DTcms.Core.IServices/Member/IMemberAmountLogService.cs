﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 会员金额日志接口
    /// </summary>
    public interface IMemberAmountLogService : IBaseService
    {
        /// <summary>
        /// 根据条件获取一条记录
        /// </summary>
        Task<MemberAmountLogDto> QueryAsync(Expression<Func<MemberAmountLogDto, bool>> funcWhere,
            WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件获取列表
        /// </summary>
        Task<IEnumerable<MemberAmountLogDto>> QueryListAsync(int top, Expression<Func<MemberAmountLogDto, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件获取分页列表
        /// </summary>
        Task<PaginationList<MemberAmountLogDto>> QueryPageAsync(int pageSize, int pageIndex,
            Expression<Func<MemberAmountLogDto, bool>> funcWhere, string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 新增余额记录，须同时增加会员余额，检查升级
        /// </summary>
        Task<MemberAmountLog> AddAsync(MemberAmountLog model, WriteRoRead writeAndRead = WriteRoRead.Write);
    }
}
