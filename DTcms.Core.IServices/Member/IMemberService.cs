﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 会员信息接口
    /// </summary>
    public interface IMemberService : IBaseService
    {
        /// <summary>
        /// 获取会员信息
        /// </summary>
        Task<Members> QueryAsync(Expression<Func<Members, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件获取列表
        /// </summary>
        Task<IEnumerable<Members>> QueryListAsync(int top, Expression<Func<Members, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件获取分页列表
        /// </summary>
        Task<PaginationList<Members>> QueryPageAsync(int pageSize, int pageIndex, Expression<Func<Members, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 获取会员总数量
        /// </summary>
        Task<int> QueryCountAsync(Expression<Func<Members, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 统计会员总数量列表
        /// </summary>
        Task<IEnumerable<MembersReportDto>> QueryCountListAsync(int top, Expression<Func<Members, bool>> funcWhere,
            WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 添加会员
        /// </summary>
        Task<MembersDto> AddAsync(MembersEditDto modelDto);

        /// <summary>
        /// 修改会员
        /// </summary>
        Task<bool> UpdateAsync(int userId, MembersEditDto modelDto);

        /// <summary>
        /// 删除会员
        /// </summary>
        Task<bool> DeleteAsync(int userId);
    }
}
