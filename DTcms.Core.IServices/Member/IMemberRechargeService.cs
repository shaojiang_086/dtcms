﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    /// <summary>
    /// 会员充值接口
    /// </summary>
    public interface IMemberRechargeService : IBaseService
    {
        /// <summary>
        /// 根据条件获取一条记录
        /// </summary>
        Task<MemberRechargeDto> QueryAsync(Expression<Func<MemberRechargeDto, bool>> funcWhere,
            WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件获取列表
        /// </summary>
        Task<IEnumerable<MemberRechargeDto>> QueryListAsync(int top, Expression<Func<MemberRechargeDto, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 根据条件获取分页列表
        /// </summary>
        Task<PaginationList<MemberRechargeDto>> QueryPageAsync(int pageSize, int pageIndex,
            Expression<Func<MemberRechargeDto, bool>> funcWhere, string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 新增一条记录
        /// </summary>
        Task<MemberRecharge> AddAsync(MemberRechargeEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 充值成功后修改状态，添加余额记录，增加会员余额，根据单次充值金额检查升级
        /// </summary>
        /// <param name="rechargeNo">充值单号</param>
        Task<bool> CompleteAsync(string rechargeNo, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 修改支付方式
        /// </summary>
        Task<bool> PayAsync(MemberRechargePaymentDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write);

        /// <summary>
        /// 获取充值总数量
        /// </summary>
        Task<int> QueryCountAsync(Expression<Func<MemberRecharge, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);

        /// <summary>
        /// 获取充值总金额
        /// </summary>
        Task<decimal> QueryAmountAsync(Expression<Func<MemberRecharge, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read);
    }
}
