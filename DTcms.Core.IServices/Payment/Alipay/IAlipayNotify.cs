﻿using DTcms.Core.Model.Alipay;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices.Alipay
{
    public interface IAlipayNotify
    {
        /// <summary>
        /// 电脑支付回调通知
        /// </summary>
        Task<AlipayPageNotifyDto> PagePay(HttpRequest request);
    }
}
