﻿using DTcms.Core.Model.Alipay;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices.Alipay
{
    /// <summary>
    /// 支付宝支付接口
    /// </summary>
    public interface IAlipayExecute
    {
        /// <summary>
        /// 电脑网站下单支付
        /// </summary>
        Task<AlipayPageParamDto> PcPayAsync(AlipayTradeDto modelDto);
    }
}
