﻿using DTcms.Core.Model.WeChatPay;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices.WeChatPay
{
    public interface IWeChatPayNotify
    {
        /// <summary>
        /// 确认解密回调信息
        /// </summary>
        Task<WeChatPayNotifyDecryptDto> ConfirmAsync(HttpRequest request, int paymentId);
    }
}
