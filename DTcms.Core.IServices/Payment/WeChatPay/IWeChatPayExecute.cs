﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Model.WeChatPay;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices.WeChatPay
{
    /// <summary>
    /// 支付接口
    /// </summary>
    public interface IWeChatPayExecute
    {
        /// <summary>
        /// 扫码下单支付
        /// </summary>
        Task<WeChatPayNativeParamDto> NativePayAsync(WeChatPayDto modelDto);
    }
}
