﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.IServices
{
    public interface IBaseService
    {
        /// <summary>
        /// 检查记录是否存在
        /// </summary>
        abstract Task<bool> ExistsAsync<T>(Expression<Func<T, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read) where T : class;

        /// <summary>
        /// 取得最新一条记录
        /// </summary>
        abstract Task<T> QueryAsync<T>(WriteRoRead writeAndRead = WriteRoRead.Read) where T : class;

        /// <summary>
        /// 查询一条记录
        /// </summary>
        abstract Task<T> QueryAsync<T>(Expression<Func<T, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read) where T : class;

        /// <summary>
        /// 返回指定数量数据
        /// </summary>
        abstract Task<IEnumerable<T>> QueryListAsync<T>(int top, Expression<Func<T, bool>> funcWhere,
            WriteRoRead writeAndRead = WriteRoRead.Read) where T : class;

        /// <summary>
        /// 返回指定数量数据，带排序
        /// </summary>
        /// <param name="top">显示条数</param>
        /// <param name="funcWhere">查询表达式</param>
        /// <param name="orderBy">排序</param>
        abstract Task<IEnumerable<T>> QueryListAsync<T>(int top, Expression<Func<T, bool>> funcWhere, string orderBy,
            WriteRoRead writeAndRead = WriteRoRead.Read) where T : class;

        /// <summary>
        /// 返回分页记录，带排序
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="totalCount">总记录数</param>
        /// <param name="funcWhere">查询表达式</param>
        /// <param name="orderBy">排序</param>
        abstract IQueryable<T> QueryPage<T>(int pageSize, int pageIndex, out int totalCount, Expression<Func<T, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read) where T : class;

        abstract Task<PaginationList<T>> QueryPageAsync<T>(int pageSize, int pageIndex, Expression<Func<T, bool>> funcWhere, string orderBy,
            WriteRoRead writeAndRead = WriteRoRead.Read) where T : class;

        /// <summary>
        /// 新增一条数据
        /// </summary>
        abstract Task<T> AddAsync<T>(T t, WriteRoRead writeAndRead = WriteRoRead.Write) where T : class;

        /// <summary>
        /// 新增多条数据(带事务)
        /// </summary>
        abstract Task<IEnumerable<T>> AddAsync<T>(IEnumerable<T> tList, WriteRoRead writeAndRead = WriteRoRead.Write) where T : class;

        /// <summary>
        /// 修改一条数据
        /// </summary>
        abstract Task<bool> UpdateAsync<T>(T t, WriteRoRead writeAndRead = WriteRoRead.Write) where T : class;

        /// <summary>
        /// 修改多条数据(带事务)
        /// </summary>
        abstract Task<bool> UpdateAsync<T>(IEnumerable<T> tList, WriteRoRead writeAndRead = WriteRoRead.Write) where T : class;

        /// <summary>
        /// 根据条件删除数据
        /// </summary>
        abstract Task<bool> DeleteAsync<T>(Expression<Func<T, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Write) where T : class;

        /// <summary>
        /// 根据实体删除一条数据
        /// </summary>
        abstract Task<bool> DeleteAsync<T>(T t, WriteRoRead writeAndRead = WriteRoRead.Write) where T : class;

        /// <summary>
        /// 删除多条数据(带事务)
        /// </summary>
        abstract Task<bool> DeleteAsync<T>(IEnumerable<T> tList, WriteRoRead writeAndRead = WriteRoRead.Write) where T : class;

        /// <summary>
        /// 保存数据(为了保证事务)
        /// </summary>
        Task<bool> SaveAsync();
    }
}
