﻿using DTcms.Core.Common.Emum;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.DBFactory.Database
{
    public interface IDbContextFactory
    {
        AppDbContext CreateContext(WriteRoRead writeRoRead);
    }
}
