﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Helper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.DBFactory.Database
{
    public class DbContextFactory : IDbContextFactory
    {
        private AppDbContext content;
        private readonly DbContextOption _Options;

        private static int _iSeed = 0;
        private static bool _isSet = true;
        private static readonly object _ObjectisSet_Look = new object();

        public DbContextFactory(/*IOptionsMonitor<DbContextOption> Options*/)
        {
            //_Options = Options.CurrentValue;
            //读取配置文件数据库连接字符串
            _Options = Appsettings.ToObject<DbContextOption>(new string[] { "ConnectionStrings"});
            //保证第一次初始化时对其赋值
            if (_isSet)
            {
                lock (_ObjectisSet_Look)
                {
                    if (_isSet)
                    {
                        _iSeed = _Options.ReadConnectionList.Count;
                        _isSet = false;
                    }
                }
            }
        }

        public AppDbContext CreateContext(WriteRoRead writeRoRead)
        {
            switch (writeRoRead)
            {
                ///写数据
                case WriteRoRead.Write:
                    content = new AppDbContext(_Options.DBType, _Options.WriteConnection);
                    break;
                //读数据
                case WriteRoRead.Read:
                    content = new AppDbContext(_Options.DBType, QueryStrategy());
                    break;
            }
            return content;
        }

        #region 私有辅助函数
        /// <summary>
        /// 选择策略
        /// </summary>
        private string QueryStrategy()
        {
            switch (_Options.Strategy)
            {
                case Strategy.Polling:
                    //轮循策略
                    return Polling();
                case Strategy.Random:
                    //随机策略
                    return Random();
                default:
                    throw new Exception("分库查询策略不存在");
            }
        }

        /// <summary>
        /// 随机策略
        /// </summary>
        private string Random()
        {
            int i = new Random().Next(0, _Options.ReadConnectionList.Count);
            return _Options.ReadConnectionList[i];
        }

        /// <summary>
        /// 轮循策略
        /// </summary>
        /// <returns></returns>
        private string Polling()
        {
            return _Options.ReadConnectionList[_iSeed++ % _Options.ReadConnectionList.Count];
        }
        #endregion
    }
}
