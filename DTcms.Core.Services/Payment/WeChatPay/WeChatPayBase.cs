﻿using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.IServices;
using DTcms.Core.IServices.WeChatPay;
using DTcms.Core.Model.WeChatPay;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.Services.WeChatPay
{
    /// <summary>
    /// 微信支付基类
    /// </summary>
    public class WeChatPayBase
    {
        private readonly ISitePaymentService _sitePaymentService;
        public WeChatPayBase(ISitePaymentService sitePaymentService)
        {
            _sitePaymentService = sitePaymentService;
        }

        /// <summary>
        /// 获取微信支付账户
        /// </summary>
        public async Task<WeChatPayAccountDto> GetAccountAsync(int id)
        {
            //取得微信支付账户
            var payModel = await _sitePaymentService.QueryAsync(x => x.Id == id);
            if (payModel == null)
            {
                throw new ResponseException("支付方式有误，请联系客服");
            }
            var model = new WeChatPayAccountDto
            {
                SiteId = payModel.SiteId,
                AppId = payModel.Key1,
                AppSecret = payModel.Key2,
                MchId = payModel.Key3,
                Apiv3Key = payModel.Key4,
                CertPath = payModel.Key5,
                NotifyUrl = payModel.Payment.NotifyUrl
            };
            if (string.IsNullOrEmpty(model.AppId)
                || string.IsNullOrEmpty(model.AppSecret)
                || string.IsNullOrEmpty(model.MchId)
                || string.IsNullOrEmpty(model.Apiv3Key)
                || string.IsNullOrEmpty(model.CertPath)
                || string.IsNullOrEmpty(model.NotifyUrl))
            {
                throw new ResponseException("支付方式设置有误，请联系客服");
            }
            model.CertPath = FileHelper.GetRootPath(model.CertPath);
            if (!FileHelper.FileExists(model.CertPath))
            {
                throw new ResponseException("证书文件不存在，请联系客服");
            }
            return model;
        }
    }
}
