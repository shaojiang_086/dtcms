﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.Services
{
    /// <summary>
    /// 会员组接口实现
    /// </summary>
    public class MemberGroupService : BaseService, IMemberGroupService
    {
        public MemberGroupService(IDbContextFactory contentFactory) : base(contentFactory) { }

        /// <summary>
        /// 根据条件获取一条记录
        /// </summary>
        public async Task<MemberGroup> QueryAsync(Expression<Func<MemberGroup, bool>> funcWhere,
            WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            //条件筛选并排序，返回第一条
            var result = await _context.Set<MemberGroup>().FirstOrDefaultAsync(funcWhere);
            return result;
        }

        /// <summary>
        /// 根据条件获取一条记录
        /// </summary>
        public async Task<MemberGroup> QueryAsync(Expression<Func<MemberGroup, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            //条件筛选并排序，返回第一条
            var result = _context.Set<MemberGroup>().Where(funcWhere).OrderByBatch(orderBy);
            return await result.FirstOrDefaultAsync();
        }
    }
}
