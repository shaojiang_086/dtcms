﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DTcms.Core.Services
{
    /// <summary>
    /// 会员附件下载记录接口实现
    /// </summary>
    public class MemberAttachLogService : BaseService, IMemberAttachLogService
    {
        public MemberAttachLogService(IDbContextFactory contentFactory) : base(contentFactory) { }

        /// <summary>
        /// 根据条件获取一条记录
        /// </summary>
        public async Task<MemberAttachLogDto> QueryAsync(Expression<Func<MemberAttachLogDto, bool>> funcWhere,
            WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            //联合查询重新组合
            var result = from a in _context.Set<MemberAttachLog>()
                         from u in _context.Set<ApplicationUser>().Where(u => u.Id == a.UserId)
                         select (new MemberAttachLogDto
                         {
                             Id = a.Id,
                             UserId = a.UserId,
                             UserName = u.UserName,
                             AttachId = a.AttachId,
                             FileName = a.FileName,
                             AddTime = a.AddTime
                         });
            //加入查询条件并返回一条
            return await result.FirstOrDefaultAsync(funcWhere);
        }

        /// <summary>
        /// 根据条件获取列表
        /// </summary>
        public async Task<IEnumerable<MemberAttachLogDto>> QueryListAsync(int top, Expression<Func<MemberAttachLogDto, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            //联合查询重新组合
            var result = from a in _context.Set<MemberAttachLog>()
                         from u in _context.Set<ApplicationUser>().Where(u => u.Id == a.UserId)
                         select (new MemberAttachLogDto
                         {
                             Id = a.Id,
                             UserId = a.UserId,
                             UserName = u.UserName,
                             AttachId = a.AttachId,
                             FileName = a.FileName,
                             AddTime = a.AddTime
                         });
            result = result.Where(funcWhere);//加入查询条件
            result = LinqExtensions.OrderByBatch(result, orderBy);//调用Linq扩展类进行排序
            if (top > 0) result = result.Take(top);//等于0显示所有数据
            return await result.ToListAsync();
        }

        /// <summary>
        /// 根据条件获取分页列表
        /// </summary>
        public async Task<PaginationList<MemberAttachLogDto>> QueryPageAsync(int pageSize, int pageIndex,
            Expression<Func<MemberAttachLogDto, bool>> funcWhere, string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            //联合查询重新组合
            var result = from a in _context.Set<MemberAttachLog>()
                         from u in _context.Set<ApplicationUser>().Where(u => u.Id == a.UserId)
                         select (new MemberAttachLogDto
                         {
                             Id = a.Id,
                             UserId = a.UserId,
                             UserName = u.UserName,
                             AttachId = a.AttachId,
                             FileName = a.FileName,
                             AddTime = a.AddTime
                         });
            result = result.Where(funcWhere);//加入查询条件
            result = LinqExtensions.OrderByBatch<MemberAttachLogDto>(result, orderBy);//调用Linq扩展类排序
            return await PaginationList<MemberAttachLogDto>.CreateAsync(pageIndex, pageSize, result);
        }
    }
}
