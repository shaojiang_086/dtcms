﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DTcms.Core.Services
{
    /// <summary>
    /// 站内消息接口实现
    /// </summary>
    public class MemberMessageService : BaseService, IMemberMessageService
    {
        public MemberMessageService(IDbContextFactory contentFactory) : base(contentFactory) { }

        /// <summary>
        /// 根据条件获取一条记录
        /// </summary>
        public async Task<MemberMessageDto> QueryAsync(Expression<Func<MemberMessageDto, bool>> funcWhere,
            WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            //联合查询重新组合
            var result = from a in _context.Set<MemberMessage>()
                         from u in _context.Set<ApplicationUser>().Where(u => u.Id == a.UserId)
                         select (new MemberMessageDto
                         {
                             Id = a.Id,
                             UserId = a.UserId,
                             UserName = u.UserName,
                             Title = a.Title,
                             Content = a.Content,
                             AddTime = a.AddTime,
                             IsRead = a.IsRead,
                             ReadTime = a.ReadTime
                         });
            //加入查询条件并返回一条
            return await result.FirstOrDefaultAsync(funcWhere);
        }

        /// <summary>
        /// 根据条件获取列表
        /// </summary>
        public async Task<IEnumerable<MemberMessageDto>> QueryListAsync(int top, Expression<Func<MemberMessageDto, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            //联合查询重新组合
            var result = from a in _context.Set<MemberMessage>()
                         from u in _context.Set<ApplicationUser>().Where(u => u.Id == a.UserId)
                         select (new MemberMessageDto
                         {
                             Id = a.Id,
                             UserId = a.UserId,
                             UserName = u.UserName,
                             Title = a.Title,
                             Content = a.Content,
                             AddTime = a.AddTime,
                             IsRead = a.IsRead,
                             ReadTime = a.ReadTime
                         });
            result = result.Where(funcWhere);//加入查询条件
            result = LinqExtensions.OrderByBatch(result, orderBy);//调用Linq扩展类进行排序
            if (top > 0) result = result.Take(top);//等于0显示所有数据
            return await result.ToListAsync();
        }

        /// <summary>
        /// 根据条件获取分页列表
        /// </summary>
        public async Task<PaginationList<MemberMessageDto>> QueryPageAsync(int pageSize, int pageIndex,
            Expression<Func<MemberMessageDto, bool>> funcWhere, string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            //联合查询重新组合
            var result = from a in _context.Set<MemberMessage>()
                         from u in _context.Set<ApplicationUser>().Where(u => u.Id == a.UserId)
                         select (new MemberMessageDto
                         {
                             Id = a.Id,
                             UserId = a.UserId,
                             UserName = u.UserName,
                             Title = a.Title,
                             Content = a.Content,
                             AddTime = a.AddTime,
                             IsRead = a.IsRead,
                             ReadTime = a.ReadTime
                         });
            result = result.Where(funcWhere);//加入查询条件
            result = LinqExtensions.OrderByBatch<MemberMessageDto>(result, orderBy);//调用Linq扩展类排序
            return await PaginationList<MemberMessageDto>.CreateAsync(pageIndex, pageSize, result);
        }
    }
}
