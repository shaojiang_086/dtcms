﻿using DTcms.Core.Common.Emum;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using DTcms.Core.Common.Extensions;
using Microsoft.EntityFrameworkCore;
using DTcms.Core.Model.ViewModels;
using System.Linq.Expressions;
using DTcms.Core.Common.Helper;
using AutoMapper;

namespace DTcms.Core.Services
{
    /// <summary>
    /// 文章类别接口实现
    /// </summary>
    public class ArticleCategoryService : BaseService, IArticleCategoryService
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public ArticleCategoryService(IDbContextFactory contentFactory,
            IUserService userService,
            IMapper mapper) : base(contentFactory)
        {
            _userService = userService;
            _mapper = mapper;

        }

        /// <summary>
        /// 更新一条数据(重组父子关系)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="modelDto"></param>
        /// <param name="writeAndRead"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(long id, ArticleCategoryEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            //查找记录
            var model = await _context.Set<ArticleCategory>()
                .FirstOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                throw new ResponseException($"数据{id}不存在或已删除", ErrorCode.NotFound);
            }
            //检查频道是否存在
            SiteChannel channelModel = await _context.Set<SiteChannel>().FirstOrDefaultAsync(x => x.Id == modelDto.ChannelId);
            if (channelModel == null)
            {
                throw new ResponseException($"频道{modelDto.ChannelId}不存在或已删除", ErrorCode.NotFound);
            }
            //添加站点主键
            model.SiteId = channelModel.SiteId;
            //检查站点是否存在
            if (await _context.Set<Sites>().FirstOrDefaultAsync(x => x.Id == model.SiteId) == null)
            {
                throw new ResponseException($"站点{model.SiteId}不存在或已删除", ErrorCode.NotFound);
            }
            //需要更新的集合
            List<ArticleCategory> updateList = new List<ArticleCategory>();
            #region 检查是否有子集
            var listData = await _context.Set<ArticleCategory>().Where(x => x.ChannelId.Equals(model.ChannelId)).ToListAsync();
            //检查是否有子集
            List<ArticleCategory> childList = GetChildList(listData, new List<ArticleCategory>(), model.Id);
            //检查现在父级是否在原来的子集中
            ArticleCategory parentModel = childList.Find(t => t.Id.Equals(modelDto.ParentId));
            if (parentModel != null)
            {
                //交换父级元素
                parentModel.ParentId = model.ParentId;
                updateList.Add(parentModel);
            }
            #endregion


            //获取当前用户名
            modelDto.UpdateBy = await _userService.GetUserNameAsync();
            modelDto.UpdateTime = DateTime.Now;
            //AutoMapper将DTO映射到源数据
            _mapper.Map(modelDto, model);
            //实体加入要修改的集合
            updateList.Add(model);
            //批量更新
            foreach (var t in updateList)
            {
                _context.Set<ArticleCategory>().Attach(t);
                _context.Entry<ArticleCategory>(t).State = EntityState.Modified;
            }
            return await this.SaveAsync();
        }
        /// <summary>
        /// 根据父ID返回一级列表
        /// </summary>
        public async Task<IEnumerable<ArticleCategory>> QueryListAsync(int channelId, int top, long parentId, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            var result = _context.Set<ArticleCategory>().Where(x => x.ParentId == parentId && x.ChannelId == channelId).OrderByBatch("SortId,Id");
            if (top > 0) result = result.Take(top);//等于0显示所有数据
            return await result.ToListAsync();
        }

        /// <summary>
        /// 根据父ID返回目录树
        /// </summary>
        public async Task<IEnumerable<ArticleCategoryDto>> QueryListAsync(int channelId, long parentId, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            var listData = await _context.Set<ArticleCategory>().Where(x => x.ChannelId.Equals(channelId)).ToListAsync();
            //调用递归重新生成目录树
            List<ArticleCategoryDto> result = await GetChilds(listData, parentId, 0);
            return result;
        }

        /// <summary>
        /// 根据条件删除数据(迭代删除)
        /// </summary>
        public async Task<bool> DeleteAsync(Expression<Func<ArticleCategory, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Write)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            var listData = await _context.Set<ArticleCategory>().ToListAsync();//查询所有数据
            var list = await _context.Set<ArticleCategory>().Where(funcWhere).ToListAsync();
            if (list == null)
            {
                return false;
            }
            foreach (var modelt in list)
            {
                await DeleteChilds(listData, modelt.Id);//删除子节点
                _context.RemoveRange(modelt);//删除当前节点
            }

            return await this.SaveAsync();
        }

        #region 辅助私有方法
        /// <summary>
        /// 迭代循环删除
        /// </summary>
        private async Task DeleteChilds(IEnumerable<ArticleCategory> listData, long parentId)
        {
            IEnumerable<ArticleCategory> models = listData.Where(x => x.ParentId == parentId);
            foreach (var modelt in models)
            {
                await DeleteChilds(listData, modelt.Id);//迭代
                _context.RemoveRange(modelt);
            }
        }

        /// <summary>
        /// 递归返回子级数，不是目录树
        /// </summary>
        /// <param name="data">主数据</param>
        /// <param name="parentId">父级</param>
        /// <returns></returns>
        private List<ArticleCategory> GetChildList(List<ArticleCategory> listData, List<ArticleCategory> list, long parentId)
        {
            IEnumerable<ArticleCategory> models = listData.Where(x => x.ParentId == parentId).OrderByBatch("SortId");//查找并排序
            if (models != null)
            {
                foreach (var m in models)
                {
                    list.Add(m);
                    GetChildList(listData, list, m.Id);
                }
            }
            return list;
        }

        /// <summary>
        /// 迭代循环返回目录树(私有方法)
        /// </summary>
        private async Task<List<ArticleCategoryDto>> GetChilds(IEnumerable<ArticleCategory> listData, long parentId, int classLayer)
        {
            classLayer++;
            List<ArticleCategoryDto> listDto = new List<ArticleCategoryDto>();
            IEnumerable<ArticleCategory> models = listData.Where(x => x.ParentId == parentId).OrderByBatch("SortId");//查找并排序
            foreach (ArticleCategory modelt in models)
            {
                ArticleCategoryDto modelDto = new ArticleCategoryDto
                {
                    Id = modelt.Id,
                    ParentId = modelt.ParentId,
                    SiteId = modelt.SiteId,
                    ClassLayer = classLayer,//深度
                    CallIndex = modelt.CallIndex,
                    Title = modelt.Title,
                    ImgUrl = modelt.ImgUrl,
                    LinkUrl = modelt.LinkUrl,
                    Content = modelt.Content,
                    SortId = modelt.SortId,
                    SeoTitle = modelt.SeoTitle,
                    SeoKeyword = modelt.SeoKeyword,
                    SeoDescription = modelt.SeoDescription,
                    Status = modelt.Status,
                    AddBy = modelt.AddBy,
                    AddTime = modelt.AddTime,
                    UpdateBy = modelt.UpdateBy,
                    UpdateTime = modelt.UpdateTime,


                };
                modelDto.Children.AddRange(
                    await GetChilds(listData, modelt.Id, classLayer)
                );
                listDto.Add(modelDto);
            }
            return listDto;
        }
        #endregion
    }
}
