﻿using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Services
{
    /// <summary>
    /// 文章标签接口实现
    /// </summary>
    public class ArticleLabelService : BaseService, IArticleLabelService
    {
        public ArticleLabelService(IDbContextFactory contentFactory) : base(contentFactory) { }
    }
}
