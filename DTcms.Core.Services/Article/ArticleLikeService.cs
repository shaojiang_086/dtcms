﻿using AutoMapper;
using DTcms.Core.Common.Emum;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.Services
{
    /// <summary>
    /// 文章点赞接口实现
    /// </summary>
    public class ArticleLikeService : BaseService, IArticleLikeService
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IArticleService _articleService;

        public ArticleLikeService(
            IDbContextFactory contentFactory,
            IUserService userService,
            IArticleService articleService,
            IMapper mapper) : base(contentFactory)
        {
            _userService = userService;
            _mapper = mapper;
            _articleService = articleService;
        }

        /// <summary>
        /// 更新用户点赞数据
        /// </summary>
        public async Task<int> UserUpdateLikeAsync(long articleId, WriteRoRead writeAndRead = WriteRoRead.Write)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            int userId =await _userService.GetUserIdAsync();
            //获得点赞数据实体
            ArticleLike likeModel = await base.QueryAsync<ArticleLike>(x => x.ArticleId == articleId && x.UserId == userId);
            //获取文章旧数据
            Articles oldArticleModel = await _articleService.QueryAsync(t => t.Id == articleId);
            if (oldArticleModel==null)
            {
                return 0;
            }
            //用于局部更新
            Articles articleModel = new Articles();
            if (oldArticleModel != null)
            {
                //指明更新主键
                articleModel.Id = oldArticleModel.Id;
                if (likeModel != null)//存在执行删除操作
                {
                    _context.Set<ArticleLike>().Attach(likeModel);
                    _context.Set<ArticleLike>().Remove(likeModel);
                    //点赞数
                    articleModel.LikeCount = oldArticleModel.LikeCount - 1;
                }
                else//不存在执行添加操作
                {
                    ArticleLike like = new ArticleLike();
                    like.ArticleId = articleId;
                    like.AddTime = DateTime.Now;
                    like.UserId = userId;
                    await _context.Set<ArticleLike>().AddAsync(like);
                    //点赞数
                    articleModel.LikeCount = oldArticleModel.LikeCount + 1;
                }
            }
            else
            {
                return 0;
            }
            //更新点赞数据
            //await _articleService.UpdateAsync(articleId, model);

            var entry = _context.Entry<Articles>(articleModel);
            //设置修改状态
            entry.State = EntityState.Unchanged;
            entry.Property(o => o.LikeCount).IsModified = true;
            //提交保存
            int res = await _context.SaveChangesAsync();
            return articleModel.LikeCount;

        }
    }
}
