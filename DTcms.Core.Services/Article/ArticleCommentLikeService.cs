﻿using AutoMapper;
using DTcms.Core.Common.Emum;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.Services
{
    /// <summary>
    /// 评论点赞接口实现
    /// </summary>
    public class ArticleCommentLikeService : BaseService, IArticleCommentLikeService
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IArticleCommentService _commentService;

        public ArticleCommentLikeService(
            IDbContextFactory contentFactory,
            IUserService userService,
            IArticleCommentService commentService,
            IMapper mapper) : base(contentFactory)
        {
            _userService = userService;
            _mapper = mapper;
            _commentService = commentService;
        }

        /// <summary>
        /// 更新用户点赞数据
        /// </summary>
        public async Task<int> UserUpdateLikeAsync(long commentId, WriteRoRead writeAndRead = WriteRoRead.Write)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            int userId = await _userService.GetUserIdAsync();
            //获得点赞数据实体
            ArticleCommentLike likeModel = await base.QueryAsync<ArticleCommentLike>(x => x.CommentId == commentId && x.UserId == userId);
            //获取评论旧数据
            ArticleComment oldcommentModel = await _commentService.QueryAsync(t => t.Id == commentId);
            //用于局部更新
            ArticleComment commentModel = new ArticleComment();
            if (oldcommentModel != null)
            {
                //指明更新主键
                commentModel.Id = oldcommentModel.Id;
                if (likeModel != null)//存在执行删除操作
                {
                    _context.Set<ArticleCommentLike>().Attach(likeModel);
                    _context.Set<ArticleCommentLike>().Remove(likeModel);
                    //点赞数
                    commentModel.LikeCount = oldcommentModel.LikeCount - 1;
                }
                else//不存在执行添加操作
                {
                    ArticleCommentLike like = new ArticleCommentLike();
                    like.CommentId = commentId;
                    like.AddTime = DateTime.Now;
                    like.UserId = userId;
                    await _context.Set<ArticleCommentLike>().AddAsync(like);
                    //点赞数
                    commentModel.LikeCount = oldcommentModel.LikeCount + 1;
                }
            }
            else
            {
                return 0;
            }
            //更新点赞数据
            //await _commentService.UpdateAsync(commentId, model);

            var entry = _context.Entry<ArticleComment>(commentModel);
            //设置修改状态
            entry.State = EntityState.Unchanged;
            entry.Property(o => o.LikeCount).IsModified = true;
            //提交保存
            int res = await _context.SaveChangesAsync();
            return commentModel.LikeCount;

        }
    }
}