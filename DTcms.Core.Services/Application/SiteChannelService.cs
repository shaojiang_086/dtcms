﻿using AutoMapper;
using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.Services
{
    /// <summary>
    /// 站点频道
    /// </summary>
    public class SiteChannelService : BaseService, ISiteChannelService
    {
        private readonly INavigationService _navigationService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public SiteChannelService(IDbContextFactory contentFactory,
            INavigationService navigationService,
            IUserService userService,
            IMapper mapper) : base(contentFactory)
        {
            _navigationService = navigationService;
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// 获取频道信息
        /// </summary>
        public async Task<SiteChannel> QueryAsync(Expression<Func<SiteChannel, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            SiteChannel result = await _context.Set<SiteChannel>()
                .Include(d => d.Fields).FirstOrDefaultAsync(funcWhere);
            return result;
        }

        /// <summary>
        /// 查询分页列表
        /// </summary>
        public async Task<PaginationList<SiteChannel>> QueryPageAsync(int pageSize, int pageIndex, Expression<Func<SiteChannel, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            var result = _context.Set<SiteChannel>()
                .Include(x => x.Fields)
                .Where(funcWhere).OrderByBatch(orderBy);//调用Linq扩展类排序
            return await PaginationList<SiteChannel>.CreateAsync(pageIndex, pageSize, result);
        }

        /// <summary>
        /// 添加频道(含菜单导航)
        /// </summary>
        public async Task<SiteChannelDto> AddAsync(SiteChannelEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库

            //检查频道名称是否重复(同一点站点频道名不能重复)
            if (await _context.Set<SiteChannel>().FirstOrDefaultAsync(
                x => x.SiteId == modelDto.SiteId && x.Name.ToLower() == modelDto.Name.ToLower()) != null)
            {
                throw new ResponseException($"频道名称{modelDto.Name}已重复", ErrorCode.RepeatField);
            }
            //检查站点信息是否正确
            if (await _context.Set<Sites>().FirstOrDefaultAsync(x => x.Id == modelDto.SiteId && x.ParentId == 0) == null)
            {
                throw new ResponseException($"站点不存在或非源数据站", ErrorCode.NotFound);
            }
            //联合查询站点菜单
            var navModel = await _navigationService.QueryBySiteIdAsync(modelDto.SiteId);
            if (navModel == null)
            {
                throw new ResponseException("站点菜单不存在或已删除", ErrorCode.NotFound);
            }
            //查找菜单模型列表
            var modelList = await _navigationService.QueryModelAsync(NavType.Channel);
            if (modelList == null)
            {
                throw new ResponseException("频道菜单模型数据不存在", ErrorCode.NotFound);
            }
            //映射成实体
            var model = _mapper.Map<SiteChannel>(modelDto);
            model.AddBy = await _userService.GetUserNameAsync();
            model.AddTime = DateTime.Now;

            //开启事务
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    //1.添加频道
                    await _context.Set<SiteChannel>().AddAsync(model);
                    await this.SaveAsync();
                    //2.查找菜单模型，添加相应的导航
                    await AddNavigation(modelList, model, navModel.Name, navModel.Id, 0);
                    //提交事务
                    await transaction.CommitAsync();
                }
                catch
                {
                    //回滚事务
                    await transaction.RollbackAsync();
                    throw new ResponseException("保存频道时发生意外错误");
                }
            }
            //映射成DTO
            return _mapper.Map<SiteChannelDto>(model);
        }

        /// <summary>
        /// 修改一条记录
        /// </summary>
        public async Task<bool> UpdateAsync(int id, SiteChannelEditDto modelDto, WriteRoRead writeAndRead = WriteRoRead.Write)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库

            //检查数据是否存在
            var model = await _context.Set<SiteChannel>().Include(x => x.Fields).FirstOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                throw new ResponseException($"数据不存在或已删除", ErrorCode.NotFound);
            }
            //频道名称不可更改
            if (model.Name != modelDto.Name)
            {
                throw new ResponseException($"频道名称不可更改", ErrorCode.ParameterError);
            }
            //所属站点不可更改
            if (model.SiteId != modelDto.SiteId)
            {
                throw new ResponseException($"所属站点不可更改", ErrorCode.ParameterError);
            }
            //联合查询站点菜单
            var siteNavModel = await _navigationService.QueryBySiteIdAsync(modelDto.SiteId);
            if (siteNavModel == null)
            {
                throw new ResponseException("站点菜单不存在或已删除", ErrorCode.NotFound);
            }
            //修改菜单
            var navModel = await _context.Set<Navigation>()
                .FirstOrDefaultAsync(x => x.ParentId == siteNavModel.Id && x.ChannelId == model.Id);
            if (navModel != null)
            {
                navModel.Title = modelDto.Title;
                _context.Set<Navigation>().Update(navModel);
            }
            //将DTO映射到源数据,修改站点信息
            _mapper.Map(modelDto, model);
            _context.Set<SiteChannel>().Update(model);
            //保存到数据库
            return await this.SaveAsync();
        }

        /// <summary>
        /// 删除频道(含扩展字段及菜单)
        /// </summary>
        public async Task<bool> DeleteAsync(SiteChannel model, WriteRoRead writeAndRead = WriteRoRead.Write)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库

            if (model == null)
            {
                throw new ResponseException("数据不存在或已删除！");
            }
            var removeList = _context.Set<Navigation>().Where(x => x.ChannelId == model.Id);
            _context.Set<Navigation>().RemoveRange(removeList);//删除菜单
            _context.Set<SiteChannel>().Remove(model);//删除频道
            return await this.SaveAsync();
        }

        /// <summary>
        /// 删除频道(含扩展字段及菜单)
        /// </summary>
        public async Task<bool> DeleteAsync(Expression<Func<SiteChannel, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Write)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库

            var list = await _context.Set<SiteChannel>()
                .Include(x => x.Fields).Where(funcWhere).ToListAsync();
            if (list == null)
            {
                return false;
            }
            foreach (var modelt in list)
            {
                //删除菜单
                var navModel = await _context.Set<Navigation>().FirstOrDefaultAsync(x => x.ChannelId == modelt.Id);
                if (navModel != null)
                {
                    _context.Set<Navigation>().Remove(navModel);
                }
                //加入追踪列表
                _context.Set<SiteChannel>().Attach(modelt);
            }
            //删除频道
            _context.Set<SiteChannel>().RemoveRange(list);
            //一次保存到数据库
            return await this.SaveAsync();
        }

        /// <summary>
        /// 迭代添加导航菜单(私有方法)
        /// </summary>
        /// <param name="modelData">菜单模型列表</param>
        /// <param name="channelModel">当前频道</param>
        /// <param name="navParentName">导航菜单父名称</param>
        /// <param name="navParentId">导航菜单父ID</param>
        /// <param name="modelParentId">菜单模型父ID</param>
        private async Task AddNavigation(IEnumerable<NavigationModel> modelData, SiteChannel channelModel,
            string navParentName, int navParentId, int modelParentId)
        {
            Navigation navModel;//创建导航菜单
            //查找并排序
            IEnumerable<NavigationModel> models = modelData.Where(x => x.ParentId == modelParentId).OrderByBatch("SortId,Id");
            foreach (var modelt in models)
            {
                //实例化菜单
                navModel = new Navigation
                {
                    ParentId = navParentId,
                    ChannelId = channelModel.Id,
                    Name = $"{navParentName}_{channelModel.Name}_{modelt.Name}",
                    Title = modelParentId == 0 ? channelModel.Title : modelt.Title,
                    SubTitle = modelt.SubTitle,
                    IconUrl = modelt.IconUrl,
                    LinkUrl = modelt.LinkUrl,
                    SortId = modelt.SortId,
                    IsSystem = 1,
                    Controller = $"{modelt.Controller}@{channelModel.Id}",
                    Resource = modelt.Resource,
                    AddBy = channelModel.AddBy
                };
                //保存导航菜单
                await _context.Set<Navigation>().AddAsync(navModel);
                await this.SaveAsync();
                //迭代循环查找并添加，直到结束
                await AddNavigation(modelData, channelModel, navParentName, navModel.Id, modelt.Id);
            }
        }
    }
}
