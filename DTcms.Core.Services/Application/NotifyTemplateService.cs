﻿using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Services
{
    public class NotifyTemplateService : BaseService, INotifyTemplateService
    {
        public NotifyTemplateService(IDbContextFactory contentFactory) : base(contentFactory) { }
    }
}
