﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.Services
{
    public class SitePaymentService : BaseService, ISitePaymentService
    {
        public SitePaymentService(IDbContextFactory contentFactory) : base(contentFactory) { }

        /// <summary>
        /// 查询一条记录
        /// </summary>
        public async Task<SitePayment> QueryAsync(Expression<Func<SitePayment, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            var result = await _context.Set<SitePayment>()
                .Include(x => x.Payment)
                .Include(x => x.Site)
                .FirstOrDefaultAsync(funcWhere);
            return result;
        }

        /// <summary>
        /// 查询指定数量列表
        /// </summary>
        public async Task<IEnumerable<SitePayment>> QueryListAsync(int top, Expression<Func<SitePayment, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            var result = _context.Set<SitePayment>()
                .Include(x => x.Payment)
                .Include(x => x.Site)
                .Where(funcWhere).OrderByBatch(orderBy);//调用Linq扩展类进行排序
            if (top > 0) result = result.Take(top);//等于0显示所有数据
            return await result.ToListAsync();
        }

        /// <summary>
        /// 查询分页列表
        /// </summary>
        public async Task<PaginationList<SitePayment>> QueryPageAsync(int pageSize, int pageIndex, Expression<Func<SitePayment, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            var result = _context.Set<SitePayment>()
                .Include(x => x.Payment)
                .Include(x => x.Site)
                .Where(funcWhere).OrderByBatch(orderBy);//调用Linq扩展类排序
            return await PaginationList<SitePayment>.CreateAsync(pageIndex, pageSize, result);
        }
    }
}
