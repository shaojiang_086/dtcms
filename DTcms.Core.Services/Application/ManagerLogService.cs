﻿using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTcms.Core.Services
{
    /// <summary>
    /// 管理日志实现
    /// </summary>
    public class ManagerLogService : BaseService, IManagerLogService
    {
        public ManagerLogService(IDbContextFactory contentFactory) : base(contentFactory) { }

    }
}
