﻿using DTcms.Core.Common.Emum;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.Services
{
    /// <summary>
    /// 系统配置接口实现
    /// </summary>
    public class SysConfigService : BaseService, ISysConfigService
    {
        public SysConfigService(IDbContextFactory contentFactory) : base(contentFactory) { }

        /// <summary>
        /// 根据配置类型返回相应数据
        /// </summary>
        public async Task<SysConfig> QueryByTypeAsync(ConfigType configType, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            return await _context.Set<SysConfig>().FirstOrDefaultAsync(x => x.Type== configType.ToString());
        }
    }
}
