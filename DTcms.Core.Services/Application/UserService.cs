﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.Services
{
    /// <summary>
    /// Identity用户接口实现
    /// </summary>
    public class UserService : BaseService, IUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public UserService(IDbContextFactory contentFactory,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IHttpContextAccessor httpContextAccessor) : base(contentFactory)
        {
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        /// <summary>
        /// 判断当前登录用户是否超管
        /// </summary>
        public async Task<bool> IsSuperAdminAsync()
        {
            //获得当前用户组
            List<Claim> claimsList = _httpContextAccessor.HttpContext.User.Claims.Where(t => t.Type == ClaimTypes.Role).ToList();
            if (claimsList == null)
            {
                return false;
            }
            //遍历用户所拥有的角色
            foreach (var claim in claimsList)
            {
                ApplicationRole role = await _roleManager.FindByNameAsync(claim.Value);
                if (role == null)
                {
                    continue;
                }
                //如果是超级管理员则直接允许访问
                if (role.RoleType == (int)RoleType.SuperAdmin)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 获取当前登录用户ID
        /// </summary>
        public async Task<int> GetUserIdAsync()
        {
            if (_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier) == null)
            {
                return 0;
            }
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var model = await _userManager.FindByIdAsync(userId);
            if (model != null)
            {
                return model.Id;
            }
            return 0;
        }

        /// <summary>
        /// 获取当前登录用户名
        /// </summary>
        public async Task<string> GetUserNameAsync()
        {
            if (_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier) == null)
            {
                return null;
            }
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var model = await _userManager.FindByIdAsync(userId);
            if (model != null)
            {
                return model.UserName;
            }
            return null;
        }

        /// <summary>
        /// 获取当前登录用户信息
        /// </summary>
        public async Task<ApplicationUser> GetUserAsync()
        {
            if (_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier) == null)
            {
                return null;
            }
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            return await _userManager.FindByIdAsync(userId);
        }

        /// <summary>
        /// 获取当前登录用户角色所有Claims
        /// </summary>
        public async Task<List<Claim>> GetRoleClaimsAsync()
        {
            List<Claim> claimList = new List<Claim>();
            //获得当前用户组
            List<Claim> claimRoleList = _httpContextAccessor.HttpContext.User.Claims.Where(t => t.Type == ClaimTypes.Role).ToList();
            if (claimRoleList == null)
            {
                return claimList;
            }
            //遍历用户所拥有的角色
            foreach (var claimRole in claimRoleList)
            {
                ApplicationRole role = await _roleManager.FindByNameAsync(claimRole.Value);
                if (role == null)
                {
                    continue;
                }
                //验证角色的权限是否一致
                IList<Claim> roleClaims = await _roleManager.GetClaimsAsync(role);
                if (roleClaims == null)
                {
                    continue;
                }
                foreach (var claim in roleClaims)
                {
                    if (claimList.Any(x => x.Type == claim.Type && x.Value == claim.Value))
                    {
                        continue;
                    }
                    claimList.Add(claim);
                }
            }
            return claimList;
        }

        /// <summary>
        /// 获取指定角色所有Claims
        /// </summary>
        public async Task<List<Claim>> GetRoleClaimsAsync(int roleId)
        {
            List<Claim> claimList = new List<Claim>();
            //获取角色的Claims
            ApplicationRole role = await _roleManager.FindByIdAsync(roleId.ToString());
            if (role == null)
            {
                return claimList;
            }
            IList<Claim> roleClaims = await _roleManager.GetClaimsAsync(role);
            if (roleClaims == null)
            {
                return claimList;
            }
            foreach (var claim in roleClaims)
            {
                if (claimList.Any(x => x.Type == claim.Type && x.Value == claim.Value))
                {
                    continue;
                }
                claimList.Add(claim);
            }
            return claimList;
        }

        /// <summary>
        /// 修改当前用户密码
        /// </summary>
        public async Task<bool> UpdatePasswordAsync(PasswordDto modelDto)
        {
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var model = await _userManager.FindByIdAsync(userId);
            if (model == null)
            {
                throw new ResponseException("尚未登录或已超时，请登录后操作", ErrorCode.NotFound);
            }
            if (modelDto.NewPassword != modelDto.ConfirmPassword)
            {
                throw new ResponseException("两次输入的密码不一至，请重试");
            }
            if(!await _userManager.CheckPasswordAsync(model, modelDto.Password))
            {
                throw new ResponseException("旧密码不正确，请重试");
            }

            var result = await _userManager.ChangePasswordAsync(model, modelDto.Password, modelDto.NewPassword);
            if (!result.Succeeded)
            {
                throw new ResponseException($"错误代码：{result.Errors.FirstOrDefault().Code}");
            }
            return true;
        }

    }
}
