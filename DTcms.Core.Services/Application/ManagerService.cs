﻿using AutoMapper;
using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.Services
{
    public class ManagerService : BaseService, IManagerService
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public ManagerService(IMapper mapper,
            IUserService userService,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IDbContextFactory contentFactory) : base(contentFactory)
        {
            _mapper = mapper;
            _userService = userService;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        /// <summary>
        /// 获取管理员信息
        /// </summary>
        public async Task<Manager> QueryAsync(Expression<Func<Manager, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库

            Manager result = await _context.Set<Manager>()
                .Include(x => x.ApplicatonUser).ThenInclude(x => x.UserRoles).FirstOrDefaultAsync(funcWhere);
            return result;
        }

        /// <summary>
        /// 获取管理员分页列表
        /// </summary>
        public async Task<PaginationList<Manager>> QueryPageAsync(int pageSize, int pageIndex, Expression<Func<Manager, bool>> funcWhere,
            string orderBy, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            IQueryable<Manager> result = _context.Set<Manager>()
                .Include(x => x.ApplicatonUser).ThenInclude(x => x.UserRoles).Where(funcWhere);//条件筛选
            result = LinqExtensions.OrderByBatch<Manager>(result, orderBy);//调用Linq扩展类排序
            return await PaginationList<Manager>.CreateAsync(pageIndex, pageSize, result);
        }

        /// <summary>
        /// 添加管理员
        /// </summary>
        public async Task<ManagerDto> AddAsync(ManagerEditDto modelDto)
        {
            //检查角色是否存在
            var role = await _roleManager.FindByIdAsync(modelDto.RoleId.ToString());
            if (role == null)
            {
                throw new ResponseException("指定的角色不存在", ErrorCode.NotFound);
            }
            //创建用户对象
            var user = new ApplicationUser()
            {
                UserName = modelDto.UserName,
                Email = modelDto.Email,
                PhoneNumber = modelDto.Phone,
                Status = modelDto.Status
            };
            //将用户与角色关联
            user.UserRoles = new List<ApplicationUserRole>()
            {
                new ApplicationUserRole()
                {
                    RoleId=role.Id,
                    UserId=user.Id
                }
            };
            //将用户管理员信息关联
            if (role.RoleType > 0)
            {
                //否则新增管理员信息表
                user.Manager = new Manager()
                {
                    UserId = user.Id,
                    Avatar = modelDto.Avatar,
                    RealName = modelDto.RealName,
                    IsAudit = modelDto.IsAudit,
                    AddTime = DateTime.Now
                };
            }
            //HASH密码，保存用户
            var result = await _userManager.CreateAsync(user, modelDto.Password);
            if (!result.Succeeded)
            {
                throw new ResponseException($"{result.Errors.FirstOrDefault().Description}");
            }

            //映射成DTO
            return _mapper.Map<ManagerDto>(user.Manager);
        }

        /// <summary>
        /// 修改管理员
        /// </summary>
        public async Task<bool> UpdateAsync(int userId, ManagerEditDto modelDto)
        {
            //检查管理员信息是否存在
            var manager = await this.QueryAsync(x => x.UserId == userId);
            if (manager == null)
            {
                throw new ResponseException("管理员不存在或已删除", ErrorCode.NotFound);
            }
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
            {
                throw new ResponseException("用户不存在或已删除", ErrorCode.NotFound);
            }
            //检查角色是否存在
            var role = await _roleManager.FindByIdAsync(modelDto.RoleId.ToString());
            if (role == null)
            {
                throw new ResponseException("指定的角色不存在", ErrorCode.NotFound);
            }
            //用户信息
            if (modelDto.UserName.IsNotNullOrWhiteSpace())
            {
                user.UserName = modelDto.UserName;
            }
            user.Email = modelDto.Email;
            user.PhoneNumber = modelDto.Phone;
            user.Status = modelDto.Status;
            //管理员信息
            user.Manager = new Manager()
            {
                Id = manager.Id,
                UserId = manager.UserId,
                Avatar = modelDto.Avatar,
                RealName = modelDto.RealName,
                IsAudit = modelDto.IsAudit,
                AddTime = manager.AddTime,
                LastIp = manager.LastIp,
                LastTime = manager.LastTime
            };
            var result = await _userManager.UpdateAsync(user);
            //异常错误提示
            if (!result.Succeeded)
            {
                throw new ResponseException($"{result.Errors.FirstOrDefault().Description}");
            }
            //角色信息更新，查找该用户拥有的角色删除
            var userRoles = await _userManager.GetRolesAsync(user);
            await _userManager.RemoveFromRolesAsync(user, userRoles);
            //添加新角色
            await _userManager.AddToRoleAsync(user, role.Name);
            //如果密码不为空，则重置密码
            if (modelDto.Password.IsNotNullOrWhiteSpace())
            {
                //生成token，用于重置密码
                string token = await _userManager.GeneratePasswordResetTokenAsync(user);
                //重置密码
                await _userManager.ResetPasswordAsync(user, token, modelDto.Password);
            }
            return true;
        }

        /// <summary>
        /// 删除管理员
        /// </summary>
        public async Task<bool> DeleteAsync(int userId)
        {
            //检查管理员信息是否存在
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
            {
                return false;
            }
            //删除管理员,角色,用户
            var result = await _userManager.DeleteAsync(user);
            if (result.Succeeded)
            {
                return true;
            }
            return false;
        }
    }
}
