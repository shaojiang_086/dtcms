﻿using DTcms.Core.Common.Emum;
using DTcms.Core.Common.Extensions;
using DTcms.Core.Common.Helper;
using DTcms.Core.DBFactory.Database;
using DTcms.Core.IServices;
using DTcms.Core.Model.Models;
using DTcms.Core.Model.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DTcms.Core.Services
{
    /// <summary>
    /// 全国省市区接口实现
    /// </summary>
    public class AreaService : BaseService, IAreaService
    {
        public AreaService(IDbContextFactory contentFactory) : base(contentFactory) { }

        /// <summary>
        /// 返回所有地区目录树
        /// </summary>
        public async Task<IEnumerable<AreasDto>> QueryListAsync(int parentId, WriteRoRead writeAndRead = WriteRoRead.Read)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            var listData = await _context.Set<Areas>().ToListAsync();
            //调用递归重新生成目录树
            List <AreasDto> result = await GetChilds(listData, parentId);
            return result;
        }

        /// <summary>
        /// 根据条件删除数据(迭代删除)
        /// </summary>
        public async Task<bool> DeleteAsync(Expression<Func<Areas, bool>> funcWhere, WriteRoRead writeAndRead = WriteRoRead.Write)
        {
            _context = _contextFactory.CreateContext(writeAndRead);//连接数据库
            var listData = await _context.Set<Areas>().ToListAsync();//查询所有数据
            var list = await _context.Set<Areas>().Where(funcWhere).ToListAsync();
            if (list == null)
            {
                return false;
            }
            foreach (var modelt in list)
            {
                await DeleteChilds(listData, modelt.Id);//删除子节点
                _context.RemoveRange(modelt);//删除当前节点
            }
            return await this.SaveAsync();
        }

        #region 辅助私有方法
        /// <summary>
        /// 迭代循环删除
        /// </summary>
        private async Task DeleteChilds(IEnumerable<Areas> listData, int parentId)
        {
            IEnumerable<Areas> models = listData.Where(x => x.ParentId == parentId);
            foreach (var modelt in models)
            {
                await DeleteChilds(listData, modelt.Id);//迭代
                _context.RemoveRange(modelt);
            }
        }
        /// <summary>
        /// 迭代循环返回目录树(私有方法)
        /// </summary>
        private async Task<List<AreasDto>> GetChilds(IEnumerable<Areas> listData, int parentId)
        {
            List<AreasDto> listDto = new List<AreasDto>();
            IEnumerable<Areas> models = listData.Where(x => x.ParentId == parentId).OrderByBatch("SortId");//查找并排序
            foreach (Areas modelt in models)
            {
                AreasDto modelDto = new AreasDto
                {
                    Id = modelt.Id,
                    ParentId = modelt.ParentId,
                    Title = modelt.Title,
                    SortId = modelt.SortId
                };
                modelDto.Children.AddRange(
                    await GetChilds(listData, modelt.Id)
                );
                listDto.Add(modelDto);
            }
            return listDto;
        }
        #endregion
    }
}
