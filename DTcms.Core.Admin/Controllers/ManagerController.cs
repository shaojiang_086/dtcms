﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTcms.Core.Admin.Controllers
{
    public class ManagerController : Controller
    {
        /// <summary>
        /// 管理员列表
        /// </summary>
        public IActionResult ManagerList()
        {
            return View();
        }

        /// <summary>
        /// 管理员编辑
        /// </summary>
        public IActionResult ManagerEdit()
        {
            return View();
        }

        /// <summary>
        /// 角色列表
        /// </summary>
        public IActionResult RoleList()
        {
            return View();
        }

        /// <summary>
        /// 角色编辑
        /// </summary>
        public IActionResult RoleEdit()
        {
            return View();
        }

        public IActionResult ManagerLog()
        {
            return View();
        }

        /// <summary>
        /// 修改账户密码
        /// </summary>
        public IActionResult Password()
        {
            return View();
        }
    }
}
