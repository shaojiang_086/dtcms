﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTcms.Core.Admin.Controllers
{
    public class SettingController : Controller
    {
        /// <summary>
        /// 系统配置
        /// </summary>
        public IActionResult Config()
        {
            return View();
        }

        /// <summary>
        /// 站点管理
        /// </summary>
        public IActionResult SiteList()
        {
            return View();
        }

        /// <summary>
        /// 站点编辑
        /// </summary>
        public IActionResult SiteEdit()
        {
            return View();
        }

        /// <summary>
        /// 频道管理
        /// </summary>
        public IActionResult ChannelList()
        {
            return View();
        }

        /// <summary>
        /// 频道编辑
        /// </summary>
        public IActionResult ChannelEdit()
        {
            return View();
        }

        /// <summary>
        /// 地区管理
        /// </summary>
        public IActionResult AreaList()
        {
            return View();
        }

        /// <summary>
        /// 地区编辑
        /// </summary>
        public IActionResult AreaEdit()
        {
            return View();
        }

        /// <summary>
        /// 支付管理
        /// </summary>
        public IActionResult PaymentList()
        {
            return View();
        }

        /// <summary>
        /// 支付编辑
        /// </summary>
        public IActionResult PaymentEdit()
        {
            return View();
        }

        /// <summary>
        /// 支付商管理
        /// </summary>
        public IActionResult PayeeList()
        {
            return View();
        }

        /// <summary>
        /// 支付商编辑
        /// </summary>
        public IActionResult PayeeEdit()
        {
            return View();
        }

        /// <summary>
        /// 菜单管理
        /// </summary>
        public IActionResult NavList()
        {
            return View();
        }

        /// <summary>
        /// 菜单编辑
        /// </summary>
        public IActionResult NavEdit()
        {
            return View();
        }
    }
}
