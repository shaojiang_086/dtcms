﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTcms.Core.Admin.Controllers
{
    public class MemberController : Controller
    {
        /// <summary>
        /// 会员审核
        /// </summary>
        public IActionResult Audit()
        {
            return View();
        }

        /// <summary>
        /// 会员列表
        /// </summary>
        public IActionResult MemberList()
        {
            return View();
        }

        /// <summary>
        /// 会员编辑
        /// </summary>
        public IActionResult MemberEdit()
        {
            return View();
        }

        /// <summary>
        /// 会员组列表
        /// </summary>
        public IActionResult GroupList()
        {
            return View();
        }

        /// <summary>
        /// 会员组编辑
        /// </summary>
        public IActionResult GroupEdit()
        {
            return View();
        }

        /// <summary>
        /// 消息列表
        /// </summary>
        public IActionResult MessageList()
        {
            return View();
        }

        /// <summary>
        /// 消息编辑
        /// </summary>
        public IActionResult MessageEdit()
        {
            return View();
        }

        /// <summary>
        /// 充值记录
        /// </summary>
        public IActionResult RechargeList()
        {
            return View();
        }

        /// <summary>
        /// 充值编辑
        /// </summary>
        public IActionResult RechargeEdit()
        {
            return View();
        }

        /// <summary>
        /// 金额记录
        /// </summary>
        public IActionResult AmountList()
        {
            return View();
        }

        /// <summary>
        /// 金额编辑
        /// </summary>
        public IActionResult AmountEdit()
        {
            return View();
        }

        /// <summary>
        /// 积分记录
        /// </summary>
        public IActionResult PointList()
        {
            return View();
        }

        /// <summary>
        /// 积分编辑
        /// </summary>
        public IActionResult PointEdit()
        {
            return View();
        }

        /// <summary>
        /// 消息模板
        /// </summary>
        public IActionResult TemplateList()
        {
            return View();
        }

        /// <summary>
        /// 消息模板编辑
        /// </summary>
        public IActionResult TemplateEdit()
        {
            return View();
        }

        /// <summary>
        /// 会员参数设置
        /// </summary>
        public IActionResult Config()
        {
            return View();
        }
    }
}
