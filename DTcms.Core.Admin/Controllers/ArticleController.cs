﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DTcms.Core.Admin.Controllers
{
    /// <summary>
    /// 网站内容部分
    /// </summary>
    public class ArticleController : Controller
    {
        #region 内容管理
        public IActionResult ArticleList()
        {
            return View();
        }
        public IActionResult ArticleEdit()
        {
            return View();
        }
        #endregion

        #region 栏目类别管理
        [HttpGet]
        public IActionResult CategoryList()
        {
            return View();
        }
        [HttpGet]
        public IActionResult CategoryEdit()
        {
            return View();
        }
        #endregion

        #region 标签管理
        public IActionResult LabelList()
        {
            return View();
        }
        public IActionResult LabelEdit()
        {
            return View();
        }
        #endregion

        #region 评论管理
        public IActionResult CommentList()
        {
            return View();
        }
        #endregion

        #region 投稿管理
        public IActionResult ContributeList()
        {
            return View();
        }
        public IActionResult ContributeEdit()
        {
            return View();
        } 
        #endregion

    }
}
