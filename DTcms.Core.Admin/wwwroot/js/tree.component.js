﻿/*
 *单选下拉树
 * value 双向绑定属性
 * data 树形数据
 * excludeId 要排除的主键
 * placeholder 默认显示文本
 * refName 组件ref
 * 
 */
Vue.component('dt-dropdown-single-tree', {
    template: `<el-dropdown split-button class="dropdown-tree" >
                    <input type="text" v-model="value" style="display:none;"/>
                    {{dropdownLable}}
                    <el-dropdown-menu slot="dropdown">
                        <el-tree 
                            :data="data" 
                            :default-expand-all="true" 
                            :expand-on-click-node="false" 
                            @node-click="handleNode" 
                            v-bind="$attrs"
                        >
                        </el-tree>
                    </el-dropdown-menu>
                </el-dropdown>`,
    inheritAttrs: false,
    props: {
        value: Number,
        data: Array,
        excludeId: {
            type: Number,
            default: 0,
        },
        placeholder: {
            type: String,
            default: '无父级'
        },
        refName: {
            type: String,
            default: 'ref' + new Date().getTime()
        }
    },
    data: function () {
        console.log(this.data);
        return {
            dropdownLable: this.placeholder,
            intiData: this.data,
        }
    },
    created: function () {        
    },
    methods: {
        handleNode(data) {
            if (this.excludeId === data.id && this.excludeId != 0) {
                this.$message.error('不能选择自身为父级！');
                return false;
            }
            this.dropdownLable = data.title;
            this.$emit('input', data.id);
        },
        /*根据Id获得父级名称
        *id  查找的id，
        *nodes   原始Json数据
        *path    供递归使用
        */
        findParentTitle: function (_id, nodes, path) {
            let _this = this;
            if (path === undefined) {
                path = [];
            }
            if (_id == 0) {
                return _this.placeholder;
            }
            for (let i = 0; i < nodes.length; i++) {
                let tmpPath = path.concat();
                tmpPath.push(nodes[i].id);
                if (_id == nodes[i].id) {
                    let title = nodes[i].title;
                    return title;
                }
                if (nodes[i].children) {
                    let findResult = _this.findParentTitle(_id, nodes[i].children, tmpPath);
                    if (findResult) {
                        return findResult;
                    }
                }
            }
        },
    },
    mounted: function () {
        
        const token = window.localStorage.getItem('access_token');
        if (token) {
            this.headers = { Authorization: 'Bearer ' + token };
        }
    },
    watch: {
        value: function (newVal) {
            let _this = this;
            if (this.value != 0) {
                _this.dropdownLable = _this.findParentTitle(_this.value, _this.data);
            } else {
                _this.dropdownLable = _this.placeholder;
            }
        },
        excludeId: function (newVal) {
        }
    }
});

/*
 * 复选框下拉树
 * data 树形数据
 * value 绑定选中节点
 * check-strictly 是否严格的遵循父子不互相关联的做法 
 * checked-keys 选中值，数组
 * trigger 触发下拉的行为hover或click
 */
Vue.component('dt-dropdown-check', {
    template: `<div class="dropdown"><el-dropdown :trigger="trigger" placement="bottom-start">
                    <el-button class="check" size="small" icon="el-icon-plus"></el-button>
                    <el-dropdown-menu slot="dropdown" class="dropdown-menu">
                    <el-tree ref="tree" 
                    :data="data" node-key="id" default-expand-all show-checkbox check-on-click-node :check-strictly="checkStrictly" 
                    :expand-on-click-node="false" @check="handleCheck">
                    <span slot-scope="{ node, data }">{{ data.title }}</span>
                    </el-tree>
                    </el-dropdown-menu>
                    </el-dropdown>
                    <el-tag :key="item.id" v-for="item in value" 
                    effect="dark" disable-transitions closable 
                    @close="handleClose(item)" style="margin-left:3px">{{item.title}}</el-tag></div>`,
    props: {
        trigger: {
            type: String,
            default: function () {
                return 'click';
            }
        },
        checkStrictly: {
            type: Boolean,
            default: true,
        },
        checked: {
            type: Array,
            default: function () {
                return [];
            }
        },
        value: {
            type: Array,
            default: function () {
                return [];
            }
        },
        data: {
            type: Array,
            default: function () {
                return [];
            }
        }
    },
    data: function () {
        return {

        }
    },
    methods: {
        //删除Tag的回调
        handleClose: function (val) {
            let _this = this; //当前页面
            //删除当前选中项
            _this.value.forEach((item, i) => {
                if (item == val) {
                    _this.value.splice(i, 1);
                }
            })
            //重新设置选中值
            let nodeKeys = [];
            _this.value.forEach((item, i) => {
                nodeKeys.push(item.id);
            })
            _this.$refs.tree.setCheckedKeys(nodeKeys);
            _this.$emit('input', _this.value)
        },
        //选中发生变化回调
        handleCheck: function () {
            let _this = this; //当前页面
            let nodes = []; //临时节点
            _this.$refs.tree.getCheckedNodes().map(item => {
                nodes.push(item);
            });
            _this.$emit('input', nodes);
        }
    },
    watch: {
        data: function (newVal) {
            if (newVal.length > 0) {
                this.$nextTick(() => {
                    this.$refs.tree.setCheckedKeys(this.checked);
                    this.handleCheck();
                });
            }
        },
        checked: function (newVal) {
            if (newVal.length > 0) {
                this.$nextTick(() => {
                    this.$refs.tree.setCheckedKeys(newVal);
                    this.handleCheck();
                });
            }
        }
    }
});

/*
 * 单选下拉树
 * data 树形数据
 * value 绑定选中节点
 * placeholder 没有选中节点时提示
 * disabled 不能选中的节点
 * trigger 触发下拉的行为hover或click
 */
Vue.component('dt-dropdown-select', {
    template: `<div class="dropdown">
                    <el-dropdown ref="drop" :trigger="trigger" placement="bottom-start">
                    <el-button class="select" plain>
                        <span>{{title}}</span>
                        <i class="el-icon-arrow-down el-icon--right"></i>
                    </el-button>
                    <el-dropdown-menu slot="dropdown" class="dropdown-menu" >
                        <el-tree ref="tree" :data="data" node-key="id" default-expand-all :current-node-key="value" :highlight-current="true"
                        :expand-on-click-node="false" @node-click="handleClick">
                            <span slot-scope="{ node, data }">{{ data.title }}</span>
                        </el-tree>
                    </el-dropdown-menu>
                    </el-dropdown>
                </div>`,
    props: {
        trigger: {
            type: String,
            default: function () {
                return 'click';
            }
        },
        placeholder: {
            type: String,
            default: function () {
                return '请选择..';
            }
        },
        value: {
            type: Number,
            default: function () {
                return 0;
            }
        },
        disabled: {
            type: Number,
            default: function () {
                return 0;
            }
        },
        data: {
            type: Array,
            default: function () {
                return [];
            }
        }
    },
    data: function () {
        return {
            title: this.placeholder
        }
    },
    methods: {
        //选中回调
        handleClick: function (data, node, tList) {
            let _this = this; //当前页面
            if (_this.disabled == data.id) {
                _this.$message.error('不能将自己设为父级');
                _this.$refs.tree.setCurrentKey(_this.value);
                return false;
            }
            _this.title = data.title;
            _this.$emit('input', data.id);
            //触发组件的点击事件关闭下拉框
            _this.$refs.drop.hide();
        },
        //迭代遍历树
        initData: function (obj) {
            let _this = this;
            obj.map(function (item) {
                if (item.id == _this.value) {
                    _this.title = item.title;
                    _this.$refs.tree.setCurrentKey(item.id);
                    return false;
                }else if (item.children) {
                    _this.initData(item.children);
                }
            });
        }
    },
    watch: {
        data: function (newVal) {
            let _this = this;
            if (this.value > 0 && newVal.length > 0) {
                _this.$nextTick(() => {
                    _this.initData(newVal);
                });
            }
        },
        value: function (newVal, oldVal) {
            let _this = this;
            if (oldVal == 0 && newVal > 0 && _this.data.length > 0) {
                _this.$nextTick(() => {
                    _this.initData(_this.data);
                });
            }
        }
    }
});