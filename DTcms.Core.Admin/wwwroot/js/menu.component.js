//定义全局组件
Vue.component('dt-menu', {
	props: {
		mini: false,
		selected: 0,
		listData: {
			type: Array,
			default: function () {
				return [];
			}
		}
	},
	render: function (h) {
		let self = this;
		return h('div', { 'class': self.mini ? 'mini' : '' }, [
			//循环遍历
			self.listData.map(function (item, index) {
				//如果是第一层，需要添加DIV
				if (item.parentId === 0) {
					/*如果是简洁模式下，展开所有第一层*/
					if (self.mini) {
						item.isExpand = true
					}
					//创建VDOM对象
					return h('div', {
						'class': item.isSelected ? 'list-group selected' : 'list-group',
					}, [
						h('h1',
							{
								attrs: { title: item.text }
							}, [
							h('i', { 'class': item.icon })]),
						h('div', { 'class': 'list-wrap' },
							[
								h('h2',
									{
										on: {
											click: function () {
												//console.log('h2 click!'+item.isExpand)
												item.isExpand = !item.isExpand
												//闭合相邻节点
												self.listData.map(function (sitem, sindex) {
													if (index != sindex) {
														sitem.isExpand = false;
													}
												})
											}
										}
									}, [
									h('i', { 'class': item.icon }),
									h('span', item.text),
									h('b', { 'class': 'iconfont icon-arrow-down' })
								]
								),
								item.children && item.children.length > 0 ? self.elements(item, item, item.children, h) : null
							]
						)
					]);
				}
			})
		]);
	},
	methods: {
		elements: function (rootItem, parentItem, sData, h) {
			var self = this;
			return h('transition', { attrs: { name: 'slide' } },
				[
					//return h('ul',
					h('ul',
						{
							//style: { 'display': parentItem.isExpand ? 'block' : 'none' },
							directives: [{ name: 'show', value: parentItem.isExpand }]
						}, [
						//循环遍历
						sData.map(function (item, index) {
							return h('li', [
								h('a', {
									'class': item.children.length == 0 && item.isSelected ? 'selected' : null,
									attrs: {
										'href': item.href != null && item.href.length > 0 ? item.channelId > 0 ? item.href + '?channelId=' + item.channelId : item.href : 'javascript:;',
										'target': 'mainframe'
									},
									on: {
										click: function (e) {
											//console.log('单击了菜单，子菜单状态：' + item.children.length)
											if (item.children.length == 0) {
												//console.log('选中菜单' + item.id)
												window.localStorage.setItem('dt_menu_selected_id', item.id);//存入本地
												self.selectItem(self.listData, rootItem, item);
											}
											if ((item.children.length == 0) || typeof (item.isExpand) === "undefined") {
												return false;
											};
											item.isExpand = !item.isExpand
											if (item.isExpand) {
												sData.map(function (sitem, sindex) {
													if (index != sindex) {
														sitem.isExpand = false;
													}
												})
											}
										}
									}
								}, [
									h('i', { 'class': item.icon != null && item.icon.length > 0 ? item.icon : item.children.length > 0 ? 'iconfont icon-folder' : 'iconfont icon-file' }),
									h('span', item.text),
									item.children.length > 0 ? h('b', { 'class': item.isExpand ? 'iconfont icon-open' : 'iconfont icon-close' }) : null,
								]),
								item.children && item.children.length > 0 ? self.elements(rootItem, item, item.children, h) : null
							])
						})
					])

				])
		},
		selectItem: function (sData, rootItem, currItem) {
			let self = this;
			sData.map(function (item) {
				item.isSelected = false
				if (currItem === item) {
					currItem.isSelected = true
					rootItem.isSelected = true
				} else {
					item.isSelected = false
				}
				if (item.children && item.children.length > 0) {
					self.selectItem(item.children, rootItem, currItem);
				}
			})
		},
		selectId: function (sData, id) {
			let self = this;
			sData.map(function (item) {
				if (item.children.length == 0 && parseInt(item.id) === parseInt(id)) {
					//console.log('找到菜单...' + item.id)
					//选中当前节点
					item.isSelected = true
					//跳转链接
					if (typeof (item.href) != 'undefined' && item.href.length > 0 && item.href != '#') {
						if (item.channelId > 0) {
							frames["mainframe"].location.href = item.href + '?channelId=' + item.channelId
						} else {
							frames["mainframe"].location.href = item.href
                        }
					}
					//查找所有的父节点
					let parentNodes = self.findAllParent(item, self.listData, [], 0);
					parentNodes.map(function (pNode) {
						if (pNode.parentId === 0) {
							pNode.isSelected = true;
							pNode.isExpand = true;
						} else {
							pNode.isExpand = true;
						}
					})
				} else {
					item.isExpand = false;
					item.isSelected = false;
				}
				if (item.children && item.children.length > 0) {
					self.selectId(item.children, id);
				}
			})
		},
		//查找所有父节点
		findAllParent: function (node, tree, parentNodes, index) {
			let self = this;
			if (!node || node.parentId === 0) {
				return;
			}
			self.findParent(node, parentNodes, tree);
			let parntNode = parentNodes[index];
			self.findAllParent(parntNode, tree, parentNodes, ++index);
			return parentNodes;
		},
		//查找父节点
		findParent: function (node, parentNodes, tree) {
			let self = this;
			for (let i = 0; i < tree.length; i++) {
				let item = tree[i];
				if (item.id === node.parentId) {
					parentNodes.push(item);
					return;
				}
				if (item.children && item.children.length > 0) {
					self.findParent(node, parentNodes, item.children);
				}
			}
		},
		//渲染完成后初始化菜单
		initData() {
			let self = this;
			let menuId = window.localStorage.getItem('dt_menu_selected_id');
			if (parseInt(self.selected) > 0) {
				//选中指定的菜单
				self.selectId(self.listData, self.selected)
			} else if (menuId > 0) {
				//选中本地的菜单
				self.selectId(self.listData, menuId)
			}
        }
	},
	watch: {
		mini: function (newVal) {
			let self = this;
			if (!newVal) {
				self.listData.map(function (item, index) {
					if (item.isSelected) {
						item.isExpand = true;
					} else {
						item.isExpand = false;
					}
				})
			}
		},
		//正确给listData赋值的方法
		listData: function (newVal) {
			let self = this;
			if (newVal) {
				self.initData();
			}
		}
	}
});